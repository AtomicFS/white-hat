#!/bin/bash

# Get dependency tree
pactree -s -r wht_system > src/_notes_/meta_packages.txt
pactree -s -r -g wht_system > meta_packages.dot

# Change colours
sed -i 's/color=chocolate4/color=crimson/g' meta_packages.dot
sed -i 's/wht_system";/wht_system" [color=crimson];/g' meta_packages.dot

# Generate image
dot -Gbgcolor='transparent' -Tpng meta_packages.dot -o src/img/archlinux/meta_packages.png

