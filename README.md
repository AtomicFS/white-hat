# WhiteHatHacker

These are my notes, that help me to organize the accumulated knowledge and to congregate it in one place. Intended to be offline source.


# Repositories

- Main repository, issue tracker and so on at [git.sr.ht/~atomicfs](https://git.sr.ht/~atomicfs/white-hat)
- Mirror repository at [gitlab.com](https://gitlab.com/AtomicFS/white-hat)
- Static web-pages:
	- [atomicfs.gitlab.io/white-hat](https://atomicfs.gitlab.io/white-hat)
	- [white-hat-book.white-hat-hacker.icu](https://white-hat-book.white-hat-hacker.icu/)


# Dependencies

Plugins are mostly from [mdBook's wiki](https://github.com/rust-lang/mdBook/wiki/Third-party-plugins).

```
cargo install mdbook-katex
cargo install mdbook-admonish
cargo install mdbook-linkcheck
```

## mdbook-katex
[mdbook-katex](https://github.com/lzanini/mdbook-katex)

## mdbook-admonish
[mdbook-admonish](https://github.com/tommilligan/mdbook-admonish)
- [examples](https://tommilligan.github.io/mdbook-admonish/)

