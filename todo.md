# TODO

## ArchLinux

- install sway
- custom packages
- Wake-on-LAN
- [fwupd](fwupd.org/)
- [Backup LUKS headers](https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Backup_and_restore)
- Setup btrfs backups
	- Check [Snapper](https://wiki.archlinux.org/title/Snapper) and Timeshift
	- [Send and receive](https://wiki.archlinux.org/title/Btrfs#Send/receive)
	- [Incremental backup](https://wiki.archlinux.org/title/Btrfs#Incremental_backup_to_external_drive)
- setup rsync? backup
- Fake default OS with backdoors
- ArchLinux installation with full disk encryption (for laptops)
- ArchISO
- add `chkboot --update` to `update-grub`
- fix [grub-imageboot](https://aur.archlinux.org/packages/grub-imageboot)???
- check [AIDE](https://wiki.archlinux.org/title/AIDE) as alternative to `chkboot`
- GPS overview


## Self-hosting

- [keepalived](https://www.keepalived.org/)
- self-hosted DNS like [BIND](https://wiki.archlinux.org/title/BIND), [Unbound](https://wiki.archlinux.org/title/Unbound) [PowerDNS](https://wiki.archlinux.org/title/PowerDNS)
	- [Pi-hole as All-Around DNS Solution](https://docs.pi-hole.net/guides/dns/unbound/)
	- [DNS servers](https://wiki.archlinux.org/title/Domain_name_resolution#DNS_servers)


## Open-source monetization

- Tech support
- Hosting service (like GitLab)

Alternatives
- donations


## Misc

- Money mules
- White horse


## AI

- genetic algorithms
- N.E.A.T. (Neural Evolution of Augmenting Topologies)
- PPO (Proximal Policy Optimization)
- the thing with two neural networks against each other

Splitting into evolutionary algorithms and reinforcement learning


## Hackers

- Steve Wozniak
- John Draper (aka Captain Crunch) - banned from DEFCON


## SSH

Get the fingerprint of an existing SSH public key
https://superuser.com/questions/1377132/get-the-fingerprint-of-an-existing-ssh-public-key

sha256:
ssh-keygen -l -v -f /path/to/publickey

md5:
ssh-keygen -l -E md5 -f /path/to/publickey


## SWAY

- [sov for workspace overview](https://wiki.archlinux.org/title/Sway#Overview_of_workspaces)
- turn off screen during lock
	- swaylock can't do it
	- maybe [swayidle](https://github.com/swaywm/swayidle) configured to not sleep and stuff?


