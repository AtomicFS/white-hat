# GRUB ISO

![](../img/construction.gif)


I always wanted to add Arch Linux ISO into boot options in [GRUB](https://wiki.archlinux.org/title/GRUB), mostly because it is just convinient to have always at hand rescue ISO in case something goes wrong. While I do usually carry [multiboot USB flash disk](multiboot_usb.md) with me as keychain, it would be cooler to have such option right in the GRUB.

I know of [grub-imageboot](https://aur.archlinux.org/packages/grub-imageboot), but that project is quite a mess and not really maintained anymore - not only the AUR package, but also the project itself. I thought about taking it over / fixing it / forking it, but maybe later.

Anyway, I just wanted to put Arch Linux ISO into the `/boot` partition and have it in GRUB. Since ISO is fine. Not gonna lie, to get it working was pain, and it is still sub-optimal.

- [GRUB/Tips_and_tricks](https://wiki.archlinux.org/title/GRUB/Tips_and_tricks#Booting_ISO9660_image_file_directly_via_GRUB)
- [Multiboot_USB_drive](https://wiki.archlinux.org/title/Multiboot_USB_drive#Configuring_GRUB)
- [(GRUB) Booting arch installer iso from a partition on ssd](https://bbs.archlinux.org/viewtopic.php?id=280470)
- [floriandejonckheere/40_iso.cfg](https://gist.github.com/floriandejonckheere/7e7b772cff3387dc8013)
- [Boot arch from iso and grub2](https://bbs.archlinux.org/viewtopic.php?id=78171&p=3)
- [GNU GRUB Manual](https://www.gnu.org/software/grub/manual/grub/grub.html)

However all of these such because they require you to hard-code values like `/boot`'s UUID and ISO filename. Not to mention that they are distribution dependent (since each distro puts `vmlinuz` into different location).

