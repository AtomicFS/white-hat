# sway


```admonish note title="Rant about GNOME"
I used to use [GNOME](https://wiki.archlinux.org/title/GNOME), and I was relatively happy with it. However there were problem. For example `GNOME` does not used configuration files like normal program, rather uses [dconf](https://en.wikipedia.org/wiki/Dconf) and I hate it. It is very difficult to version the configuration as other dotfiles.

And then with `GNOME` version 40 came changes ... they changed keyboard shortcuts, they changed workspace layout, they changed preview ... just terrible. I hated these changes mostly because they were less efficient with screen space and also made no sense. So for a while I was using [vertical-overview](https://github.com/RensAlthuis/vertical-overview) `GNOME` extension to fix these problems.

As the extension maintainer put it:

> Gnome has had vertically stacked workspaces for a long time. The Gnome 40 update unfortunately made the switch to a horizontal layout. A choice that many Gnome users disagree with. This extension Aims to replace the new Gnome overview with something that resembles the old style.

But it was time to look for alternatives.
```

Friends of mine were using [sway](https://swaywm.org/) and it looked awesome! Not gonna lie, it took a while to get used to it, but the transition was worth it.

`sway` is quite modular, checkout section [useful add ons](https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway) in their wiki.

```admonish warning
Keep in mind that my `sway` setup is still work in progress.
```


# Installation

As with most of my systems, I handle the installation via [meta packages](https://wiki.archlinux.org/title/Meta_package_and_package_group). Here is the specific one for `sway`:

```toml
{{#include ../../submodules/atomicfs-repo-arch/packages/archlinux/x86_64/wht_sway/PKGBUILD}}
```

```admonish tip
Add your user into the `seat` group. If the group does not exists, create it.
~~~
# gpasswd -a atom seat
~~~
```


# Login manager

You do not need login manager, but I use it. Specifically [tuigreet](https://github.com/apognu/tuigreet).

![](../img/daily_driver_linux_setup/tuigreet.png)

Edit the configuration file at `/etc/greetd/config.toml`:

```toml
{{#include ../../submodules/dotfiles-system/etc/greetd/config.toml}}
```

And then enable the `greetd.service` and you are ready to go.
```
# systemctl enable greetd.service
```


# Sway configuration

`sway` is configured by `~/.config/sway/config`. It is usable from the start, but feel free to fool around ;)

![](../img/daily_driver_linux_setup/sway-default-01.jpg)

![](../img/daily_driver_linux_setup/sway-default-02.jpg)

Here is my main `sway` configuration file, but things were moved into separate files into [`~/.config/sway/config.d/`](https://git.sr.ht/~atomicfs/dotfiles/tree/master/item/.config/sway/config.d) directory

```admonish example collapsible=true title="~/.config/sway/config"
~~~bash
{{#include ../../submodules/dotfiles/.config/sway/config}}
~~~
```

```admonish tip
- Terminal opens with `Super+Enter`
- `sway` config reloads with `Super+Shift+c`.

*`Super` key is the key with logo (often Windows logo)*
```


## Waybar configuration

While the default status bar is perfectly fine, I have decided recently to switch to [waybar](https://archlinux.org/packages/community/x86_64/waybar/). It is cool and allows you to do all kinds of crazy stuff.

```admonish example collapsible=true title="config"
~~~json
{{#include ../../submodules/dotfiles/.config/waybar/config##template}}
~~~
```

```admonish example collapsible=true title="style.css"
~~~css
{{#include ../../submodules/dotfiles/.config/waybar/style.css}}
~~~
```



# Inspiration

More can be found at [r/unixporn](https://www.reddit.com/r/unixporn), just look for `sway`.

![](../img/daily_driver_linux_setup/sway-unixporn-01.png)

![](../img/daily_driver_linux_setup/sway-unixporn-02.png)

![](../img/daily_driver_linux_setup/sway-unixporn-03.png)


