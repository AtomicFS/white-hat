# Gaming

![](../img/construction.gif)


## Steam

I have installed Steam via Flatpak for foloowing reasons:
- I do not want to infect my system with 32b rubbish required by Steam and Windows games
- I very much dislike Steam and games to spam my home directory with rubbish files and directories, I understand that it is common in Windows to store your data wherever, but here in Linux we do have some standards (I shame on those who do not uphold them! Especially those in OpenSource since they should know better)
- I like to have at least some level of sandboxing between my system and proprietary software

https://wiki.archlinux.org/title/Flatpak


### Proton GE
https://github.com/GloriousEggroll/proton-ge-custom#flatpak
flatpak install com.valvesoftware.Steam.CompatibilityTool.Proton-GE

```bash
{{#include ../../submodules/dotfiles/.local/bin/steam-flatpak}}
```


