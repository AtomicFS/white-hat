# Ansible

![](../img/construction.gif)

- [modules and plugins](https://docs.ansible.com/ansible/latest/collections/all_plugins.html)
- [ansible.builtin.command](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html)
- [Creating a playbook](https://docs.ansible.com/ansible/latest/getting_started/get_started_playbook.html)
- [Ansible](https://wiki.archlinux.org/title/Ansible)

