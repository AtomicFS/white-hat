# tmate

```admonish info
[tmate](https://tmate.io/) is program for instant terminal sharing.
```

[tmate](https://tmate.io/) is awesome! I use it quite often and it is superior to simple screen-sharing that seems to be industry standard. And it is usable right out of the box (see the webpage for details)!

I also host my own [tmate server](../self_hosting/tmate.md).

