# aerc

![](../img/construction.gif)


```admonish info
[aerc](https://aerc-mail.org/) is a terminal-based e-mail client with Vim-style keybindings.
```

```admonish bug title="Rant on email and it's design flaws"
Let me just start by saying that sure, email is nice and all, but it is so obsolete technology.

And there are far too many "hotfixes" and additional layers of complexity just to keep it running these days in somewhat secure way. The protocol was simply not designed well, there is no security in mind, so many features were added on as afterthought and many things lack standardization.

I have underwent a process of creating my own email server and oh boy, that is a pain in the ass. So many loops and hoops you have to get though ...
```

```admonish warning
[aerc](https://aerc-mail.org/) is a work in progress.
```

```admonish warning
[aerc](https://aerc-mail.org/) is designed to work as online client - there is no offline storage by default.

[MailDir](https://en.wikipedia.org/wiki/Maildir) can be manually added by editing the config file, but it comes with few caveats.
```

```admonish warning
gmail does not work out of the box! You need 2FA (2 Factor Authentification) and special `app password`, but thankfully both of these are rather easy to setup.

It used to be enought to `Allow Less Secure Apps` in account settings, but that is no longer possible. See [aerc mailing list: invalid credentials](https://lists.sr.ht/~rjarry/aerc-discuss/%3CCAD_FeTfSntviRw02Q6PYHzuBbYijBHVt%3DcC-J%3Dyn9xKcMot%3DGw%40mail.gmail.com%3E) and [Less secure apps & your Google Account](https://support.google.com/accounts/answer/6010255)
```

What I will describe here is setup of `aerc` to work offline and with multiple accounts. Regarding email providers, I will try to get following to work:
- Google / Gmail
- Microsoft / Outlook
- Protonmail
- Tutanota
- My personal email server

And I will use the following software:
- [aerc](https://aerc-mail.org/) = the email client, will serve as interface to emails stored locally on disk
- [khard](https://archlinux.org/packages/community/any/khard/) = console address book
- [mbsync/isync with goimapnotify](https://wiki.archlinux.org/title/Isync#With_imapnotify) = synchronize mailboxes
	- I want to use `goimapnotify` to get push notifications (mostly to save bandwidth, but the immediate mail delivery is also nice)
- [keepassxc](https://archlinux.org/packages/community/x86_64/keepassxc/) = password manager (to avoid saving passwords in config files)
	- [Example of keepassxc with offlineimap](https://wiki.archlinux.org/title/OfflineIMAP#KeePassXC_with_Freedesktop.org_secret-service)
	- [Example of aerc with keepassxc](https://man.sr.ht/~rjarry/aerc/integrations/password-manager.md)
- [msmtp](https://wiki.archlinux.org/title/Msmtp) = SMTP client for sending emails (you need this for offline use)

```admonish tip
Given the modular nature of the setup, you can use other software.
```

Sources I have used for inspiration:
- [https://takashiidobe.com/gen/offline-email.html](https://takashiidobe.com/gen/offline-email.html)
- [Drew DeVault's blog post: aerc, mbsync, and postfix for maximum comfy offline email](https://drewdevault.com/2021/05/17/aerc-with-mbsync-postfix.html)
- [Google / Sign in with App Passwords](https://support.google.com/accounts/answer/185833)


## The structure

- all emails will be stored in [MailDir](https://en.wikipedia.org/wiki/Maildir) format for offline use
- the local `MailDir` will be synchronised with email server via `mbsync/isync`, triggered by `goimapnotify` using push notifications
- outgoing emails will be handled by `msmtp` which will queue them until online
- `aerc` will be acting as user interface
- `khard` will be used as address book
- `keepassxc` will store passwords for the email accounts

The MailDir can be anywhere, but I will use `~/.mail`, and each account will have it's subdirectory:
```
~/.mail
|-- account1
|   |-- INBOX
|   `-- ..
|-- account2
`-- ...
```


## First issues

The first problem that I can see is that there is a lot of redundant configuration. The email accounts are defined in practically each of the used programs. If you have multiple accounts, it will be a lot of work.

Config files that need account details:
- aerc: `~/.config/aerc/accounts.conf`
- mbsync/isync: `~/.mbsyncrc` (I will move this to `~/.config/mbsyn/mbsyncrc.conf` - do this in sync script)
- goimapnotify: `~/.config/imapnotify/<account>.conf`
- msmtp: `~/.config/msmtp/config`

I will start by configuring everything manually, one thing at the time, and slowly add on the layers. At the end, I will create a script for handling the configuration for me.

