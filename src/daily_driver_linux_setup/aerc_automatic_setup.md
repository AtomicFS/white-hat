# The automatic setup

![](../img/construction.gif)


Basic idea is to have single configuration file with definitions of all accounts, and then with script generate all other configuration files for all programs that require to account information (aerc, mbsync/isync, msmtp, ...).

I have written a Python script to handle the configuration, and it is placed at [https://git.sr.ht/~atomicfs/aerc-offline-configurator](https://git.sr.ht/~atomicfs/aerc-offline-configurator).

It uses [configparser](https://docs.python.org/3/library/configparser.html) to retrieve the data in a Python script. The script will then generate configuration files.



# stuff

- IMAP to get mail
- SMTP to send mail
- MailDir to store mail
- Push notifications


- For push notifications systemd services check out some kind of lock file

mbsync --config /tmp/pytest-of-atom/pytest-current/test__mbsync_livecurrent/mbsyncrc.conf/mbsyncrc.conf --list -a






## Gmail
- `INBOX`
- `[Gmail]/All Mail`
- `[Gmail]/Drafts`
- `[Gmail]/Important`
- `[Gmail]/Sent Mail`
- `[Gmail]/Spam`
- `[Gmail]/Starred`
- `[Gmail]/Trash`

## Protonmail
- `INBOX`
- `All Mail`
- `Archive`
- `Drafts`
- `Sent`
- `Spam`
- `Starred`
- `Trash`

## white
- `INBOX`
- `trash`
- `drafts`
- `junk`
- `sent`

## seznam
- `INBOX`
- `archive`
- `drafts`
- `sent`
- `spam`
- `trash`







