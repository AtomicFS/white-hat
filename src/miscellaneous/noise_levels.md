# Audio / Noise levels

Entire spectrum of possible noise level is show in table below, along with permissible exposure for some of the levels.

Sources:
- [Decibel exposure time guidelines](https://dangerousdecibels.org/education/information-center/decibel-exposure-time-guidelines/)
- [Noise level chart (archive.org)](https://web.archive.org/web/20211009141125/https://www.noisehelp.com/noise-level-chart.html)
- [Comparitive examples of noise levels (archive.org)](https://web.archive.org/web/20190518024505/http://www.industrialnoisecontrol.com/comparative-noise-examples.htm)

```admonish warning
There is no cure for hearing loss caused by excessive noise. Such type of damage is permanent damage.
```

| dB	| Example		| Effects			| Permissible exposure	|
|------:|:----------------------|:------------------------------|:----------------------|
| 0	|     			|				|			|
| 10	| Pin drop		|				|			|
| 20	| Rustling leaves	|				|			|
| 30	| Whisper		|				|			|
| 40	| Bird calls		|				|			|
| 60	| Conversation		|				|			|
| 70	| Shower		| Safe threshold		|			|
| 80	| Alarm clock		| Annoyingly loud		| 8 hours		|
| 90	| Squeeze toy		| Possible damage		| 2 hours		|
| 100	| Motorcycle		| Serious damage possible	| 15 minutes		|
| 110	| Rock band		| Pain threshold		| 2 minutes		|
| 120	| Thunderclap		| Painful			| less than 7 seconds	|
| 130	| Stadium crowd		| 				| no exposure		|
| 140	| Aircraft carrier deck	| Immediate nerve damage	|			|
| 150	| Jet-engine (at 25 meters) | Eardrum rupture		|			|
| 160	| Shotgun		|				|			|
| 170	| Safety airbag		|				|			|
| 180	| Rocket launch		|				|			|
| 194	| 			| Sound waves become shock waves	|		|

Agency (EPA) determined in 1974 that a 24-hour average noise exposure level of 70 dB or less prevented measurable hearing loss over a lifetime.

```admonish info
To protect yourself, avoid any exposure exceeding 85 dB. Sounds above 85 dB are considered harmful.
```

```admonish tip
If you find yourself near any of these without hearing protection, use your fingers to plug your ears and move away from the source of noise. Every meter of distance counts.
```

For every 3 dB over 85 dB, the permissible exposure time before possible damage can occur is cut in half.

| dB	| Permissible Exposure	|
|------:|:----------------------|
| 85	| 8 hours		|
| 88	| 4 hours		|
| 91	| 2 hours		|
| 94	| 1 hour		|
| 97	| 30 minutes		|
| 100	| 15 minutes		|
| 103	| 7.5 minutes		|
| 106	| 3.7 minutes		|
| 109	| 1.8 minutes		|
| 112	| 56 seconds		|
| 115	| 28 seconds		|

```admonish tip
After exposure to loud noise, recovery time is required, which can possibly reduce the damage.
```

