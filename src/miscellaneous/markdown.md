# Markdown

```admonish info
Markdown is a lightweight markup language for creating formatted text using a plain-text editor.
```

- [Markdown guide](https://www.markdownguide.org)
- [Markdown cheat-sheet](https://www.markdownguide.org/cheat-sheet/)

