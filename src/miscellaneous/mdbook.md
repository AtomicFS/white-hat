# mdbook


```admonish info
mdbook is a utility to create modern online books from Markdown files.
```

- [Repository](https://github.com/rust-lang/mdBook)
- [User guide](https://rust-lang.github.io/mdBook/)
- [3rd party plugins](https://github.com/rust-lang/mdBook/wiki/Third-party-plugins)

Below are few of my favorite plugins.


## mdbook-admonish

Adds support for Material Design admonishments, based on the mkdocs-material implementation.

- [Repository](https://github.com/tommilligan/mdbook-admonish)
- [Reference](https://tommilligan.github.io/mdbook-admonish/)


## mdbook-katex

A preprocessor rendering LaTex equations to HTML.

- [Repository](https://github.com/lzanini/mdbook-katex)


## mdbook-linkcheck

Checks for broken links.

- [Repository](https://github.com/Michael-F-Bryan/mdbook-linkcheck)

