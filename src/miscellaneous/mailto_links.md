# Mailto links

Mailto link has following format:
```
mailto:<email_address>,<email_address2>?<parameter>&<parameter2>
```

Recipients are separated by comma (`,`). You can have more than 2 recipients ;).

Additional parameters are possible, such as:
- cc (carbon copy)
- bcc (blind carbon copy)
- subject
- body

These additional arguments must be separated from the `to` field with question mark (`?`).

| | Example |
|:--|:--|
| Basic | [mailto:jack@example.com](mailto:jack@example.com) |
| Multiple recipients | [mailto:jack@example.com,mark@example.com](mailto:jack@example.com,mark@example.com) |
| CC | [mailto:jack@example.com?cc=mark@example.com](mailto:jack@example.com?cc=mark@example.com)
| BCC | [mailto:jack@example.com?bcc=mark@example.com](mailto:jack@example.com?bcc=mark@example.com)
| Subject | [mailto:jack@example.com?subject=subject%20here](mailto:jack@example.com?subject=subject%20here) |
| Body | [mailto:jack@example.com?Body=body%20here](mailto:jack@example.com?body=body%20here) |
| Everything | [mailto:jack@example.com,mark@example.com?cc=luke@example.com&bcc=tony@example.com&subject=subject%20here&body=body%20here](mailto:jack@example.com,mark@example.com?cc=luke@example.com&bcc=tony@example.com&subject=subject%20here&body=body%20here)

You are rather limited in the characters you can put into a HTML link.

```admonish info
At least according to [stackoverflow.com](https://stackoverflow.com/a/1547940) and this [Uniform Resource Identifier (URI): Generic Syntax](https://www.ietf.org/rfc/rfc3986.txt) document, following characters are allowed in HTML link:

~~~
ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~:/?#[]@!$&'()*+,;=
~~~
```

If you want other characters, or perhaps you would like to have a reserved character in the subject field, you have to encode the character into `%<hex_code>`.

Here is a short table of the most used hex codes (full table at [HTML URL Encoding Reference](https://www.w3schools.com/tags/ref_urlencode.asp)):

| Hex code | Character |
|:-----:|:---:|
| `%20` | ` ` |
| `%21` | `!` |
| `%2D` | `-` |
| `%2E` | `.` |
| `%3A` | `:` |
| `%3F` | `?` |


```admonish example
`subject=Hello there!` would become `subject=Hello%20there%21`
```

