# My favorite quotes

## Archimedes of Syracuse
> Give me the place to stand, and I shall move the earth.


## George Carlin
> Think of how stupid the average person is and then realise half of them are stupider than that.


## Shakespeare
Macbeth:
> When shall we three meet again in thunder, lightning, or in rain?
>
> When the hurlyburly's done, when the battle's lost and won.


## Tina Fey
Bossypants:
> Don't waste time telling crazy people they are crazy.


## Jan Samek
> If you don't control it, you don't own it. Then you have to pwn it.

> Multifunction is malfunction.

> A polished shit is still a shit.

> COBOL = Compiles Only By Odd Luck


## Markiplier
> In my defense, I was left unsupervised.


## Linus Torvalds
> Windows is an onion operating system. Layer upon layer of crap that never gets fixed.

> We all know Linux is great ... it does infinite loops in 5 seconds.


## Eleanor Roosevelt
> Learn from the mistakes of others. You can't live long enough to make them all yourself.


## Carl Sagan
> Extraordinary claims require extraordinary evidence


## Noam Chomsky
> If we don't believe in freedom of expression for people we despise, we don't believe in it at all.


## Thunderf00t
[Self-Powered TV: BUSTED!!!!](https://youtu.be/jRJvUGwbN3c?t=325):
> The only "get rich quick" scheme that works is selling "get rich quick" schemes that don't work.


## Unknown
> A friend is someone who will bail you out of jail. A best friend is the one sitting next to you.

> Dance like Nobody's Watching; Encrypt like Everyone is

Every sensible security technician
> Rule \#1 in crypto: never write your own crypto.

> There is nothing more permanent than temporary solution.


## Misc
Mordin Solus, Mass Effect 3
> Had to be me. Someone else might have gotten it wrong.

> You Must Learn from the Mistakes of Others. You Will Never Live Long Enough to Make Them All Yourself.

> Arguing with an idiot is like playing chess with a pidgeon. It'll just knock over all the pieces, shit on the board, and strut about like it's won anyway.

[Quote Origin](https://quoteinvestigator.com/2023/02/03/teachers-frogs/)
> There are two kinds of teachers: the kind that fill you with so much quail shot that you can’t move, and the kind that just give you a little prod behind and you jump to the skies.

Bender, Futurama
> Anything less than immortality is a complete waste of time.

