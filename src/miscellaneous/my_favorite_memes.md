# My favorite memes

Links to sources can be found in [repository](https://git.sr.ht/~atomicfs/white-hat/tree/master/item/src/img/memes).

![](../img/memes/6E7uS8g.png)

![](../img/memes/7p9btf6vjkha1.png)

[Command-line Tools can be 235x Faster than your Hadoop Cluster](https://adamdrake.com/command-line-tools-can-be-235x-faster-than-your-hadoop-cluster.html)

![](../img/memes/Ni9tt7JO4SZ8lCt-0hrgeaeWosG2ClpdYT3W6j2VVXQ.webp)

![](../img/memes/Programmers-Vs-Users.webp)

![](../img/memes/Windows_Vs_Mac_Vs_Linux_2.jpeg)

![](../img/memes/f63c2bf6d3448cd973ac7062de9d3707.jpeg)

![](../img/memes/lsmOStB.jpg)

![](../img/memes/mCU492aj4pfW45Vce1PPoX-dokepPXvaGbo0d08yONc.webp)

![](../img/memes/og_og_1511778483287484769.jpg)

![](../img/memes/r_1802151_KnJ9Y.jpg)

![](../img/memes/r_391740_BJMKi.jpg)


