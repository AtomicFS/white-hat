# Adrian Lamo

![](https://img.ibxk.com.br/2018/03/16/16194445730439.jpg)

```admonish info
[Adrián Alfonso Lamo Atwood](https://en.wikipedia.org/wiki/Adrian_Lamo) (February 20, 1981 – March 14, 2018) was an American threat analyst and hacker. Lamo first gained media attention for breaking into several high-profile computer networks, including those of The New York Times, Yahoo!, and Microsoft, culminating in his 2003 arrest.

Lamo was best known for reporting U.S. soldier Chelsea Manning to Army criminal investigators in 2010 for leaking hundreds of thousands of sensitive U.S. government documents to WikiLeaks. Lamo died on March 14, 2018, at the age of 37.
```

Also known as the homeless hacker.


# Known for

Adrian Lamo was known for unconventional life-style, mostly [couch-surfing](https://en.wikipedia.org/wiki/Couch-surfing) or [squatting](https://en.wikipedia.org/wiki/Squatting). Lamo was using rather simple and primitive methods of gaining access to restricted information. Most of the time, he used only web browser.


# Additional sources

- [Wikipedia](https://en.wikipedia.org/wiki/Adrian_Lamo)
- [The Screen Savers interview](https://www.youtube.com/watch?v=6oHAPMEKvdM)
- YouTube video: [Who is Adrian Lamo?](https://www.youtube.com/watch?v=sGTYyltYi80)
- CBS News report: [How Does Wikileaks Get Its Information](https://www.youtube.com/watch?v=Po-VxrDc0s4)
- YouTube video: [Scary Mysteries: Homeless White Hat Hacker - Adrian Lamo](https://www.youtube.com/watch?v=LDp3zZTPgBU)

