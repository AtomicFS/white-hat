# VPN

![](../img/construction.gif)


First of all, let me say this: I see ads and promotions for NordVPN, Surfshark VPN and other all the time, and I get triggered.

They increase your security only in specific cases! To use VPN all the time is stupid, unless you live in country with heavy censorship.


Bullshit arguments to use VPN:
- Your internet provider can see all you traffic, they are not trustworthy
	- When using VPN service, the VPN provider can see you traffic instead. Is your VPN provider trustworthy?
	- Remember: **If you are not paying for the product, you are the product.** However high product price and selling your data is not mutually-exclusive.
- We do not log any data
	- This is a straight up lie. All providers log data, the question is which data and how long they keep it. Check the VPN provider privacy policy.
- You are anonymous with VPN
	- No you are not, you only hide your IP address.
- Hackers
	- ... eeehh ... where do I even start ...

```admonish warning
VPN is not a privacy tool.
```

Valid reasons to use VPN:
- Bypass [geo-blocking](https://en.wikipedia.org/wiki/Geo-blocking)
- When using public hotspots while traveling


VPN service alternatives:
- Run your own VPN server
	- With public IP address at your home, you can run VPN server at your home. Which is great because you get access to your home network, and it runs on hardware fully under your control.
	- You can also install VPN into [VPS](https://en.wikipedia.org/wiki/Virtual_private_server). Then the question again is if you trust the VPS provider. If you can install your own system and configure it to your liking (disk encryption, etc), then I guess it is better than nothing.
- Use TOR or other decentralized network

