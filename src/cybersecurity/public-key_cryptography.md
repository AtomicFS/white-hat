# Public-key cryptography

![](../img/construction.gif)


## Brief comparison of SSH keys

[Comparing SSH Keys](https://goteleport.com/blog/comparing-ssh-keys/) is a quite nice comparison, which could be summarized like so:
- `DSA`
	- quite bad history of security problems caused by bad implementations
- `ECDSA` / `ECDSA-SK`
	- uses elliptic curve
	- in essence elliptic curve implementation of `DSA`, improves speed but not security compared to `DSA`
- `Ed25519` / `Ed25519-SK`
	- uses elliptic curve
	- considered more secure than `ECDSA`
	- quite popular
	- **really good go-to key type**
- `RSA`
	- most widely adopted
	- Considered secure if at least 3072 bits long

```admonish note
The `*-SK` versions have fixed length.
```

```admonish tip
`ed25519` is a good choice, got with it is possifle. It has good security and good adoption.

`ecdsa` is also decent choice, but adoption is not great.

`rsa` is OK if you use keys of at least 3072 bits long. It is compatible with almost everything.

`dsa` should be last resort.
```


## `ECDSA` vs `ECDH` vs `Ed25519` vs `Curve25519`
- [Curve25519](https://en.wikipedia.org/wiki/Curve25519) is a elliptic curve (mathematical thing).
- [ECDH](https://en.wikipedia.org/wiki/Elliptic-curve_Diffie%E2%80%93Hellman) is protocol for key exchange for elliptic-curve algorithms.
- [EdDSA](https://en.wikipedia.org/wiki/EdDSA) is generic algorithm for public key cryptography using curves.
- [Ed25519](https://en.wikipedia.org/wiki/EdDSA#Ed25519) is a `EdDSA` using `Curve25519` elliptic-curve.
- [ECDSA](https://en.wikipedia.org/wiki/Elliptic_Curve_Digital_Signature_Algorithm) is re-implementation of [DSA](https://en.wikipedia.org/wiki/Digital_Signature_Algorithm) with `P-256` elliptic-curve.


