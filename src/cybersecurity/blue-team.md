# Blue-team

![](../img/construction.gif)


- Intrusion Detection System (IDS)
- Intrusion Prevention System (IPS)
- LIDS - Log-Based Intrusion Detection

- Host-based intrusion detection system (HIDS)
- Network intrusion detection systems (NIDS)

Keep in mind, if you add it, you have to also maintain it and monitor it. So more is not always better.


## rkhunter

[rkhunter](https://wiki.archlinux.org/title/Rkhunter) sounds like nice thing to have, but it is not really. Basically it is obsolete and unmaintained anti-virus.

First of all, it is so obsolete it is painful.
- Scans from user-space, meaning it cannot detect kernel root-kits
- Signature-based detection is obsolete
- It is abandon-ware, last release was in 2018, since then there was almost no development and no issues get fixed.


## chkrootkit

The same story as for rkhunter, but worse. The output is complete unusable rubbish.


## Snort vs Suricata

Sure their use is limited since a lot of traffic is being encrypted nowadays, but not all traffic. Super handy to deal with [Layer 3](https://en.wikipedia.org/wiki/Network_layer) attacks.

Often you hear argument against Snort because of being single-threaded, and Suricata multi-threaded:
- Snort <2.x is single-threaded
- Snort >3.0 is multi-threaded
- Suricata is multi-threaded

single-threaded IDP / IPS will negatively impact your network performace!

- [Open source IDS: Snort or Suricata?](https://resources.infosecinstitute.com/topic/open-source-ids-snort-suricata/)


- [Blue Team Training Course / Introduction](https://www.youtube.com/watch?v=Bt5fh3wQUAQ&list=PLBf0hzazHTGNcIS_dHjM2NgNUFMW1EZFx&index=2)


## Suricata

```
+----+    +--------+                    +--------+
| PC |<-->| Switch |<------------------>| Router |<--> Internet
+----+    +--------+                    +--------+

+----+    +--------+    +----------+    +--------+
| PC |<-->| Switch |<-->| Suricata |<-->| Router |<--> Internet
+----+    +--------+    +----------+    +--------+
```

