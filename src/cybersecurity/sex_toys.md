# Sex toys

![](../img/construction.gif)


![](../img/pegi-18.png)
![](../img/pegi-18.png)
![](../img/pegi-18.png)

Cybersecurity is generally poor when it comes to commercial products. However when the product can cause a serious harm, it is a big deal.

Some sex toys have companion applications for phones, leaking data. Moreover, some even feature cameras and microphones, often for [virtual sex](https://en.wikipedia.org/wiki/Virtual_sex) or [cybersex](https://en.wikipedia.org/wiki/Cybersex)). These can be accessed by 3rd party and used for blackmail or [revenge porn](https://en.wikipedia.org/wiki/Revenge_porn).

Other sex toys feature physical properties, which can under some circumstances, cause physical damage. One famous example is a chastity cage (also known as [male chastity belt](https://en.wikipedia.org/wiki/Chastity_belt_(BDSM)#Male_chastity_cages)) when [attacker can lock down your genitals](https://www.pcmag.com/news/flaw-in-sex-toy-can-let-a-hacker-literally-lock-down-your-crotch).

![](https://i.pcmag.com/imagery/articles/06lXUzhtglA0P2aXi2ITw9N-1.fit_lim.size_1600x900.v1602020161.jpg)

And so far the scariest one it a [pear flower anal plug](https://twitter.com/QIUI13/status/1306055991935422465) made by QIUI. Product inspired by [pear of anguish](https://en.wikipedia.org/wiki/Pear_of_anguish) (medieval torture device). This one is potentially very dangerous toys which could cause a serious harm, since it can open up to 180 degrees. However I was unable to figure out if the actuation is handled via some motor, or if it is manual.

![](https://cdn.shopify.com/s/files/1/2330/6113/products/QIUI-anal-Plug-toy-BDSM-app-remote-control-Stretching-Anus-Expander-Chain-Butt-Tail-Chastity-slave_jpg_Q90_jpg.webp)


## Bluetooth

The devices often connect to smart phones via [Bluetooth](https://en.wikipedia.org/wiki/Bluetooth), which is known for it's flawed design and terrible security model (for example [bluesnarfing](https://en.wikipedia.org/wiki/Bluesnarfing)).


## Projects

- [The Internet Of Dongs Project](https://internetofdon.gs/) is a project to hack sex toys to make them safer and more private
- [Pen Test Partners](https://www.pentestpartners.com/) provides cyber security consulting and testing to a huge variety of industries and organisations
- [Privacy Not Included](https://foundation.mozilla.org/en/privacynotincluded/) is web with product reviews by expert
	- [Privacy Not Included / sex-toys](https://foundation.mozilla.org/en/privacynotincluded/categories/sex-toys/)


## Articles

- [Why smart sex toys can do a whole lot of harm ... to your cyber security](https://web.archive.org/web/20220527024829/https://www.itnews.asia/news/why-smart-sex-toys-can-do-a-whole-lot-of-harmto-your-cyber-security-562210)
- [We need to talk about sex toys and cyber security](https://www.pentestpartners.com/security-blog/we-need-to-talk-about-sex-toys-and-cyber-security/)
	- YouTube video [We need to talk about smart sex toys and cyber security by Ken Munro](https://www.youtube.com/watch?v=md_Gtjd_EeM)
- [Sex in the digital era – ESET reveals new research into security of smart sex toys](https://www.eset.com/us/about/newsroom/press-releases/sex-in-the-digital-era-eset-reveals-new-research-into-security-of-smart-sex-toys-1/)
- [Sex in the digital era: How secure are smart sex toys?](https://www.welivesecurity.com/2021/03/11/sex-digital-era-how-secure-are-smart-sex-toys/)
- [Don’t Get Your Valentine an Internet-Connected Sex Toy](https://12ft.io/proxy?q=https%3A%2F%2Fwww.wired.com%2Fstory%2Finternet-connected-sex-toys-security%2F)
- [Internet Of Dildos: A Long Way To A Vibrant Future – From IoT To IoD](https://sec-consult.com/blog/detail/internet-of-dildos-a-long-way-to-a-vibrant-future/)

