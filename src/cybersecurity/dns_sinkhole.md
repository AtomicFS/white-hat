# DNS sinkhole

```admonish info title="DNS sinkhole"
A DNS sinkhole, also known as a sinkhole server, Internet sinkhole, or Blackhole DNS[1] is a Domain Name System (DNS) server that has been configured to hand out non-routable addresses for a certain set of domain names.

[Source](https://en.wikipedia.org/wiki/DNS_sinkhole)
```

In essence, DNS sinkhole is a DNS server with whitelist and/or blacklist of domains to be blocked. It is often used to block advertisements, malicious domains and [botnets](https://en.wikipedia.org/wiki/Botnet).

Most notably [WannaCry](malware__wannacry.md) was stopped by DNS sinkhole thanks to discovery of hard-coded kill switch.

```admonish info title="DNS"
The Domain Name System (DNS) is a hierarchical and distributed naming system for computers, services, and other resources in the Internet or other Internet Protocol (IP) networks.

It associates various information with domain names assigned to each of the associated entities. Most prominently, it translates readily memorized domain names to the numerical IP addresses needed for locating and identifying computer services and devices with the underlying network protocols.

[Source](https://en.wikipedia.org/wiki/Domain_Name_System)
```

DNS in essence translates domain (such as `gitlab.com`) into IP address (such as `172.65.251.78`). Device sends a request to DNS server to resolve a domain, in return DNS server returns IP address.

```
+-------------+   +---------+
|   PC        |<->|  DNS    |
| 192.168.1.2 |   | 1.1.1.1 |
+-------------+   +---------+

gitlab.com     -->
               <-- 172.65.251.78

blockthis.com  -->
               <-- 199.59.243.222
```

DNS sinkhole is essentially Man-in-the-Middle. You set your device to use DNS sinkhole server as DNS server (this can be done via [DHCP](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol)).

The DNS sinkhole will first check it's blacklist for requested domain, if present it returns non-routable addresses (such as `0.0.0.0`), if not present it forwards the request to upstream DNS server.

```
+-------------+   +--------------+   +---------+
|   PC        |<->| DNS sinkhole |<->|  DNS    |
| 192.168.1.2 |   | 192.168.1.1  |   | 1.1.1.1 |
+-------------+   +--------------+   +---------+

gitlab.com     --> gitlab.com     -->
               <-- 172.65.251.78  <-- 172.65.251.78

blockthis.com  -->
               <-- 0.0.0.0
```



## Advertisements

Probably most common use of DNS sinkhole by common folk is to block ads on the internet. When it comes to blocking ads, the most common way are browser extensions or addons. In my opinion you should use both.

While browser extensions are great, there are few advantages to use DNS sinkhole.

**Pros** of DNS sinkhole over browser extension:
- Faster browsing and lower internet bandwidth
	- With extension, you computer has to resolve the domain and download the content, which then gets hidden by the extension. *Although this depends on the combination of browser + extension + specific web.*
	- With DNS sinkhole, the domain is resolved to non-routable addresses (which is very fast since the DNS sinkhole is usually on local network) and no content is downloaded.
- Any device or program on the network can be used with DNS sinkhole (browsers, programs, smartphones, smart TVs, ... you name it)
	- Yes, you can get rid off mobile ads without installing any additional software to your device.

```admonish warning
Unfortunately some devices come with hard-coded DNS server, and therefore DNS sinkhole will not work, for example Chromecast.

But it might be possible to change the DNS somewhere in settings.
```


**Cons** of DNS sinkhole over browser extension:
- Granting temporary exceptions can be pain (especially for non-admin users)


## Available DNS sinkholes

There are public DNS servers offer filtering, for example [CloudFlare DNS servers](https://blog.cloudflare.com/introducing-1-1-1-1-for-families/):

| DNS IP  | Filtering |
|:--------|:----------|
| 1.1.1.1 | None      |
| 1.1.1.2 | Malware   |
| 1.1.1.3 | Malware and Adult content |

```admonish note
CloudFlare is rather controversial. Mostly with claims of censorship, abusive monopoly and censorship.

Personally, I do not know what to make of it, I have not done my research yet.

- [Reddit post in `r/privacy`](https://www.reddit.com/r/privacy/comments/r1bct2/cloudflares_abusive_monoply_on_the_www/)
	- [Questionable rant over CloudFlare](https://framagit.org/dCF/deCloudflare/-/blob/master/readme/en.md)
- [More CloudFlare reddit posts](https://www.reddit.com/r/privacy/search/?q=Cloudflare)
```

Alternatively you can self-hosted DNS sinkholes:

- [Pi-hole](https://wiki.archlinux.org/title/Pi-hole) - I use this one, described in [Pi-hole section](../self_hosting/pi-hole.md)
- [AdGuardHome](https://github.com/AdguardTeam/AdGuardHome)
- [Technitium DNS](https://technitium.com/dns/)
- [Blocky](https://github.com/0xERR0R/blocky)

