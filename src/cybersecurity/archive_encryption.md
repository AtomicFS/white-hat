# Archive encryption

It is easy to protect archive with a password, however common process will not encrypt the header (which contains the tree structure with file names).


# 7zip

```
\$ 7za -p'password' -mhe=on a archive_name.7z folder_name
\$ 7za -p -mhe=on a archive_name.7z folder_name
```

Where `-p'password'` sets the password and `-mhe=on` enables encryption of the header. If there is no string with password, user will be prompted to enter it - this way it will not be logged in terminal history.


