# Kevin Mitnick

![](../img/cybersecurity/kevin_mitnick.jpg)

```admonish info
[Kevin Mitnick](https://en.wikipedia.org/wiki/Kevin_Mitnick) (born August 6, 1963) is an American computer security consultant, author, and convicted hacker. He is best known for his high-profile 1995 arrest and five years in prison for various computer and communications-related crimes.
```

# Known for

- 1979: Social engineering into getting developer account at [DEC](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation) because of peer pressure.
- 1981: Stealing computer manuals from [Pacific Bell](https://en.wikipedia.org/wiki/Pacific_Bell)'s switching center.
- 1982: Breaking into computers at the University of Southern California.
- 1987: Gained access into computers of a Santa Cruz software publisher.
- 1988: Going into [DEC](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation) again to obtain copy of [RSTS/E operating system](https://en.wikipedia.org/wiki/RSTS/E) ([Source](https://passwordresearch.com/stories/story47.html)).
- 1992: Gained access into [Pacific Bell](https://en.wikipedia.org/wiki/Pacific_Bell) voicemail computers to keep an eye on law enforcement personnel monitoring him.

The [Pacific Bell](https://en.wikipedia.org/wiki/Pacific_Bell) voicemail's confirmed his suspicions and Kevin decided to run. In 1992 Kevin Mitnick became the most wanted hacker in the world.

Kevin Mitnick was arrested multiple times, but shit hit the fan in 1995 when he got arrested by [FBI](https://en.wikipedia.org/wiki/FBI). He was put into prison for 4 years without trial, without bail hearing. In the end spent 5 years in prison, out of which 8 months were in solitary confinement.

This spawned [Free Kevin](https://www.linuxjournal.com/article/5052) movement, as the hacking community revolted against what they called a gross injustice.

There are multiple sources regarding Kevin Mitnick's story, most of it is from [John Markoff's book Takedown](https://en.wikipedia.org/wiki/John_Markoff), which was later made into movie [Track Down (2000)](https://en.wikipedia.org/wiki/Track_Down). Most of the mentioned allegations are unfounded gossip at best. And as expected, Kevin Mitnick claims that John Markoff's version of the story is fundamentally inaccurate. I recommend to watch documentary [Freedom Downtime (2001)](https://www.imdb.com/title/tt0309614/), which was re-released in 2004 with additional footage and hour long interview with Kevin.


# Currently

> Since 2000, Mitnick has been a paid security consultant, public speaker, and author. He does security consulting for, performs penetration testing services, and teaches social engineering classes to companies and government agencies. His company Mitnick Security Consulting is based in Las Vegas, Nevada where he currently resides.
> 
> [Source](https://en.wikipedia.org/wiki/Kevin_Mitnick#Consulting)

# Additional sources

- [Wikipedia](https://en.wikipedia.org/wiki/Kevin_Mitnick)
- [Kevin Mitnick - The Most Infamous Hacker of All Time](https://cyberexperts.com/kevin-mitnick-the-most-infamous-hacker-of-all-time/)
- [YouTube documentary on the life of Kevin Mitnick](https://www.youtube.com/watch?v=Qe73tRTksf0)
- Documentary about Free Kevin movement: [Freedom Downtime (2001)](https://www.imdb.com/title/tt0309614/)
	- [Bonus interview with Kevin Mitnick from Freedom Downtime 2004 DVD release](https://www.youtube.com/watch?v=HZB1WYk3GG0)


## Funny stories

- [Hacking McDonald's drive through](https://www.youtube.com/watch?v=Lat48rrtFto)
- [Hacking from Solitary Confinement](https://www.youtube.com/watch?v=a9hq7dFERBM)
- [FBI doughnuts](https://www.youtube.com/watch?v=-dSiXmUI6qU)

