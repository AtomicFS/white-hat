# Automatic installation

While I love the installation process of Arch Linux, once you figure out how you like your systems to be installed and structured, any followup installation is just a chore.

It helps greatly to write down some notes or instructions, just like I did in [manual installation](installation_manual.md) section, it is time-consuming. And what is the best way to kill time ... I mean save time? Automation baby!


# `atomicfs-install`

First comes the install script [atomicfs-install](https://git.sr.ht/~atomicfs/atomicfs-repo-arch/tree/master/item/packages/archlinux/x86_64/atomicfs-install).

```admonish warning
The script is very naive and simple, since I require almost no variation between installations.
```

I have it in form of `pacman` package.
```toml
{{#include ../../submodules/atomicfs-repo-arch/packages/archlinux/x86_64/atomicfs-install/PKGBUILD}}
```

[The code](https://git.sr.ht/~atomicfs/atomicfs-repo-arch/tree/master/item/packages/archlinux/x86_64/atomicfs-install/src/atomicfs-install.sh) is quite straight forward, mostly just commands I would write manually into console. It has few additional features such as resume function (thanks to flag-files).

```admonish info
Flag file is usually empty file used to alter programs behaviour. Basically:
- IF file exists: do something
- IF file does not exists: do something else

It is super handy to get some persistence across multiple execution or instances.
```

Then there is also very important point of downloading and setting up my dotfiles. For this, I need to supply few special things such as SSH keys, this is supplied in encrypted archive `data.7z`. The archive is then decrypted and extracted during the installation.


# Installation managed by meta packages

Most of the magic comes from the custom [meta packages](meta_package.md) which can be found in [atomicfs-repo-arch](https://git.sr.ht/~atomicfs/atomicfs-repo-arch/tree/master/item/packages/archlinux/x86_64) (packages named `wht_*`).

My meta-packages have following dependency structure:
```
{{#include ../_notes_/meta_packages.txt}}
```

![](../img/archlinux/meta_packages.png)

For example the `wht_system` which defines the minimum to get working system, and some handy tools on top - if you install this, you get working system.

Other packages are building on top of `wht_system` and each other. For example if I want to install system on server, I simply install only `wht_server` - since it depends on `wht_system` it will install that too automatically.

Or if I want to install system with `sway` and my favorite desktop tools (like web browser, office suite, etc) I simply install only `wht_desktop` - this will automatically also install `wht_sway` and `wht_system`.

Thanks to the dependency management, you can even set up such things like conflicts -  you can specify that server and desktop are mutually exclusive. For more details checkout Arch Linux wiki [creating packages](https://wiki.archlinux.org/title/Creating_packages) and [package guidelines](https://wiki.archlinux.org/title/Arch_package_guidelines).


# `atomicfs-archiso`

And finally comes the [atomicfs-archiso](https://git.sr.ht/~atomicfs/atomicfs-archiso), which is a project for remastering Arch Linux ISO disc to include my installation script.

When the disc is booted, simply run the `atomicfs-install` with proper arguments and let it do it's magic. In few minutes, your system will be up and running ;)

