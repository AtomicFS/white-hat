# [pacman](https://wiki.archlinux.org/title/Pacman)


![](../img/construction.gif)


---
# [AUR helpers](https://wiki.archlinux.org/title/AUR_helpers)
---

## [yay](https://aur.archlinux.org/packages/yay/)
Skip integrity check (useful when importing GPG keys is not working):
```
$ yay -S --mflags --skipinteg <PACKAGE>
```


---
# Troubleshooting

When upgrading the system with `pacman -Syu` or installing new packages, and you get a lot of errors in the stage `checking package integrity` that look soimething like this:
```
error: glibc: signature from "NAME NAME <EMAIL@EXAMPLE.com>" is unknown trust
:: File /var/cache/pacman/pkg/PACKAGE.pkg.tar.zst is corrupted (invalid or corrupted package (PGP signature)).
```

Then you have obsolete pacman keys for signature verification.

```admonish success title="Solution"
~~~
# pacman -S archlinux-keyring
# pacman-key --refresh-keys
~~~

The `refresh-keys` might take a while and you might see a lot of errors - this is fine.
```

