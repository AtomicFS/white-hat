# Meta package

[Meta package](https://wiki.archlinux.org/title/Meta_package_and_package_group) in a package with not code or executable, but only dependencies.

I use these to define sets of packages to maintain my machines.

