# pacman: custom packages

![](../img/construction.gif)


[makepkg](https://wiki.archlinux.org/title/Makepkg)

[pacman/Package signing](https://wiki.archlinux.org/title/Pacman/Package_signing)

[pacman.conf](https://man.archlinux.org/man/pacman.conf.5)

[GnuPG](https://wiki.archlinux.org/title/GnuPG)

