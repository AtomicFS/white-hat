# ArchLinux {{footnote: `neofetch --ascii_distro Arch -L`}}

```
                   -`
                  .o+`
                 `ooo/
                `+oooo:
               `+oooooo:
               -+oooooo+:
             `/:-:++oooo+:
            `/++++/+++++++:
           `/++++++++++++++:
          `/+++ooooooooooooo/`
         ./ooosssso++osssssso+`
        .oossssso-````/ossssss+`
       -osssssso.      :ssssssso.
      :osssssss/        osssso+++.
     /ossssssss/        +ssssooo/-
   `/ossssso+/:-        -:/+osssso+-
  `+sso+:-`                 `.-/+oso:
 `++:.                           `-/+/
 .`                                 `/
```

[Arch Linux](https://archlinux.org/) is a highly customizable distribution and as such it does not have any graphical installer. Everything is done manually in terminal. As a result it is highly flexible and lean distribution.

> Lightweight and flexible Linux distribution that tries to Keep It Simple

```admonish
So far my favorite distro.
```

```admonish info
`Arch Linux` is what you make it.
```


