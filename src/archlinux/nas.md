# NAS (Network-Attached Storage)

![](../img/construction.gif)


```admonish info
`Arch Linux` is what you make it.
```

## Add disk

### mdadm

[According to archlinux wiki](https://wiki.archlinux.org/title/RAID#Adding_a_new_device_to_an_array):
```
mdadm --add /dev/md0 /dev/sdc1
mdadm --grow /dev/md0 --raid-devices=4
```

Check status:
```
cat /proc/mdstat
mdadm --misc --detail /dev/md0
```

Wait until the process is finished.

### LUKS / dmcrypt

Increase size:
```
cryptsetup --verbose resize <luks_name>
```

Check state:
```
cryptsetup status <luks_name>
```

### BTRFS

```
btrfs filesystem resize max <mountpoint>
```

