# Gaming

![](../img/construction.gif)


## Disctrobox

[Issue with Archlinux](https://github.com/89luca89/distrobox/issues/382)

```
distrobox create --image archlinux:latest --name gaming --pre-init-hooks "pacman -Syy; pacman -S --noconfirm archlinux-keyring; pacman-key --init; pacman-key --populate archlinux; pacman -Syu --noconfirm" --volume /data/games/steam:/data/games/steam
```

/etc/pacman.conf

[multilib]
Include = /etc/pacman.d/mirrorlist

sudo pacman -Syu
sudo pacman -S steam

distrobox-export --app steam





```
sudo pacman -S flatpak
```

Select `xdg-desktop-portal-wlr` and `wireplumber`.

Follow steps in [flatpak installation](https://wiki.archlinux.org/title/Steam#Alternative_Flatpak_installation)
```
flatpak --user remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak --user install flathub com.valvesoftware.Steam
flatpak run com.valvesoftware.Steam
```


