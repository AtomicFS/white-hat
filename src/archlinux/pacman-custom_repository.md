pacman: custom repository
=========================

![](../img/construction.gif)


To simplify installation if custom packages, you can create custom repository.

I am going to use following structure:
- [git.sr.ht/~atomicfs/atomicfs-repo-arch](https://git.sr.ht/~atomicfs/atomicfs-repo-arch)
	- the main repository with all the code and packages
	- upon trigger it builds new versions of packages and `git push --force` into gitlab
- [gitlab.com/AtomicFS/atomicfs-repo-arch-gitlab](https://gitlab.com/AtomicFS/atomicfs-repo-arch-gitlab)
	- repository used only to host the content in `pages`


---
# GPG signature
---

Firstly create a PGP key ([guide](https://wiki.archlinux.org/title/GnuPG#Create_a_key_pair)):
```
gpg --full-gen-key
```

Upon successful creation you should see following:
```
pub   rsa3072 2022-04-09 [SC] [expires: 2023-04-09]
      B941681D8C7FE3876F84F9BEE0C66981D83CBF4A
uid                      Vojtech Vesely (custom_archlinux_packages) <vojtech_vesely@white-hat-hacker.icu>
sub   rsa3072 2022-04-09 [E] [expires: 2023-04-09]
```

Where the `B941681D8C7FE3876F84F9BEE0C66981D83CBF4A` is the `user-id`.

Export public and private key:
```
gpg --export --armor --output archlinux_packages.pub.key B941681D8C7FE3876F84F9BEE0C66981D83CBF4A
gpg --export-secret-keys --armor --output archlinux_packages.priv.asc B941681D8C7FE3876F84F9BEE0C66981D83CBF4A
```

You can also publish it:
```
gpg --send-keys B941681D8C7FE3876F84F9BEE0C66981D83CBF4A
```

Add the private key into `secrets`:

![](../img/srht-secret-add_gpg_key.png)

Then the new secret will get assigned UUID (in this case `d146a201-579a-4f45-b097-b282ca4c6bd7`):

![](../img/srht-secret-added_gpg_key.png)

Add this secret into the `secrets` section in the `build.yml` file:
```yml
secrets:
  - d146a201-579a-4f45-b097-b282ca4c6bd7
```

And then import the key before building the packages:
```
gpg --import ~/.gpg/archlinux_packages.priv.asc
```

---
# Add the repository in system
---

Edit `pacman`'s configuration file and add the repository:
```
[atomicfs-repo-arch]
Server = https://atomicfs.gitlab.io/atomicfs-repo-arch-gitlab/x86_64/
SigLevel = Required DatabaseOptional
```

Or you can point your own domain to it:
```
[atomicfs-repo-arch]
Server = https://repo-arch.white-hat-hacker.icu/x86_64/
SigLevel = Required DatabaseOptional
```

Import the public key to your other systems:
```
pacman-key --recv-key B941681D8C7FE3876F84F9BEE0C66981D83CBF4A --keyserver keyserver.ubuntu.com
pacman-key --lsign-key B941681D8C7FE3876F84F9BEE0C66981D83CBF4A
```

And you are done, now you can install packages from your own repository.

