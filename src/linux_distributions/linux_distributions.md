# Linux Distributions

```admonish warning title="DISCLAIMER"
These are purely my opinions. If you disagree, good for you.
```

---
# [ArchLinux](https://archlinux.org/) {{footnote: `neofetch --ascii_distro Arch -L`}}

```
                   -`
                  .o+`
                 `ooo/
                `+oooo:
               `+oooooo:
               -+oooooo+:
             `/:-:++oooo+:
            `/++++/+++++++:
           `/++++++++++++++:
          `/+++ooooooooooooo/`
         ./ooosssso++osssssso+`
        .oossssso-````/ossssss+`
       -osssssso.      :ssssssso.
      :osssssss/        osssso+++.
     /ossssssss/        +ssssooo/-
   `/ossssso+/:-        -:/+osssso+-
  `+sso+:-`                 `.-/+oso:
 `++:.                           `-/+/
 .`                                 `/
```

| Last used         | Today  |
|-------------------|--------|
| Daily driver from | 2019   |
| Daily driver to   | now    |

While very difficult to get started, it is worth it for me.

I failed miserably multiple times trying to install it. And even with some helping hand it took me over 2 or 3 days to get functional system up and running. Thanks to this distro, I have learned a lot about GNU/Linux and how to tweak it.


---
# [Debian](https://www.debian.org/) {{footnote: `neofetch --ascii_distro Debian -L`}}

```
       _,met\$\$\$\$\$gg.
    ,g\$\$\$\$\$\$\$\$\$\$\$\$\$\$\$P.
  ,g\$\$P"     """Y\$\$.".
 ,\$\$P'              `\$\$\$.
',\$\$P       ,ggs.     `\$\$b:
`d\$\$'     ,\$P"'   .    \$\$\$
 \$\$P      d\$'     ,    \$\$P
 \$\$:      \$\$.   -    ,d\$\$'
 \$\$;      Y\$b._   _,d\$P'
 Y\$\$.    `.`"Y\$\$\$\$P"'
 `\$\$b      "-.__
  `Y\$\$
   `Y\$\$.
     `\$\$b.
       `Y\$\$b.
         `"Y\$b._
              `"""
```

| Last used         | 2022 |
|-------------------|------|
| Daily driver from | ???? |
| Daily driver to   | ???? |

I have never really used Debian as my daily driver. It was always for some project (customer's PC, docker image, etc).

Well, this is a distribution that I hate for multiple reasons. While I have not used it a lot, but still came across few caveats.

Here are the reasons why I hate it:
- Default `PATH` variable is wrong
	- `PATH` should by default include location of system/admin software such as `/usr/bin` simply because user should be just warned of insufficient privileges, instead of error saying that the software is not installed.
	- Best option is to fix `PATH`, but if you don't do that (and let's assume that most new users will not) then they are going to run either most commands as `root` just to simplify things. Which is stupid.
	- **Message to Debian:** fix `PATH` variable
- Packaging is pain
	- I have tried to create my own packages for `Debian` in the past and ended up without will to live.
	- The official documentation is over complicated and also practically useless. Unofficial documentation is fragmented, obsolete, incomplete.
	- **Recommendation:**
		- Look into some widelly used `Debian` package and copy the structure
		- Make package for another distro and then use some script to convert it
		- Ignore all the documentation, just start with blank directory and add files based on error messages
- Obsolete software
- Installer by default uses kernel names instead of `UUID`'s in `/etf/fstab`
	- Kernel names can and do change over time (when new hardware is added, etc), so it is common practice to use `UUID` instead because that will remain the same across all computers and all distributions. But no, `Debian` has to use by default kernel names. This will mess things up when for example a virtual machine create in VirtualBox will be started in `KVM/QEMU` (kernel name in `VirtualBox` will be `/dev/sda` while in `KVM/QEMU` it will be `/dev/vda`).
	- **Message to Debian:** use `UUID` like a normal person
- Remastering installation disk is [F.U.B.A.R](https://www.urbandictionary.com/define.php?term=F.U.B.A.R){{footnote: `Fucked up beyond all recognition` or `Fucked up beyond all repair`}}
	- I have tried to change partitioning scheme and so I looked into documentation and changed the configuration files. And it installed itself, but with default partitioning scheme. I have spent a lot of time on this and concluded that the documentation is wrong and the software is garbage. When I tried to copy-paste example config from documentation without any changes, it also failed claiming that the configuration is invalid.
	- **Recommendation:** avoid

In conclusion, this distribution is broken by default in so many aspects. It has old software and even older documentation.


---
# [Fedora](https://getfedora.org/) {{footnote: `neofetch --ascii_distro Fedora -L`}}

```
          /:-------------:\
       :-------------------::
     :-----------/shhOHbmp---:\
   /-----------omMMMNNNMMD  ---:
  :-----------sMMMMNMNMP.    ---:
 :-----------:MMMdP-------    ---\
,------------:MMMd--------    ---:
:------------:MMMd-------    .---:
:----    oNMMMMMMMMMNho     .----:
:--     .+shhhMMMmhhy++   .------/
:-    -------:MMMd--------------:
:-   --------/MMMd-------------;
:-    ------/hMMMy------------:
:-- :dMNdhhdNMMNo------------;
:---:sdNMMMMNds:------------:
:------:://:-------------::
:---------------------://
```

| Last used         | 2019 |
|-------------------|------|
| Daily driver from | 2011 |
| Daily driver to   | 2019 |

On recommendation from friend, I have tried Fedora. It is not really a beginner-friendly distribution, but after few tries I have managed to get it up and running the way I wanted. It came with GNOME and I got used to that.

There were quite a few problems over the years, the most prominent being broken system upgrade between releases. You were better of re-installing. It was a decent run, but I would not return.


---
# [Ubuntu](https://ubuntu.com/) {{footnote: `neofetch --ascii_distro Ubuntu_old -L`}}

```
                         ./+o+-
                 yyyyy- -yyyyyy+
              ://+//////-yyyyyyo
          .++ .:/++++++/-.+sss/`
        .:++o:  /++++++++/:--:/-
       o:+o+:++.`..```.-/oo+++++/
      .:+o:+o/.          `+sssoo+/
 .++/+:+oo+o:`             /sssooo.
/+++//+:`oo+o               /::--:.
+/+o+++`o++o               ++////.
 .++.o+++oo+:`             /dddhhh.
      .+.o+oo:.          `oddhhhh+
       +.++o+o``-````.:ohdhhhhh+
        `:o+++ `ohhhhhhhhyo++os:
          .o:`.syhhhhhhh/.oo++o`
              /osyyyyyyo++ooo+++/
                  ````` +oo+++o:
                         `oo++.
```

| Last used         | 2016 |
|-------------------|------|
| Daily driver from | 2011 |
| Daily driver to   | 2011 |

Ubuntu was my first ever experience with GNU/Linux system, which lasted less than a week.

Basically, I disliked the whole experience which was far to much like Windows which I hated. The system just forced me to do things certain way, and kept breaking if they were done differently. It was just the whole Windows crappy experience all over again. Very quickly I got pissed and switched to another distribution.

Later I came across Ubuntu again for few projects and the things have not changed at all. What changed was my knowledge and experience. All the stuff in background, for example unattended (and unwanted) updates. And the upgrade from one release to another never worked, it was a miracle when the system booted afterwards.


