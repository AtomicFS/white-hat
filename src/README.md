# White Hat Hacker's book

These are my notes, that help me to organize the accumulated knowledge and to congregate it in one place. Intended to be offline source.

![](img/anonymous_pixel_freedom.png)


## What is a hacker?

For me, a hacker is skilled computer expert that uses their technical knowledge to overcome a problem by creative means.

```admonish info
> The hacker culture is a subculture of individuals who enjoy - often in collective effort - the intellectual challenge of creatively overcoming the limitations of software systems or electronic hardware (mostly digital electronics), to achieve novel and clever outcomes

[Wkipedia / Hacker culture](https://en.wikipedia.org/wiki/Hacker_culture)
```

```admonish info
> A hacker is a person skilled in information technology who uses their technical knowledge to achieve a goal or overcome an obstacle, within a computerized system by non-standard means.

[Wikipedia / Hacker](https://en.wikipedia.org/wiki/Hacker)
```

```admonish info title="Quote"
> Hackers have a mind that is optimized for figuring out what is possible. When presented with some random new gadget, your mom will ask what it can do, hacker will ask what can I make this do.

[Pablos Holman | TEDxMidwest](https://youtu.be/hqKafI7Amd8?t=816)
```

The term is, unfortunately, more often being associated with a person who uses computers to gain unauthorized access to data.

```admonish info
> A hacker is a person who breaks into a computer system.

[Cisco / What Is a Hacker?](https://www.cisco.com/c/en/us/products/security/what-is-a-hacker.html)
```

Without hackers, we would not have computers. Look at [Steve Wozniak](https://en.wikipedia.org/wiki/Steve_Wozniak) for example.

Here is a great example what skilled person / hacker can do: [Using My Python Skills To Punish Credit Card Scammers](https://www.youtube.com/watch?v=StmNWzHbQJU).

Personally, I do not approve of [black-hat-hacking](https://en.wikipedia.org/wiki/Black_hat_(computer_security)), however I do encourage [white-hat-hacking](https://en.wikipedia.org/wiki/White_hat_(computer_security)). White-hat-hackers are very important for security and freedom of all of us.

I am happy that times change and hackers are gaining a better reputation. I am also happy that the obsolete trend of threatening hackers for finding a flaws in systems is dying out. Not long ago it was common practice for companies to [threat with legal actions](https://www.zdnet.com/article/pwc-sends-security-researchers-cease-and-desist-letter-instead-of-fixing-security-flaw/) or [even have police raid homes](https://www.dailydot.com/debug/justin-shafer-fbi-raid/) if anyone tried to let them know of discovered problem.

Imagine a physical bank building. And imagine that on your way to said bank you notice somebody forgot to close a door into the vault. White-hat-hacker would go to the front desk and report such problem, a noble thing to do. Unfortunately for a long time the bank would threaten to call police instead of thanking you and fixing the issue. How would you like that?

![](img/bad_security.jpg)

Thankfully this mentality is dying out and hackers and security researchers are in some cases even being rewarded for reporting a security flaws.


## Background

This document has been evolving for a long time now. On my way to become a [white-hat-hacker](https://en.wikipedia.org/wiki/White_hat_(computer_security)) I have learned a lot, and I am still learning.

Back in 2020, I started writing these notes in [LaTeX](https://www.latex-project.org/), but eventually hit a wall with long compile times and huge PDF document (over 400 pages). Not to mention portability and accessibility problems.


## Links

- Main repository, issue tracker and so on at [git.sr.ht/~atomicfs](https://git.sr.ht/~atomicfs/white-hat)
- Mirror repository at [gitlab.com](https://gitlab.com/AtomicFS/white-hat)


## Hosted static web-pages

[atomicfs.gitlab.io/white-hat](https://atomicfs.gitlab.io/white-hat)

![](img/web-qr-domain.png)

[white-hat-book.white-hat-hacker.icu](https://white-hat-book.white-hat-hacker.icu/)

![](img/web-qr-gitlabio.png)


