# Template

## Warning, under construction

![](../img/construction.gif)


## Notes

```admonish note
My personal oppinion or side-note
```

```admonish info
Fact

[Source]()
```

```admonish example title="/etc/hosts"
Example source code or configuration
```

```admonish example collapsible=true title="/etc/hosts"
Example source code or configuration
```

```admonish warning
Warning about something
```

```admonish warning title='DISCLAIMER'
Viewer discretion is advised.
```

```admonish bug
Existing bug, design flaw or evil design.
```

```admonish tip
Tips and tricks
```

```admonish success
```

```admonish success title="Solution"
Solution to your problem is

~~~
# help
~~~
```

```admonish info title="Quote"
> Some quoteted text here

[Link to source]()
```


## Images without source

![](../img/srht-secret-add_gpg_key.png)


## Images with source

[Source: pinout.xyz](https://pinout.xyz/#)

![](../img/bios_and_uefi/raspberry-pi-pinout.png)


## Quote

> Quoted text


