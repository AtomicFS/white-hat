# Abbreviations

| Acronym | Full name                                  |
|:--------|:-------------------------------------------|
| ACPI    | Advanced Configuration and Power Interface |
| AP      | Access Point                               |
| BS      | Bull Shit                                  |
| BSoD    | Blue Screen of Death                       |
| CD      | Compact Disc                               |
| CPI     | Cycles Per Second                          |
| DDoS    | Distributed Denial of Service              |
| DIY     | Do It Yourself                             |
| DVD     | Digital Versatile Disc                     |
| DoS     | Denial of Service                          |
| EULA    | End-User License Agreement                 |
| FUBAR   | Fucked Up Beyond All Repair/Recognition    |
| GPL     | General Public License                     |
| GUI     | Graphical User Interface                   |
| HW      | Hard Ware                                  |
| IDE     | Integrated Development Environment         |
| IPS     | Instructions Per Second                    |
| ISP     | Internet Service Provider                  |
| MIPS    | Millions of Instructions Per Second        |
| MITM    | Man In The Middle                          |
| OS      | Operating System                           |
| RAM     | Random-Access Memory                       |
| RTFM    | Read The Fucking Manual                    |
| STP     | Spanning Tree Protocol                     |
| STP     | Spanning Tree Protocol                     |
| SW      | Soft Ware                                  |
| TIM     | Thermal Interface Material                 |
| URL     | Uniform Resource Locators                  |


