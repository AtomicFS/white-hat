# Unix, Linux and computer revolution


## Unix

[Unix](https://en.wikipedia.org/wiki/Unix) originated in 1960 as a product of [AT&T](https://en.wikipedia.org/wiki/AT%26T_Corporation) and it was a proprietary operating system.

Many of the design principles of Unix (also known as [Unix philosophy](https://en.wikipedia.org/wiki/Unix_philosophy)) are still used to this day and are considered good practice:

> The Unix philosophy is documented by Doug McIlroy in the Bell System Technical Journal from 1978: 
> - Make each program do one thing well. To do a new job, build afresh rather than complicate old programs by adding new "features".
> - Expect the output of every program to become the input to another, as yet unknown, program. Don't clutter output with extraneous information. Avoid stringently columnar or binary input formats. Don't insist on interactive input.
> - Design and build software, even operating systems, to be tried early, ideally within weeks. Don't hesitate to throw away the clumsy parts and rebuild them.
> - Use tools in preference to unskilled help to lighten a programming task, even if you have to detour to build the tools and expect to throw some of them out after you've finished using them.
>
> It was later summarized by Peter H. Salus in A Quarter-Century of Unix (1994):
> - Write programs that do one thing and do it well.
> - Write programs to work together.
> - Write programs to handle text streams, because that is a universal interface.
>
> In their award-winning Unix paper of 1974, Ritchie and Thompson quote the following design considerations:
> - Make it easy to write, test, and run programs.
> - Interactive use instead of batch processing.
> - Economy and elegance of design due to size constraints ("salvation through suffering").
> - Self-supporting system: all Unix software is maintained under Unix.

YouTube channel [Computerphile](https://www.youtube.com/@Computerphile) has a great video about [Mainframes and the Unix Revolution](https://www.youtube.com/watch?v=-rPPqm44xLs).


## GNU

As a legends say, [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman), bored of limitations of proprietary software, established a [Free Software Foundation ](https://www.fsf.org/) in 1985 and founded the [GNU](https://en.wikipedia.org/wiki/GNU) project.

```admonish info
[GNU](https://en.wikipedia.org/wiki/GNU) is collection of free-software programs to be essential part of operating system.

`GNU` stands for `Gnu is Not Unix`.
```

```admonish info
[Free-software](https://en.wikipedia.org/wiki/Free_software) and [open-source software](https://en.wikipedia.org/wiki/Open-source_software) are not the same thing. Both are similar, but not the same.

**Free-software** is heavily focused on user freedoms and rights, while **open-source software** focuses on practicality.

**Open-source software** requires, as the name implies, the source-code to be freely accessible. However the use of the software might be limited by the license.

**Free-software** also requires free access to the source-code, but in addition has additional requirements:
- Freedom 0: The freedom to use the program for any purpose.
- Freedom 1: The freedom to study how the program works, and change it to make it do what you wish.
- Freedom 2: The freedom to redistribute and make copies so you can help your neighbor.
- Freedom 3: The freedom to improve the program, and release your improvements (and modified versions in general) to the public, so that the whole community benefits.

It can be assumed that all free-software is also open-source software, but not all open-source software is free-software. In other words, free-software is [subset](https://en.wikipedia.org/wiki/Subset) of open-source software.

The combination of both is often referred to as [free and open-source software](https://en.wikipedia.org/wiki/Free_and_open-source_software).
```

The GNU project was going well, with only missing piece being the kernel.

```admonish info
> [Kernel](https://en.wikipedia.org/wiki/Kernel_(operating_system)) is a computer program at the core of a computer's operating system and generally has complete control over everything in the system.

Kernel controls all the hardware, assigns system resources to programs and provides a abstraction layer to said programs via drivers.
```


## Linux kernel

In 1990, [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds) was exposed to [MINIX](https://en.wikipedia.org/wiki/Minix). Later, in 1991, Linus Torvalds developed the first Linux kernel and released it on 17 September 1991 (for the Intel x86 systems).

```admonish info
[Minix](https://en.wikipedia.org/wiki/Minix) is open-source Unix-like operating system designed for education. Basically bare-bones of Unix with some additions.

Since 2015 all Intel chipsets are running [Minix3](https://en.wikipedia.org/wiki/Minix#MINIX_3) as [Intel Management Engine](../hardware/intel-me_and_amd-psp.md).
```

Nowadays, Linux kernel is developed by a community, making it the largest software community project in human history.

Quite a few Unix system (such as MacOS, Solaris, etc.) are developed by corporations as proprietary software.

```admonish note
Linus Torvalds, while working on Linux, found that source control and versioning tools available at the time were terrible. So in 2005 created [Git](https://en.wikipedia.org/wiki/Git).

Interesting video from Google: [Tech Talk: Linus Torvalds on git](https://www.youtube.com/watch?v=4XpnKHJAok8)

Here is the same video but with chapters and time-stamps: [Creator of git, Linus Torvalds Presents the Fundamentals of git](https://www.youtube.com/watch?v=MjIPv8a0hU8)
```

In 2001 was released a very interesting documentary [Revolution OS](https://www.imdb.com/title/tt0308808/) which cover over 20 years of GNU, Linux, open-source, and the free software movement. More information can be found at [wikipedia page](https://en.wikipedia.org/wiki/Revolution_OS) or [official website](http://www.revolution-os.com/). The documentary is also available on YouTube thanks to some questionable means.


## BSD

[BSD](https://en.wikipedia.org/wiki/Berkeley_Software_Distribution) branched off Unix in 1978. Originally source-available, later open-source.

Later from BSD branched open-source projects such as [FreeBSD](https://en.wikipedia.org/wiki/FreeBSD) or [OpenBSD](https://en.wikipedia.org/wiki/OpenBSD) (just to name a few).


# Final notes

[Unix history](https://en.wikipedia.org/wiki/Unix-like#/media/File:Unix_history-simple.svg):
![](../img/operating_systems/unix_history-simple.svg)

```admonish info
BSD is a branch of Unix, based on the actual Unix source-code.

Linux is only inspired by Unix, hence Unix-like.
```

Nice and handy website called [distrowatch.com](https://distrowatch.com/) acts like a centralized information hub regarding different free and open-source operating systems and distributions (Unix-like, Unix-based, etc).


## Linux vs GNU/Linux

Generally, I prefer `Linux` over `GNU/Linux`, for following reasons:
- Linux is convenient short-hand. People know that in conversation `Linux` refers to one of Linux distributions, often but not always including GNU.
- There are Linux distributions which have nothing to do with GNU, such as [Alpine Linux](https://distrowatch.com/table.php?distribution=alpine) ([gcc](https://en.wikipedia.org/wiki/GNU_Compiler_Collection) is replaced with [musl](https://en.wikipedia.org/wiki/Musl), and GNU core utilities are replaced with [BusyBox](https://en.wikipedia.org/wiki/BusyBox))
	- Besides that, it is almost never only Linux kernel and GNU utilities, but also whole plethora of other software such as window manager, init system and so on.

