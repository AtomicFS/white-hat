# Lightweight

Sources:
- [Wikipedia / Light-weight Linux distribution](https://en.wikipedia.org/wiki/Light-weight_Linux_distribution)

There some some really minimalistic operating systems which can run on almost anything. Sometimes it is nice to resurrect some old and obsolete hardware, but it is difficult or close to impossible to fit onto it modern system. However there are some Linux distributions made for such task.


## Tiny Core Linux

- 16 MB graphical Linux desktop based on recent Linux kernel. Runs entirely from memory and therefore it is fast.
- [Minimum requirements](http://www.tinycorelinux.net/faq.html#req): 46 MB RAM, CPU `i486DX` (486 with a math processor).
- Recommended requirements: 128 MB RAM + SWAP, CPU `Pentium 2` or better.
- Links:
	- [distrowatch.com](https://distrowatch.com/table.php?distribution=tinycore)
	- [Home Page](http://www.tinycorelinux.net/)

![](../img/operating_systems/tinycore.png)


## Damn Small Linux

- **DISCONTINUED**
	- last release was `DSL 4.11 RC2` in 2012
- 50 MB graphical, nearly complete Linux desktop.
- [Minimum requirements](http://www.damnsmalllinux.org/wiki/frequently_asked_questions.html#Will_DSL_ever_get_bigger_than_50_MBytes.3F): 16 MB RAM, CPU `i486DX` (486 with a math processor).
- Recommended requirements: 2 GB disk, 64 MB RAM, CPU 200 MHz or better.
- Links:
	- [distrowatch.com](https://distrowatch.com/table.php?distribution=damnsmall)
	- [Home Page](http://www.damnsmalllinux.org/)

![](../img/operating_systems/damn_small_linux.png)


## Puppy Linux

- 300 MB graphical Linux desktop, can run LIVE. Runs entirely from memory and therefore it is fast.
- [Minimum requirements](https://wikka.puppylinux.com/MinimumSystemRequirements): 350 MB disk, 64 MB RAM, CPU 333 MHz.
- Recommended requirements: 256 MB RAM + 512 MB SWAP, CPU 600 MHz or better.
- Links:
	- [distrowatch.com](https://distrowatch.com/table.php?distribution=puppy)
	- [Home Page](https://puppylinux-woof-ce.github.io/)


## FreeDOS

- It is not Linux-based, but it is open-source software. It can run any [MS-DOS](https://en.wikipedia.org/wiki/MS-DOS) compatible software, with improvements in memory usage and power management compared to `MS-DOS`. In addition, number of different packages have bee ported from Linux to extend functionality.
- [Minimum requirements](http://wiki.freedos.org/wiki/index.php/FreeDOS_Spec#CPU_support): 20 MB disk, 640 kB RAM, CPU `8088(XT)`.
- Recommended requirements: CPU `Intel i386` or better.
- Links:
	- [Home Page](https://www.freedos.org/)


# Honorable mentions

- [Alpine Linux](https://distrowatch.com/table.php?distribution=alpine)
- [OpenWRT](http://openwrt.org/)
	- Designed to run on embedded network devices.
- [Linux From Scratch](https://www.linuxfromscratch.org/)
	- This one is not a ready-to-go distro, rather a step-by-step guide to build your own custom Linux system.


# Super cool projects with tiny Linux


## Running Linux on business card

![](../img/operating_systems/linuxCardWhole.jpg)

Specs:
- CPU: `ATSAMD21`, 32-bit ARM, form  48 MHz to 72 MHz
- Memory: from 4 MB to 32 MB

Sources:
- [A Linux Business Card You Can Build](https://hackaday.com/2022/07/14/a-linux-business-card-you-can-build/)
- [My business card runs Linux (and Ultrix), yours can too](https://dmitry.gr/?r=05.Projects&proj=33.%20LinuxCard)

