# Windows-like

Here are few options to replace Windows.

```admonish warning title='DISCLAIMER'
I have no experience with any of these systems.
```


## Linux with Cinnamon

If you only need looks of Window 10, consider [Cinnamon](https://wiki.archlinux.org/title/Cinnamon). Cinnamon is a desktop environment which is out of the box similar to classic Windows.

Few distributions shipping with Cinnamon:
- [EndeavourOS](https://distrowatch.com/table.php?distribution=endeavour)
- [Linux Mint](https://distrowatch.com/table.php?distribution=mint)
- [openSUSE](https://distrowatch.com/table.php?distribution=opensuse)

Additionally, Cinnamon supports [themes](https://cinnamon-spices.linuxmint.com/themes/popular), here are some available Windows themes:
- [Windows 10 Light Theme](https://cinnamon-spices.linuxmint.com/themes/view/Windows-10)
	- ![](https://cinnamon-spices.linuxmint.com/git/themes/Windows-10/screenshot.png)
- [Windows-10-Basic](https://cinnamon-spices.linuxmint.com/themes/view/Windows-10-Basic)
	- ![](https://cinnamon-spices.linuxmint.com/git/themes/Windows-10-Basic/screenshot.png)


## Zorin OS

[Zorin OS](https://distrowatch.com/table.php?distribution=zorin) is aiming to be beginner-friendly Ubuntu-based system designed to feel like Windows.

![](https://distrowatch.com/images/ktyxqzobhgijab/zorin.png)


## Robolinux

[Robolinux](https://distrowatch.com/table.php?distribution=robolinux) is also Ubuntu-base system, but with a twist. It offers pre-configured virtual machines with Windows XP, Windows 7 and even Windows 10.

![](https://distrowatch.com/images/ktyxqzobhgijab/robolinux.png)

![](https://lcom.static.linuxfound.org/sites/lcom/files/robolinux_2.jpg)


## ReactOS

[ReactOS](https://distrowatch.com/table.php?distribution=ReactOS) is **not a Linux-based system!** It is actually operating system written from scratch to be binary-compatible with Windows.

![](https://reactos.org/sites/default/files/putty.png)

Even though it is being developed for over 20 years, do not expect any miracles. It is still in [alpha](https://en.wikipedia.org/wiki/Software_release_life_cycle#Alpha) stage.

[Software testing results](https://reactos.org/wiki/Testing_Central).

