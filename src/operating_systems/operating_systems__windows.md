# Windows

[Microsoft Windows](https://en.wikipedia.org/wiki/Microsoft_Windows) is a proprietary operating system developed by Microsoft. Since Microsoft's inception, there were many Windows versions, which are listed in [Wikipedia article](https://en.wikipedia.org/wiki/Microsoft_Windows#Timeline_of_releases).

![](https://upload.wikimedia.org/wikipedia/commons/1/17/Suite_des_versions_de_Windows.svg)


## Reasons why I do not use Windows

- [EULA](../licenses/microsoft_windows_eula.md)
- No privacy
	- [![](https://shields.tosdr.org/en_244.svg)](https://tosdr.org/en/service/244)
	- [![](https://shields.tosdr.org/en_202.svg)](https://tosdr.org/en/service/202)
	- Microsoft's telemetry is spying on users. While it is possible to disable, it is often re-enabled by updates.
	- [Basic (required) telemetry](https://learn.microsoft.com/en-us/windows/privacy/configure-windows-diagnostic-data-in-your-organization#required-diagnostic-data) includes details about all connected hardware (including printers, cameras, etc) and Microsoft Store data such as installed programs.
	- [Enhanced telemetry](https://learn.microsoft.com/en-us/windows/privacy/configure-windows-diagnostic-data-in-your-organization#enhanced-diagnostic-data) includes all above and browser history, used programs, disk usage and more. Microsoft is rather vague about details.
	- Computers can often even take 100% of system resources for couple of seconds or even minutes to compile the telemetry report, regardless whether or not you are using said computer.
	- Additional sources:
		- YouTube video from [Techquickie: What Data Does Windows 10 Send to Microsoft](https://www.youtube.com/watch?v=XZiSiEwKSYc)
	- **Conclusion: Microsoft, go to the corner of shame next to Facebook and Google**
- Updates
	- I was seriously pissed about the forced upgrade from Windows 7 to Windows 10. And many Windows users too.
		- [Never10](https://www.grc.com/never10.htm) program to disable the automatic upgrades
		- [Stops Accidental Windows 10 Upgrades](https://www.pcmag.com/news/never10-tool-stops-accidental-windows-10-upgrades)
		- [Forum post](https://www.tenforums.com/installation-upgrade/20002-almost-forced-upgrade-7-10-no-help-me-stop.html)
		- Personally, I would not be surprised if the same will happen with Windows 11.
	- Forced updates
		- Windows randomly decides to update without authorization. As [Murphy's law](https://en.wikipedia.org/wiki/Murphy's_law) dictates, at the most inconvinient time.
		- Many updates fail, sometimes damaging the operating system or even deleting all of user data.
		- More than once I left my computer run over night to finish a task, just to discover in the morning that automatic update restarted the computer.
	- Long waiting time for bug fixes
		- [Microsoft takes months to fix critical Azure Synapse bug](https://www.techtarget.com/searchsecurity/news/252521579/Microsoft-takes-months-to-fix-critical-Azure-Synapse-bug)
		- [Why Microsoft Took A Year To Fix Critical Windows Bug That Allowed Hackers To Spy On Worker PCs](https://www.forbes.com/sites/thomasbrewster/2015/02/10/microsoft-windows-flaw-survives-for-a-year/)
		- [Microsoft Keeps Failing to Patch the Critical PrintNightmare Bug](https://www.wired.com/story/microsoft-keeps-failing-patch-windows-printnightmare-bug/)
		- Not a security issue, but [Microsoft Finally Fixes Notepad After 20 Years of Inadequacy](https://www.howtogeek.com/fyi/microsoft-finally-fixes-notepad-after-20-years-of-inadequacy/)
	- **Conclusion: fuck you Microsoft**
- Security
	- Example can be Windows Share exploit which leaked from [NSA](https://en.wikipedia.org/wiki/National_Security_Agency) tools, latex exploited by [WannaCry](../cybersecurity/malware__wannacry.md).
	- Windows is full of the stupid security holes. Not only that, but it takes Microsoft months or even years to fix some of the known issues. And some issue will never be fixed because of the flawed design.
		- [pwdump](https://en.wikipedia.org/wiki/Pwdump) exploits a poor password management in Windows ([pwdump7](https://www.tarasco.org/security/pwdump_7/index.html))
			- Video demonstrating [cracking window password using pwdump](https://www.youtube.com/watch?v=TbqpyrMj_Bo)
			- Video with [deeper explanation](https://www.youtube.com/watch?v=PmwlkyTomVs). This problem cannot be fixed, it is result of flawed design, Microsoft just tries to [obfuscate](https://en.wikipedia.org/wiki/Obfuscation) it.
	- **Conclusion: security in Windows is a joke**
- Windows Store
	- Simply horrible experience (last updated in November 2018)
		- Full of bloatware (check Windows debloating youtube video [Debloat Windows 10](https://www.youtube.com/watch?v=q4ziE5Am0pM)).
		- Forcing developers to pay for this service exactly like Valve with [Steam](https://en.wikipedia.org/wiki/Steam_(service)).
	- [](https://tosdr.org/en/service/202)
- Notification Bar
	- Very annoying and useless, full of stuff that will be there no matter what.
- Security Center
	- I want to pick my Anti-Virus, and choose my firewall settings.
- Cortana
	- Privacy issues.
	- Takes a lot of resources.
- Suggested apps
	- Annoying bullshit equivalent to door salesman.
- Bloated Installation
	- In the fresh installation of Windows, there is so much useless crap, it is unbelievable. No wonder you need SSD, 16GB of RAM and the newest CPU to to even turn on Windows.
		- [Windows 11 and Windows 10 bloatware list](https://www.digitalcitizen.life/windows-10-bloatware/)
- Windows is unstable
	- And no wonder since they [fired a whole lot of testers in 2015](https://www.linkedin.com/pulse/20140806183208-12100070-why-did-microsoft-lay-off-programmatic-testers)

**Conclusion: Burn it with fire**

Inspiration for this was video from [Chris Titus Tech; Why I stopped using Windows 10 - 8 Major Reasons](https://www.youtube.com/watch?v=QSykL1I-WIc).

I agree with a Chris's prediction from the video, that Microsoft will attempt make Windows store the only way to install software.

My prediction: Microsoft will eventually scrap the whole Windows thing and replace with Ubuntu. Of course they would change wallpaper, pre-install opposite of [WINE](https://en.wikipedia.org/wiki/Wine_(software)) and call it something stupid like "Windows Next".


