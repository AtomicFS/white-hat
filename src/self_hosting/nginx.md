# nginx

![](../img/construction.gif)


## Self-signed certificates for local network

[](https://wiki.archlinux.org/title/OpenSSL)
[](https://wiki.archlinux.org/title/Transport_Layer_Security#Obtaining_a_certificate)

- Generate a private key (set permission mask to `077`!)
	- it will be placed in `/etc/ssl/private` by default (unless you change configuration)

```
# cd /etc/ssl/private
# openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:4096 -out server.key
# chmod 400 falcon_openssl.key
```

- Create CSR
```
# openssl req -new -sha256 -key server.key -out server.csr
```

- Create certificate
```
# openssl x509 -req -days 1095 -in server.csr -signkey server.key -out server.crt
```

```admonish
I usually leave everything as default, only change the `Common Name (e.g. server FQDN or YOUR name) []:` option to whatever the `server_name` will be set in `nginx` configuration.
```


## Certbot

- Create and host `hello world` html document.
- `certbot --nginx`



[](https://wiki.archlinux.org/title/Certbot#Webroot)



