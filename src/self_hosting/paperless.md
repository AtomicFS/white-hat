# Paperless

```admonish info
[Paperless](https://wiki.archlinux.org/title/Paperless) is an open source document management system that indexes your scanned documents and allows you to easily search for documents and store metadata alongside your documents.
```

> A supercharged version of paperless: scan, index and archive all your physical documents.

I am going with bare-metal installation, and to manage all dependencies I use a [meta package](../archlinux/meta_package.md) called [wht_nas_paperless](https://git.sr.ht/~atomicfs/atomicfs-repo-arch/tree/master/item/packages/archlinux/x86_64/wht_nas_paperless).

Most of these steps are taken from the [documentation](https://paperless-ngx.readthedocs.io/en/latest/setup.html#setup-bare-metal).

```admonish bug collapsible=true
If you see this error log, it is known issue:
~~~
bad escape \d at position 7 : Traceback (most recent call last):
  File "/usr/lib/python3.10/site-packages/django_q/cluster.py", line 432, in worker
    res = f(*task["args"], **task["kwargs"])
  File "/usr/share/paperless/src/documents/tasks.py", line 154, in consume_file
    document = Consumer().try_consume_file(
  File "/usr/share/paperless/src/documents/consumer.py", line 334, in try_consume_file
    date = parse_date(self.filename, text)
  File "/usr/share/paperless/src/documents/parsers.py", line 221, in parse_date
    return next(parse_date_generator(filename, text), None)
  File "/usr/share/paperless/src/documents/parsers.py", line 280, in parse_date_generator
    yield from __process_content(text, settings.DATE_ORDER)
  File "/usr/share/paperless/src/documents/parsers.py", line 271, in __process_content
    date = __process_match(m, date_order)
  File "/usr/share/paperless/src/documents/parsers.py", line 262, in __process_match
    date = __parser(date_string, date_order)
  File "/usr/share/paperless/src/documents/parsers.py", line 235, in __parser
    return dateparser.parse(
  File "/usr/lib/python3.10/site-packages/dateparser/conf.py", line 92, in wrapper
    return f(*args, **kwargs)
  File "/usr/lib/python3.10/site-packages/dateparser/__init__.py", line 61, in parse
    data = parser.get_date_data(date_string, date_formats)
  File "/usr/lib/python3.10/site-packages/dateparser/date.py", line 456, in get_date_data
    parsed_date = _DateLocaleParser.parse(
  File "/usr/lib/python3.10/site-packages/dateparser/date.py", line 200, in parse
    return instance._parse()
  File "/usr/lib/python3.10/site-packages/dateparser/date.py", line 204, in _parse
    date_data = self._parsers[parser_name]()
  File "/usr/lib/python3.10/site-packages/dateparser/date.py", line 224, in _try_freshness_parser
    return freshness_date_parser.get_date_data(self._get_translated_date(), self._settings)
  File "/usr/lib/python3.10/site-packages/dateparser/date.py", line 262, in _get_translated_date
    self._translated_date = self.locale.translate(
  File "/usr/lib/python3.10/site-packages/dateparser/languages/locale.py", line 131, in translate
    relative_translations = self._get_relative_translations(settings=settings)
  File "/usr/lib/python3.10/site-packages/dateparser/languages/locale.py", line 159, in _get_relative_translations
    self._generate_relative_translations(normalize=True))
  File "/usr/lib/python3.10/site-packages/dateparser/languages/locale.py", line 173, in _generate_relative_translations
    pattern = DIGIT_GROUP_PATTERN.sub(r'?P<n>\d+', pattern)
  File "/usr/lib/python3.10/site-packages/regex/regex.py", line 710, in _compile_replacement_helper
    is_group, items = _compile_replacement(source, pattern, is_unicode)
  File "/usr/lib/python3.10/site-packages/regex/_regex_core.py", line 1737, in _compile_replacement
    raise error("bad escape \\%s" % ch, source.string, source.pos)
regex._regex_core.error: bad escape \d at position 7
~~~

- [[BUG] regex._regex_core.error: bad escape \d at position 7 #1684](https://github.com/paperless-ngx/paperless-ngx/issues/1684)
- [[BUG] Paperless fails import at date parsing stage #1201](https://github.com/paperless-ngx/paperless-ngx/issues/1201)
- [[BUG] Paperless fails import at date parsing stage #1200](https://github.com/paperless-ngx/paperless-ngx/issues/1200)
- [[BUG] Upload fails due to date parsing errors #1188](https://github.com/paperless-ngx/paperless-ngx/issues/1188)

They mostly blame ArchLinux packaging, which does not follow python's `requirements.txt`. In my opinion, pinning dependency to a specific version is not the solution.

The underlying problem is incompatibility of two or more python packages - they should have already open issues about it and hopefully it will be fixed soon.

**UPDATE:** Problem is fixed with `python-dateparser v1.1.4`.
```

```admonish bug collapsible=true
As described in [AUR/paperless-ngx](https://aur.archlinux.org/packages/paperless-ngx), if you see following error:
~~~
Mar 10 21:36:21 ark systemd[1]: Started Paperless Celery Workers.
Mar 10 21:36:22 ark celery[284848]: Traceback (most recent call last):
Mar 10 21:36:22 ark celery[284848]:   File "/usr/bin/celery", line 33, in <module>
Mar 10 21:36:22 ark celery[284848]:     sys.exit(load_entry_point('celery==5.2.7', 'console_scripts', 'celery')())
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/celery/__main__.py", line 15, in main
Mar 10 21:36:22 ark celery[284848]:     sys.exit(_main())
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/celery/bin/celery.py", line 217, in main
Mar 10 21:36:22 ark celery[284848]:     return celery(auto_envvar_prefix="CELERY")
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/core.py", line 1130, in __call__
Mar 10 21:36:22 ark celery[284848]:     return self.main(*args, **kwargs)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/core.py", line 1055, in main
Mar 10 21:36:22 ark celery[284848]:     rv = self.invoke(ctx)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/core.py", line 1655, in invoke
Mar 10 21:36:22 ark celery[284848]:     sub_ctx = cmd.make_context(cmd_name, args, parent=ctx)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/core.py", line 920, in make_context
Mar 10 21:36:22 ark celery[284848]:     self.parse_args(ctx, args)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/core.py", line 1378, in parse_args
Mar 10 21:36:22 ark celery[284848]:     value, args = param.handle_parse_result(ctx, opts, args)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/core.py", line 2360, in handle_parse_result
Mar 10 21:36:22 ark celery[284848]:     value = self.process_value(ctx, value)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/core.py", line 2316, in process_value
Mar 10 21:36:22 ark celery[284848]:     value = self.type_cast_value(ctx, value)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/core.py", line 2304, in type_cast_value
Mar 10 21:36:22 ark celery[284848]:     return convert(value)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/click/types.py", line 82, in __call__
Mar 10 21:36:22 ark celery[284848]:     return self.convert(value, param, ctx)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/celery/bin/worker.py", line 58, in convert
Mar 10 21:36:22 ark celery[284848]:     value = concurrency.get_implementation(worker_pool)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/celery/concurrency/__init__.py", line 28, in get_implementation
Mar 10 21:36:22 ark celery[284848]:     return symbol_by_name(cls, ALIASES)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/kombu/utils/imports.py", line 56, in symbol_by_name
Mar 10 21:36:22 ark celery[284848]:     module = imp(module_name, package=package, **kwargs)
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/importlib/__init__.py", line 126, in import_module
Mar 10 21:36:22 ark celery[284848]:     return _bootstrap._gcd_import(name[level:], package, level)
Mar 10 21:36:22 ark celery[284848]:   File "<frozen importlib._bootstrap>", line 1050, in _gcd_import
Mar 10 21:36:22 ark celery[284848]:   File "<frozen importlib._bootstrap>", line 1027, in _find_and_load
Mar 10 21:36:22 ark celery[284848]:   File "<frozen importlib._bootstrap>", line 1006, in _find_and_load_unlocked
Mar 10 21:36:22 ark celery[284848]:   File "<frozen importlib._bootstrap>", line 688, in _load_unlocked
Mar 10 21:36:22 ark celery[284848]:   File "<frozen importlib._bootstrap_external>", line 883, in exec_module
Mar 10 21:36:22 ark celery[284848]:   File "<frozen importlib._bootstrap>", line 241, in _call_with_frames_removed
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/celery/concurrency/prefork.py", line 19, in <module>
Mar 10 21:36:22 ark celery[284848]:     from .asynpool import AsynPool
Mar 10 21:36:22 ark celery[284848]:   File "/usr/lib/python3.10/site-packages/celery/concurrency/asynpool.py", line 29, in <module>
Mar 10 21:36:22 ark celery[284848]:     from billiard.compat import buf_t, isblocking, setblocking
Mar 10 21:36:22 ark celery[284848]: ImportError: cannot import name 'buf_t' from 'billiard.compat' (/usr/lib/python3.10/site-packages/billiard/compat.py)
Mar 10 21:36:22 ark systemd[1]: paperless-task-queue.service: Main process exited, code=exited, status=1/FAILURE
Mar 10 21:36:22 ark systemd[1]: paperless-task-queue.service: Failed with result 'exit-code'.
~~~
The problem is that `paperless-ngx` requires `python-billiard v3.x.x`, and in Arch Linux there is already `python-billiard v4.x.x`. To fix this, you have to manually install the old `python-billiard` version.

The do
~~~
# systemctl restart paperless.target
~~~
```


## Installation

```admonish
The package itself comes from [AUR](https://aur.archlinux.org/packages/paperless-ngx). I have added it into my personal repository to be built.

Unfortunately there are many dependencies which are also in AUR, and I had to add them all manually :/
```

```admonish example collapsible=true title="wht_server_paperless/PKGBUILD"
[wht_server_paperless/PKGBUILD](https://git.sr.ht/~atomicfs/atomicfs-repo-arch/tree/master/item/packages/archlinux/x86_64/wht_nas_paperless/PKGBUILD)
~~~ini
{{#include ../../submodules/atomicfs-repo-arch/packages/archlinux/x86_64/wht_server_paperless/PKGBUILD}}
~~~
```

```admonish warning
Paperless will not allow symlinks to persistent storage! Since I run this on NAS with RAID (system disk is separate), my first thought was to create symlinks. That is not possible.

You can change the location of persistent storage for paperless, but I prefer to keep it default.

Since I use [btrfs](https://wiki.archlinux.org/title/Btrfs), I just created a new subvolume and mounted it at `/var/lib/paperless`.
```


## Configuration

As for the configuration, most of it is default, I have just changed few things (mostly secrets, passwords and stuff). Importantly I have change `PAPERLESS_DBPORT` to what my [MariaDB](mariadb.md) database is configured.

```admonish example collapsible=true title="/etc/paperless.conf##template"
[/etc/paperless.conf##template](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/etc/yadm/alt/etc/paperless.conf%23%23template)

This is a [yadm template](https://yadm.io/docs/templates). I used template so that I can store passwords separatly in encrypted files.
~~~ini
{{#include ../../submodules/dotfiles-system/etc/yadm/alt/etc/paperless.conf##template}}
~~~
```

Next, create `paperless` database (as defined in `PAPERLESS_DBNAME`). Also a user `paperless` (as defined in `PAPERLESS_DBUSER`) in the `MariaDB` and give it the password that you set in `PAPERLESS_DBPASS`. Do not forget to give this user access to the database.

```
# mariadb -u root -p
> CREATE DATABASE paperless;
> CREATE USER 'paperless'@'localhost' IDENTIFIED BY '<password>';
> GRANT ALL PRIVILEGES ON paperless.* TO 'paperless'@'localhost';
> FLUSH PRIVILEGES;
> quit
```


```admonish info
- `user@%` allows access from all locations
- `user@localhost` allows access from localhost only
```

```admonish
To list all databases:
~~~
SHOW DATABASES;
~~~

To list all users:
~~~
SELECT User FROM mysql.user;
~~~

Show user permissions:
~~~
SHOW GRANTS FOR 'user'@'localhost';
~~~
```

After initial setup (and also after updates), run database migration:
```
# sudo -u paperless paperless-manage migrate
```

Create admin:
```
# sudo -u paperless paperless-manage createsuperuser
```


At this point the `paperless` server should be available at [localhost:7998](localhost:7998) (I have change the default port!). Now let's set up [nginx as a reverse proxy](https://paperless-ngx.readthedocs.io/en/latest/setup.html#using-nginx-as-a-reverse-proxy).


## nginx

For HTTPS chek out [Self-signed certificates for local network](nginx.md#self-signed-certificates-for-local-network).

The `/etc/nginx/nginx.conf` is not that interesting.

```admonish example collapsible=true title="/etc/nginx/nginx.conf"
[/etc/nginx/nginx.conf](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/etc/yadm/alt/etc/nginx/nginx.conf%23%23default)
~~~ini
{{#include ../../submodules/dotfiles-system/etc/yadm/alt/etc/nginx/nginx.conf##default}}
~~~
```

However the `/etc/nginx/sites-available/paperless.conf` might give you a headache.

I looked at [ArchLinux wiki / nginx / TLS](https://wiki.archlinux.org/title/Nginx#TLS), tried the [Mozilla SSL Configuration Generator](https://ssl-config.mozilla.org/#server=nginx&config=modern&hsts=false&ocsp=false) and I was still getting odd behavior. And when I finally got the login srceen for paperless, I got error after login `CSRF verification failed`.

Thankfully with a bit of searching the internets, I found a simple solution: add `proxy_set_header X-Forwarded-Proto https;`.

```admonish example collapsible=true title="/etc/nginx/sites-available/paperless.conf"
[/etc/nginx/sites-available/paperless.conf](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/etc/yadm/alt/etc/nginx/sites-available/paperless.conf%23%23default)
~~~ini
{{#include ../../submodules/dotfiles-system/etc/yadm/alt/etc/nginx/sites-available/paperless.conf##default}}
~~~
```

So now you should be able to go to the paperless over HTTPS :D

I will likely see this error:

![](../img/firefox_certificate_warning.png)

That is simply because you are using self-signed certificates. It is possible to fix this, but I am too lazy right now (maybe later).


## Usage

I highly recommend to read the [documentation / usage overview](https://paperless-ngx.readthedocs.io/en/latest/usage_overview.html#) section.

