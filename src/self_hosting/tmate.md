# tmate

You can host your own `tmate` server. As usual, I am handling the installation and dependencies via [meta package](../archlinux/meta_package.md).

## Installation

```admonish example collapsible=true title="wht_server/PKGBUILD"
[wht_server/PKGBUILD](https://git.sr.ht/~atomicfs/atomicfs-repo-arch/tree/master/item/packages/archlinux/x86_64/wht_server/PKGBUILD)
~~~ini
{{#include ../../submodules/atomicfs-repo-arch/packages/archlinux/x86_64/wht_server/PKGBUILD}}
~~~
```

```admonish
The package itself comes from [AUR](https://aur.archlinux.org/packages/tmate-ssh-server-git). I have added it into my personal repository to be built.
```

Now comes the fun part :D

Here is the `PKGBUILD` file from the AUR:

```admonish example collapsible=true title="tmate-ssh-server-git/PKGBUILD"
[AUR/tmate-ssh-server-git](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=tmate-ssh-server-git)
~~~ini
{{#include ../../submodules/atomicfs-repo-arch/packages/archlinux/x86_64/tmate-ssh-server-git/PKGBUILD}}
~~~
```

According to the instructions, you are supposed to generate SSH keys via `./create_keys.sh`. When looking into the upstream repository into the [create_keys.sh script](https://github.com/tmate-io/tmate-ssh-server/blob/master/create_keys.sh), it is just a simple wrapper for `ssh-keygen`.

Thankfully, the maintainer of the AUR package created these handy `systemd` services which do the exact same thing - they will create SSH keys in `/etc/tmate-ssh-server`:
- `tmate-ssh-server-genkeys-ed25519.service`
- `tmate-ssh-server-genkeys-rsa.service`


## Configuration

As for the configuration of the server, it is rater simple.

```admonish example title="/etc/tmate-ssh-server/tmate-ssh-server.conf"
[/etc/tmate-ssh-server/tmate-ssh-server.conf](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/etc/yadm/alt/etc/tmate-ssh-server/tmate-ssh-server.conf%23%23hostname.falcon)
~~~ini
{{#include ../../submodules/dotfiles-system/etc/yadm/alt/etc/tmate-ssh-server/tmate-ssh-server.conf##hostname.falcon}}
~~~
```

Now you can start and enable the services:

```
# systemctl start tmate-ssh-server-genkeys-ed25519.service
# systemctl start tmate-ssh-server-genkeys-rsa.service
# systemctl enable --now tmate-ssh-server.service
```

Now you will need to get `SHA256` fingerprint. To do that run commands:

```
# journalctl -eu tmate-ssh-server-genkeys-ed25519.service
# journalctl -eu tmate-ssh-server-genkeys-rsa.service
```

Here is the example output for `tmate-ssh-server-genkeys-ed25519.service`:

```
Dec 02 10:55:06 falcon systemd[1]: Starting tmate ed25519 key generation...
Dec 02 10:55:06 falcon ssh-keygen[2641]: Generating public/private ed25519 key pair.
Dec 02 10:55:06 falcon ssh-keygen[2641]: Your identification has been saved in /etc/tmate-ssh-server/ssh_host_ed25519_key
Dec 02 10:55:06 falcon ssh-keygen[2641]: Your public key has been saved in /etc/tmate-ssh-server/ssh_host_ed25519_key.pub
Dec 02 10:55:06 falcon ssh-keygen[2641]: The key fingerprint is:
Dec 02 10:55:06 falcon ssh-keygen[2641]: SHA256:wT+1dUSJNxJ/9b82RzvPeThZicv4g2VJPG+0xgc1Wac root@falcon
Dec 02 10:55:06 falcon ssh-keygen[2641]: The key's randomart image is:
Dec 02 10:55:06 falcon ssh-keygen[2641]: +--[ED25519 256]--+
Dec 02 10:55:06 falcon ssh-keygen[2641]: |             .o.X|
Dec 02 10:55:06 falcon ssh-keygen[2641]: |       .     o.O=|
Dec 02 10:55:06 falcon ssh-keygen[2641]: |        o   o Eo*|
Dec 02 10:55:06 falcon ssh-keygen[2641]: |         o . *.oo|
Dec 02 10:55:06 falcon ssh-keygen[2641]: |        S o o B.=|
Dec 02 10:55:06 falcon ssh-keygen[2641]: |           . = O=|
Dec 02 10:55:06 falcon ssh-keygen[2641]: |            * +Xo|
Dec 02 10:55:06 falcon ssh-keygen[2641]: |           o +=.B|
Dec 02 10:55:06 falcon ssh-keygen[2641]: |            ...o+|
Dec 02 10:55:06 falcon ssh-keygen[2641]: +----[SHA256]-----+
Dec 02 10:55:06 falcon systemd[1]: Finished tmate ed25519 key generation.
```

Take a not of that `SHA256:wT+1dUSJNxJ/9b82RzvPeThZicv4g2VJPG+0xgc1Wac`, you need it for client configuration.

Surprise surprise, the last thing is to configure clients.

```admonish example title="~/.tmate.conf"
[~/.tmate.conf](https://git.sr.ht/~atomicfs/dotfiles/tree/master/item/.tmate.conf)
~~~ini
{{#include ../../submodules/dotfiles/.tmate.conf}}
~~~
```

```admonish tip
Do not forget to set up port forwarding.
```

I use [firewalld](https://wiki.archlinux.org/title/Firewalld), and I simply added a config for the `tmate` server.

```admonish example title="/etc/firewalld/services/tmate.xml"
[/etc/firewalld/services/tmate.xml](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/etc/yadm/alt/etc/firewalld/services/tmate.xml%23%23hostname.falcon)
~~~xml
{{#include ../../submodules/dotfiles-system/etc/yadm/alt/etc/firewalld/services/tmate.xml##hostname.falcon}}
~~~
```

```admonish warning
If you are on the same network as the machine hosting the `tmate` server, you might come across problem where the `tmate` client will refuse to work with error:
~~~
Error connecting: Connection refused
Reconnecting... (Error connecting: Connection refused)
~~~

I have public IP address at home and domain. `tmate.white-hat-hacker.icu` is sub-domain pointing to my public IP address. And when on the same network, and `tmate-server-host` set to `tmate.white-hat-hacker.icu`, I see this error.

Since I also run my own DNS, I simply added a entry to fix this. Now when resolving the `tmate.white-hat-hacker.icu` I get local IP address of the machine instead of my public IP address.
```


## Notes

### Sessions

On the server, sessions create sockets in:
```
/tmp/systemd-private-<RANDOM_STRING>-tmate-ssh-server.service-<RANDOM_STRING>/tmp/tmate/sessions/<SESSION_ID>
```

This can be useful when providing remote support to less tech-savy people. If you have already configured their `tmate` to connect to your server, you can easily find out what is the session ID and connect to it.

```admonish warning
You can totally invade on people's sessions >:D

Actually, anybody can :/
```

The session ID can be guessed (especially since you can name the session yourself - see documentation, section [named sessions](https://tmate.io/#named_sessions)).

```admonish success title="Solution"
To deal with this problem, you can use pass `authorized_keys` to `tmate`!

See documentation, section [access control](https://tmate.io/#access_control).
```

