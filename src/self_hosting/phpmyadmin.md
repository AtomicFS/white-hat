# phpMyAdmin

![](../img/construction.gif)

```admonish bug
I got this error when trying to login.
~~~
Failed to set session cookie. Maybe you are using HTTP instead of HTTPS to access phpMyAdmin.
~~~

In the end, the problem was caused by my extension `DarkReader`! Workaround was to disable it and [install dark theme](https://www.phpmyadmin.net/themes/) right into `phpMyAdmin` (I chose `darkwolf`).
```

