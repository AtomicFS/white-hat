# Self hosting

```admonish info
Self-hosting is the practice of hosting and managing applications on your own server(s) instead of consuming from [SaaSS](https://www.gnu.org/philosophy/who-does-that-server-really-serve.html){{footnote: Service as a Software Substitute}} providers.

[Source](https://github.com/awesome-selfhosted/awesome-selfhosted)
```

Few interesting links:
- [github.com/awesome-selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)
- Reddit
	- [r/selfhosted](https://www.reddit.com/r/selfhosted/)
	- [r/homelab](https://www.reddit.com/r/homelab/)
	- [r/HomeServer](https://www.reddit.com/r/HomeServer/)
	- [r/DataHoarder](https://www.reddit.com/r/DataHoarder/)
- [archlinux.org/title/Category:Servers](https://wiki.archlinux.org/title/Category:Servers)

