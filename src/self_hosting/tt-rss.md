# Tiny Tiny RSS (tt-rss)

![](../img/construction.gif)


I had so much pain getting this up and running.


```admonish bug
When following the [ArchLinux guide](https://wiki.archlinux.org/title/Tiny_Tiny_RSS), I have got following error:
~~~
2023/02/06 19:24:34 [error] 53880#53880: *1 "/usr/share/nginx/html/tt-rss/index.php" is forbidden (13: Permission denied), client: <REDACTED>, server: tt-rss.white-hat-hacker.icu, request: "GET / HTTP/2.0", host: "tt-rss.white-hat-hacker.icu"
~~~
```

```admonish solution
The solution is actually to [change the location of hosted files](https://bbs.archlinux.org/viewtopic.php?id=269626) - instead of
~~~
# ln -s /usr/share/webapps/tt-rss /usr/share/nginx/html/tt-rss
~~~

use this:
~~~
# ln -s /usr/share/webapps/tt-rss /srv/http/tt-rss
~~~
```

```admonish bug
Another problem I have seen appeared when I access the URL:
~~~
Exception while creating PDO object:could not find driver
~~~
```


