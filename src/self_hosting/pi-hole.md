# Pi-Hole

```admonish info
A black hole for Internet advertisements

[Source](https://pi-hole.net/)
```

```admonish info
[Documentation](https://docs.pi-hole.net/)
```

Follow guide at [ArchLinux wiki](https://wiki.archlinux.org/title/Pi-hole).

In essence install `pi-hole-server` from AUR and start `pihole-FTL.service`.


## Configuration

```admonish example collapsible=true title='/etc/nginx/sites-available/pihole.conf'
[/etc/nginx/sites-available/pihole.conf](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/etc/yadm/alt/etc/nginx/sites-available/pihole.conf%23%23default)
~~~nginx
{{#include ../../submodules/dotfiles-system/etc/yadm/alt/etc/nginx/sites-available/pihole.conf##default}}
~~~
```

```admonish example collapsible=true title='/etc/php/php.ini'
[/etc/php/php.ini](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/etc/yadm/alt/etc/php/php.ini%23%23hostname.falcon)
~~~ini
{{#include ../../submodules/dotfiles-system/etc/yadm/alt/etc/php/php.ini##hostname.falcon}}
~~~
```

It might be a good idea to restrict the access to the web GUI only to local network. This is super easy to do. I have created a `/etc/nginx/private.conf` file which then can be simply included in the site where you need it.

```admonish example title='/etc/nginx/private.conf'
[/etc/nginx/private.conf](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/etc/nginx/private.conf)
~~~nginx
{{#include ../../submodules/dotfiles-system/etc/nginx/private.conf}}
~~~
```

```admonish example title='/etc/nginx/sites-available/pihole.conf'
~~~nginx
server {
	...
	include private.conf;
	...
}
~~~
```

Do not forget to add the DNS into DHCP options. If you have [OpenWRT](https://openwrt.org/), then go to `Network -> Interfaces -> LAN -> DHCP server -> Advanced Settings` and into `DHCP-Options` type `6,<IP>` where `<IP>` is the IP address of Pi-hole. For example `6,192.168.1.12`.

Consider adding secondary DNS server as fallback when your pi-hol is down. Such as `6,192.168.1.12,9.9.9.9`.

Consider [setting up a password](https://wiki.archlinux.org/title/Pi-hole#Password-protected_web_interface).

Consider securing the server with HTTPS, see section [nginx](nginx.md).


## Gotchas

```admonish tip
If starting `pihole-FTL.service` fails, add following into `/etc/systemd/resolved.conf` and restart `systemd-resolved.service`:
~~~
[Resolve]
DNSStubListener=no
~~~
```

```admonish bug
~~~
While executing: attempt to write a readonly database error
~~~
This error happened to me the because I did not follow the instructions:
~~~
Updating... this may take a while. Please do not navigate away from or close this page. 
~~~

Solution is to re-run the gravity update of blacklists.
```

```admonish tip
If running on SSD, check out [Optimise for solid state drives](https://wiki.archlinux.org/title/Pi-hole#Optimise_for_solid_state_drives).
```

```admonish bug
You might see this error:
~~~
There was a problem applying your settings.
Debugging information:
PHP error (2): file_exists(): open_basedir restriction in effect. File(/var/log/apache2/error.log) is not within the allowed path(s): ...
~~~

If the path is added in the allowed paths, the check if the file exists, it not:
~~~
\$ mkdir -p /var/log/apache2
\$ touch /var/log/apache2/error.log
\$ chown pihole:pihole /var/log/apache2/error.log
~~~
```

```admonish warning
I did have problems when I tried to use the `open_basedir` option, the site would not work. In the end I just did not used it.
```

```admonish warning
It is not really feasible to version control the pi-hole dotfiles. But you can at least export and import the settings in the web UI.
```


## Additional blacklists

- [hagezi](https://github.com/hagezi/dns-blocklists)
	- nice list of blacklists (you can selectively pick what to block)
	- really good starting point
	- contains one list for Windows-specific junk
- [oisd](https://oisd.nl/downloads)
	- one big list :/
- [lightswitch05](https://github.com/lightswitch05/hosts)
	- contains lists for Facebook and dating sites
- [anudeepND](https://github.com/anudeepND/blacklist)
	- ads and Facebook
- [firebog.net](https://firebog.net/)
	- collection of lists
- [blocklistproject](https://github.com/blocklistproject/Lists)
- [crazy-max/WindowsSpyBlocker](https://github.com/crazy-max/WindowsSpyBlocker)
	- [blacklist for Windows junk](https://crazymax.dev/WindowsSpyBlocker/blocking-rules/hosts/)
- [StevenBlack](https://github.com/StevenBlack/hosts)

For more take a look at [reddit.com/r/pihole](https://www.reddit.com/r/pihole/search/?q=blocklists)

```admonish warning
The more blocklists you add, the more likely you'll come across a false positive. Consider adding whitelists.
```


## Whitelists

- [firebog.net](https://firebog.net/)
- [discourse.pi-hole.net](https://discourse.pi-hole.net/t/commonly-whitelisted-domains/212)
- [anudeepND/whitelist](https://github.com/anudeepND/whitelist)


## Test your setup

- [ads-blocker.com/testing](https://ads-blocker.com/testing/)
- [blockads.fivefilters.org](https://blockads.fivefilters.org/)

