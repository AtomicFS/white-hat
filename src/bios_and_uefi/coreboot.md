# coreboot

```admonish info
coreboot is one of the open alternatives to the BIOS and UEFI, and supports a great variety of hardware.

It does contain few proprietary software components, but they are "necessary evil" for CPU function (since CPU manufacturer refuses to provide source code or documentation).
```

Official homepage at [coreboot.org](https://www.coreboot.org/).

## Philosophy

UEFI is running all the time in background to provide various services to operating system. coreboot's philosophy is exact opposite - do the bare minimum to get the system working and hand over the control to operating system.


## Payloads

Additional features are added though [payloads](https://doc.coreboot.org/payloads.html) such as SeaBIOS or TianoCore. 

SeaBIOS is an open source implementation of a 16-bit x86 BIOS. SeaBIOS can run in an emulator (it is the default BIOS for the QEMU and KVM virtualization) or it can run natively on x86 hardware with the use of coreboot (SeaBIOS used as payload). It runs on 386 and later processors and requires a minimum of 1 MB of RAM. Compiled SeaBIOS images can be flashed into supported motherboards using [flashrom](https://www.flashrom.org/Flashrom).

## Supported devices

[coreboot.org/status/board-status.html](https://coreboot.org/status/board-status.html) (the site contains a lot of data and might be very slow).

