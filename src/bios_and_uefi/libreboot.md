# libreboot

```admonish info
libreboot is a fork of [coreboot](coreboot.md) with focus on removing all the proprietary components and sticking to true Free Software spirit. That unfortunately greatly limits the list of compatible hardware.

In addition, you get equivalent results by selecting specific hardware and configuration.
```

Official homepage at [libreboot.org](https://libreboot.org/).


# Supported devices

[List of supported hardware](https://libreboot.org/docs/hardware)

It is very unlikely that any modern hardware will ever be supported due to required [Intel ME and AMD PSP](../hardware/intel-me_and_amd-psp.md) since that would defy the whole point of LibreBOOT.

