# BIOS

```admonish info
BIOS stands for Basic Input/Output System and dates back into 1975. It initializes the hardware and loads the boot loader which then initializes the operating system.
```

Back in the day, BIOS provided a abstraction layer for operating system to access the hardware (keyboard, mouse, etc.), nowadays it is done by the operating system itself. It used to be stored in ROM on the motherboard, but in modern computers it is in flash memory to allow end-user updates (bug fixes, new features, etc.), which also opened a new vector of attacks (BIOS rootkits).

![Typical BIOS setup utility on a standard PC](../img/old/Award_BIOS_setup_utility.png)


BIOS has severe limitations, such as the BIOS must run in 16-bit mode, BIOS has only 1 MB of space to execute in, problems to initialize multiple hardware devices at once (longer boot time) and more.

BIOS uses MBR partition scheme. Because of that, BIOS can't use disk for booting larger than 2 TB.

