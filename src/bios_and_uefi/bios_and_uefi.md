# BIOS and UEFI

![](../img/construction.gif)

Sources:
- [howtogeek.com](https://www.howtogeek.com/56958/)
- [wikipedia.org/wiki/BIOS](https://en.wikipedia.org/wiki/BIOS)
- [wikipedia.org/wiki/Unified_Extensible_Firmware_Interface](https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface)

Both BIOS and UEFI are low-level software to initialize the hardware and boot OS.

BIOS is commonly a proprietary software made specifically for each motherboard model by the manufacturer.

Proprietary BIOS and UEFI firmware often contains back-doors, can be slow and has severe bugs (for more details see section [Intel ME and AMD PSP](../hardware/intel-me_and_amd-psp.md)). It contains bloat from decades ago for backwards compatibility to awkward and no longer used devices, sometimes even software for obscure interfaces not present on motherboard. Developers simply copy-paste the old code without a thought or any review.

Open alternatives are faster, more secure and more reliable than most non-free firmware options. In addition, they provide many advanced features, like encrypted `/boot` partition, GPG signature checking before booting kernel and more. Here are few specimens:

[coreboot](coreboot.md) started as LinuxBIOS back in a day when hardware was simple enough. Back then Linux kernel was used instead of BIOS. Since then, the hardware got more complicated as well as the boot sequence (for example nowadays memory has to be trained).

[libreboot](libreboot.md) is coreboot without proprietary blobs, but this is to certain degree redundant since coreboot build will not contain any blobs for motherboards that can run without them.

[u-boot](https://en.wikipedia.org/wiki/Das_U-Boot) is also open and de facto a standard for ARM devices. Although it is rumored that the code quality if worse than coreboot's.


## Intel, EFI and slimboot

14th of September 2018, Intel has announced open EFI implementation called slimboot, however it is widely despised, especially in coreboot community and for a good reasons.

Long-story short, Intel wanted coreboot to include their EFI, coreboot community refused since it contradicts their philosophy and so Intel created slimboot by forking coreboot, added their EFI and abandoned the project afterwards (there is practically no activity in slimboot git repository).

Sidenote: Intel is often trying to push their bullshit into coreboot community.

Here are links to the coreboot mailing-list thread:
- [Intel attempting to push into coreboot UEFI's payloads](https://www.mail-archive.com/coreboot@coreboot.org/msg55608.html)
	- [Response to intel from Peter Stuge](https://www.mail-archive.com/coreboot@coreboot.org/msg55612.html)
	- [Response from Ronnald G. Minnich](https://www.mail-archive.com/coreboot@coreboot.org/msg55615.html)

