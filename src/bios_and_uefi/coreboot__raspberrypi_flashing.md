# Raspberry Pi flashing

![](../img/construction.gif)


This is how I set-up my Raspberry Pi to be used as external flasher.

```admonish warning
Raspberry Pi makes for poor and over-complicated flashing tool, but it is often already in drawer ready for use. Consider getting dedicated programmer if you are going to flash motherboards on regular basis.
```


## Bill of Materials

- [Raspberry Pi](https://www.raspberrypi.com/) - I am going to use [Raspberry Pi 1 Model B+](https://www.raspberrypi.com/products/raspberry-pi-1-model-b-plus/) because that is what I have available at hand
- [Raspbian](https://www.raspberrypi.com/software/operating-systems/) - specifically [Raspberry Pi OS Lite](https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2022-04-07/2022-04-04-raspios-bullseye-armhf-lite.img.xz) based on Debian 11
- SOIC clip (SOIC-8, which is for 8 pin chips - Pomona 5250 or equivalent)


## Setting up Raspberry Pi

Here are briefly described steps I have made:
- Copy the operating system image to microSD card
- Mount the system partition
- Add ssh public key into `/root/.ssh/authorized_keys`
- Change `/etc/hostname` to desired hostname
- Assign static IP address to the Raspberry Pi in the router
- Place the card into Raspberry Pi and boot
- Go though the first boot setup
- Update the system with `apt update && apt upgrade`
- Install `openssh-server` and `flashrom` (and maybe `aptitude`)
- Configure the ssh by editing `/etc/ssh/sshd_config`
	- I intent to use root user with ssh keys, so enable root login and disable password authentication
- `raspi-config`
	- enable SPI interface
	- enable ssh
	- disable autologin (optional)


## Wiring

[Source: pinout.xyz](https://pinout.xyz/#)

![](../img/bios_and_uefi/raspberry-pi-pinout.png)

| Pin | Name    | Function           |
|:---:|:--------|:-------------------|
| 25  | GND     | Ground             |
| 24  | CS      | Chip Select        |
| 23  | SCK     | Serial clock input |
| 21  | DO      | Data Output        |
| 19  | DI      | Data Input         |
| 17  | 3.3 VCC | Power supply       |

The GPIO Pin 17 should be used only as power supply (do not connect external power to it), but it has a [limit of $50mA$](https://elinux.org/Rpi_Low-level_peripherals) and powering the chip in circuit might draw much more since other components might turn on as well, easily exceeding the current limit. Therefore external power supply might be needed.

In general the `WP` and `HOLD` pins on flash chip (usually pin 3 and pin 7) should be connected to `VCC` unless they are required to be floating or connected to `GND` (both extremely uncommon for SPI flash chips). Please consult the datasheet for the flash chip in question.


## Flashing

Whenever calling `flashrom`, you have to specify SPI interface and speed:
```
# flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=1000
```

Output example:
```
flashrom v1.2 on Linux 5.15.32+ (armv6l)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
Found Winbond flash chip "W25Q64.V" (8192 kB, SPI) on linux_spi.
No operations were specified.
```


