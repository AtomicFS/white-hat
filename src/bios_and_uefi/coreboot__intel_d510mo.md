# Intel D510MO

![](../img/construction.gif)


According to the [documentation](https://libreboot.org/docs/hardware/d510mo.html)
> This board has a working framebuffer in Grub, but in GNU/Linux in native resolution the display is unusable due to some raminit issues. This board can however be used for building a headless server.

Simply said, it has significant problems to display anything on screen with integrated GPU. I have tested old dedicated GPU in PCI port, but it was not getting any signal (it was working with proprietary BIOS).

Even with such limitation, it is still usable for headless server, NAS or similar use.

