# Resetting BIOS / UEFI passwords

This is a long story, which just recently had a update.

Long story short, I have set a supervisor password (password to access BIOS / UEFI settings) on my personal [Lenovo ThinkPad E531](https://pcsupport.lenovo.com/cz/en/products/laptops-and-netbooks/thinkpad-edge-laptops/thinkpad-edge-e531) laptop without writing it down. And of course I forgot it.

I kept using it since I could still access boot order menu, but eventually a needed to change settings protected by the supervisor password. And as one does in such situation, I searched the internet. (this is back in 2019)

I have quickly learned that this specific model does not store the password in CMOS memory (which can be easily erased) but in EEPROM, and that Lenovo does not offer any means of removal / reset of the password other than replacing the entire motherboard. Which in my opinion is unacceptable waste of perfectly functional hardware.

Even though it was likely to fail, I created a thread on [forums.lenovo.com](https://forums.lenovo.com/t5/ThinkPad-11e-Windows-13-E-and-Edge-series-Laptops/Lenovo-Thinkpad-E531-how-to-clear-supervisor-password-into-BIOS/m-p/4613822) in hopes of getting some help. Of course I got very useless response. However after successfully solving the problem myself, I have returned to that forum thread and asserted my dominance by letting everyone know that it is possible to bypass this so called "security feature".

> Lenovo Thinkpad E531 - how to clear supervisor password into BIOS
>
> Hi,
>
> I have a Lenovo Thinkpad E531. And some time ago I have set a supervisor password in BIOS, but I have unfortunatelly forgotten it.
>
> When I attemped to clear the password by clearing CMOS (by removing batteries), it did not help.
>
> How do you clear the supervisor password into BIOS?
>
>> Welcome to the Community.
>>
>> Short answer is, "you don't." A BIOS password wouldn't be much security if it could easily be changed. I'm afraid you'll either have to try harder to recall it or swap out the motherboard.
>>
>> Probably useless, but if you can find other passwords you set at about the same time you created this one, it might help. While it's not a good idea to use the same password for everything, I find anything that puts my brain into the same time frame as something I'm trying to remember, I have a better chance of remembering it. Could be just the way I'm wired.. :)
>>
>> Doc
>>> I have solved the problem by short-circuiting the EEPROM, bypassing the password.
>>>
>>> Never mind.

In following subsection are few methods of resetting BIOS / UEFI passwords that I know of.

Sources:
- [Ultimate Guide to Removing or Resetting a BIOS Password](https://www.online-tech-tips.com/cool-websites/reset-bios-password/)



## CMOS

Old devices have BIOS passwords stored in volatile memory backed by CMOS battery.

In this case the password can be removed (along with all BIOS settings) by removing all power-sources including the CMOS battery for few minutes.

![](https://s11986.pcdn.co/wp-content/uploads/2008/11/cmos-battery.jpg)

Alternatively there might be a jumper to clear the CMOS, which might save you some time.

![](https://www.online-tech-tips.com/wp-content/uploads/2008/11/clear-cmos.jpg)



## Backdoor password

Some devices offer a recovery option / backdoor. If you enter wring passwords multiple times, you might be presented with a unique code.

![](https://www.online-tech-tips.com/wp-content/uploads/2008/11/bios-password-disabled.jpg)

Normally you would have to call a tech support and provide them with this code, but there is a nifty website [bios-pw.org](https://bios-pw.org/) which is a collection of backdoor password generator for various BIOS and UEFI systems.



## Default password

[Generic BIOS password listings](https://www.computerhope.com/issues/ch000451.htm)



## EEPROM short-circuit (Lenovo ThinkPad E531)

In essence, you press power-on button, wait a certain time, temporarily short-circuit SDA and SCL pins on EEPROM, enter BIOS / UEFI and change password.

```admonish warning
Be very careful, you might damage you device with this method.
```

Your goal is to short the data pins on EEPROM exactly in the time that BIOS / UEFI tries to read the stored password(s). Thanks to shorting of the pins, it will fail and the system will behave like no password is set.

```admonish tip
I recommend using metallic tweezers to short the pins. I found it far more reliable and precise than screwdriver.
```

The timing, while critical, can vary greatly on your system and you have to experiment. For me, it took around 20 tries to get the exact timing. For example my system could not even start with shorted pins, but I had to short then very soon after.

After you successfully get into BIOS / UEFI without password (the attack was successful), you have to change the password. By changing it, you over-write the existing password in EEPROM which you just bypassed. Afterwards you can remove it by conventional means if you wish.

What really helped me was forum [allservice.ro](https://www.allservice.ro/forum/viewtopic.php?t=52) which hosts many pictures with EEPROM location.

![](https://www.allservice.ro/forum/images/e531.jpg)


Sources:
- Detailed guide at [Remove BIOS and Supervisor Password from Lenovo ThinkPad Laptops](https://www.dataimpact.nl/blog/removo-lenovo-thinkpad-bios-and-supervisor-password) ([FYI](https://www.urbandictionary.com/define.php?term=FYI) this article came out afterwards)
- [Resetting the EEPROM/Lost Supervisor Password on Lenovo Thinkpad T430s
](https://www.reddit.com/r/thinkpad/comments/4z9czz/resetting_the_eepromlost_supervisor_password_on/)



## UEFI embedded password (HP Zbook 15 G5)

Recently, I have acquired [HP Zbook 15 G5](https://support.hp.com/us-en/document/c06010510), unfortunately with UEFI password set (of course without knowing the password).

Even more unfortunately it seems that the password is set right in the SPI flash where UEFI resides. Also, this specific device came from corporate which disabled the [backdoor password](#backdoor-password) which could be normally used.

As far as I know, it should be possible to "simply" edit the UEFI ROM and remove the password. This is one of my future projects.


![](../img/construction.gif)

