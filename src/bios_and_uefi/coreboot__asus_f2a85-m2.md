# ASUS F2A85-M2

![](../img/construction.gif)


```admonish bug
At the moment I am experiencing a problem with memory. `Memtest86+` fails and in coreboot logs there is a `SPD READ ERROR`.

Also, `dmedecode -t memory` gives no output.
```

Rather neat board with socketted SPI flash chip for BIOS / UEFI.

My initial trials were less than successful, especially when it comes to `vgabios` which was rather problematic. But in the end I managed to get the board up and running with coreboot.

```admonish tip
If you have problems, use the serial console (the motherboard has 9-pin RS232 connector).
```


## Internal flashing and UEFI version

First of all, original UEFI version - coreboot docs state that internal flashing can be used for any version in between `v5018` and `v6402` (included), but `ASUS F2A85-M2` has different versioning system.

So I have looked at release dates. Let's take `v6402` for `ASUS F2A85-M` which was released 2013-07-26 according to [manufacturer's web](https://www.asus.com/US/supportonly/F2A85-M/HelpDesk_BIOS/). The closest match is `v0704` for `ASUS F2A85-M2` which was released on the exact same date according to [manufacturer's web](https://www.asus.com/us/supportonly/F2A85-M2/HelpDesk_BIOS/).

```admonish
My motherboard has custom UEFI varian claiming to be `v9904`, which made things a bit more complicated. I could use internal flashing, but I could not use UEFI's `ASUS EZ Flash 2 Utility` to flash `v0704`. It was complaining about downgrade, even though according to UEFI setup the release date for running UEFI sometime in 2012 (year older than `v0704`).

Although custom, the running UEFI version fit in between the versions with possible internal flashing.
```


## vgabios

I am not going to use dedicated GPU, so I want to get the integrated GPU running, for which I need `vgabios`.

`vgabios` is a bit complicated. First of all, in [coreboot docs](https://doc.coreboot.org/mainboard/asus/f2a85-m.html) there is very little information about this `ASUS F2A85-M2` so I have made few guesses. One of which was the UEFI version (I settled with `v0704`).

Then I used the suggested `MMTool Aptio v5.0.0.7` (which seems to be some proprietary software that is not openly available). Steps are described in [coreboot docs](https://doc.coreboot.org/mainboard/asus/f2a85-m.html#option-2-extract-from-the-vendor-binary). I have used the mentioned `v0704` UEFI version.

![](../img/bios_and_uefi/mmtool.png)

The CPU I intend to use is `AMD A8-6500`, which according to [cpu-world.com](https://www.cpu-world.com/CPUs/Bulldozer/AMD-A8-Series%20A8-6500%20-%20AD6500OKA44HL.html) has integrated `Radeon HD 8570D` which translates to VGA device PCI ID `1002,990e`.

The I placed the `vgabios` into root directory of coreboot and named it `vgabios.bin`
```
coreboot
|-- ...
|-- vgabios.bin
|-- .config
|-- Makefile
|-- README.md
`-- ...
```


## Configuration

My `.config` file can be found in [sourcehut repository](https://git.sr.ht/~atomicfs/coreboot__f2a85-m2)

In `Mainboard` select vendor `ASUS` and model `F2A85-M`, the rest can remain on default.

```admonish warning
Select `DDR3 memory voltage` according to your used RAM modules!
```

Then with `make menuconfig`, in `Devices` I have enabled `Add a VGA BIOS image` and set `VGA device PCI IDs` to `1002,990e` as mentioned above.

As primary payload I use `SeaBIOS`, with secondary payloads `coreinfo`, `Memtest86+` and `nvramcui`.

Also, in `System tables` I have changed `SMBIOS Product name` from `ASUS F2A85-M` to `ASUS F2A85-M2`, but this is only cosmetic change.

And that is it. Configuration is now complete.


## Flashing

Just use internal programmer. If the board does not boot, just take out the SPI flash and program it externally in breadboard or some doodad.

