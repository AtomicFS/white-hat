# UEFI

![](../img/construction.gif)


```admonish info
UEFI stands for Unified Extended Firmware Interface. It aims to replace BIOS and is designed to not have the limitations of BIOS. Intel has started to work on EFI specification in 1998 and in 2007 Intel, AMD, Microsoft and others agreed to UEFI.
```

UEFI uses GPT partition table allowing it to boot from devices larger than 2 TB.


## UEFI Criticism

UEFI and even more it's feature SecureBoot has been heavily criticized from OpenSource community. Since there have been many attempts to cut out any operating system other then Microsoft Windows from booting. Citation from [wikipedia](https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface):

> Numerous digital rights activists have protested against UEFI. Ronald G. Minnich, a co-author of coreboot, and Cory Doctorow, a digital rights activist, have criticized EFI as an attempt to remove the ability of the user to truly control the computer. It does not solve any of the BIOS's long-standing problems of requiring two different drivers-one for the firmware and one for the operating system-for most hardware.

> In 2011, Microsoft announced that computers certified to run its Windows 8 operating system had to ship with secure boot enabled using a Microsoft private key. Following the announcement, the company was accused by critics and free software/open source advocates (including the Free Software Foundation) of trying to use the secure boot functionality of UEFI to hinder or outright prevent the installation of alternative operating systems such as Linux. Microsoft denied that the secure boot requirement was intended to serve as a form of lock-in, and clarified its requirements by stating that Intel-based systems certified for Windows 8 must allow secure boot to enter custom mode or be disabled, but not on systems using the ARM architecture. Windows 10 allows OEMs to decide whether or not secure boot can be managed by users of their x86 systems.

More information can be found in interview with Ronald G. Minnich at [archive.fosdem.org](https://archive.fosdem.org/2007/interview/ronald%2bg%2bminnich.html) or in article [The Coming War on General Purpose Computation](https://boingboing.net/2011/12/27/the-coming-war-on-general-purp.html).

