# Gigabyte GA-G41M-ES2L

![](../img/construction.gif)


This board is possible to flash internally - even with original BIOS.

While I attempted to do that, I failed and board is now bricked, since the guide forgot to mention to do complete shutdown instead reboot after flashing. After it rebooted, the board was bricked. I attempted to re-flash again externally, but the chip was not recognized - possibly damaged. Now, I am waiting for delivery of new chip to solder on the board instead (after flashing). Clearing CMOS does not help.


