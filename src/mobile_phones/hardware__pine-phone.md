# PipePhone

```admonish warning title="DISCLAIMER"
I am just a customer and these are my opinions. I am not affiliated with PinePhone or Pine64.
```

I have purchased [PINEPHONE Beta Edition with Convergence Package Linux SmartPhone](https://pine64.com/product/pinephone-beta-edition-with-convergence-package/), and later I also got myself the [keyboard case](https://pine64.com/product/pinephone-pinephone-pro-keyboard-case/) right after it became available.

PinePhone specs:
- CPU: 64-bit Quad-core 1.2 GHz ARM Cortex A-53
- RAM: 3GB LPDDR3 SDRAM
- Storage: 32GB eMMC

```admonish info
[PinePhone wiki](https://wiki.pine64.org/wiki/Main_Page)
```

```admonish info
[Keyboad case manual](https://files.pine64.org/doc/PinePhone/USER_MANUAL-KEYBOARD-V2-EN-DE-FR-ES.pdf)
```

```admonish bug
When using the keyboard case, do not charge your phone via the USB-C on your phone, but via the USB-C in the keyboard case. You will fuck-up the changing circuit. See manual.
```

```admonish warning
PinePhone Pro has different boot order! PinePhone boots primarily from SD card, then eMMC. PinePhone pro boots primarily from eMMC, booting the Manjaro u-Boot, taking away control over the bootloader. This is unacceptable.
```


## p-boot demo on SD card

[p-boot-demo](https://xnux.eu/p-boot-demo/) is definitely a must-have. In essence, it is a collection of multiple Operating Systems for PinePhone, and it allows you to quickly try out various operating systems. Download link is in the `Download` section.

![](https://xnux.eu/p-boot-demo/menu2.webp)

### Installation

Decompress the archive
```
\$ zstd -d multi.img.zst
```

Write the image to SD card (in this example the SD card shows up as `/dev/sdc` block device)
```
# dd if=multi.img of=/dev/sdc bs=4M oflag=direct status=progress
```

Resize the second partition with `fdisk`
```
# fdisk /dev/sdc
```

- Delete 2nd partition
- Create new partition
	- Partition number: 2
	- Partition type: primary
	- First sector: 409600
	- Do not remove the file-system signature


```admonish warning
So not forget to change first sector!

The original partition table had 2nd partition starting at sector `409600`.

~~~
Disk /dev/sdc: 14.52 GiB, 15590227968 bytes, 30449664 sectors
Disk model: UHS-II SD Reader
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x12345678

Device     Boot  Start      End  Sectors  Size Id Type
/dev/sdc1  *      8192   409599   401408  196M 83 Linux
/dev/sdc2       409600 20479999 20070400  9.6G 83 Linux
~~~
```

```
Welcome to fdisk (util-linux 2.38.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): d
Partition number (1,2, default 2): 

Partition 2 has been deleted.

Command (m for help): n
Partition type
   p   primary (1 primary, 0 extended, 3 free)
   e   extended (container for logical partitions)
Select (default p): 

Using default response p.
Partition number (2-4, default 2): 
First sector (2048-30449663, default 2048): 409600
Last sector, +/-sectors or +/-size{K,M,G,T,P} (409600-30449663, default 30449663): 

Created a new partition 2 of type 'Linux' and of size 14.3 GiB.
Partition #2 contains a btrfs signature.

Do you want to remove the signature? [Y]es/[N]o: n

Command (m for help): w

The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
```

The resize the `BTRFS` file-system.
```
# mount /dev/sdc2 /mnt/
# btrfs filesystem resize max /mnt/
# sync
# umount /mnt
# eject /dev/sdc
```


## Flash eMMC

Firstly use the [p-boot](#p-boot-demo-on-sd-card) to boot `JumpDrive`, and then connect the phone via USB-C cable to you phone. The `JumpDrive` will expose eMMC and SD card is block devices.

You can then use `dd` to write image you wish to install.

```
# dd if=Manjaro-ARM-phosh-pinephone-beta28.img of=/dev/sdc bs=4M oflag=direct conv=fsync status=progress
```


## Keyboard tools

Firstly install `yay`
```
# pacman -S --needed base-devel
\$ git clone https://aur.archlinux.org/yay.git
\$ cd yay
\$ makepkg -si
```

Then install `pinephone-keyboard-git`
```
\$ yay -S pinephone-keyboard-git
```

Now you can use `ppkb-i2c-charger-ctl` to interact with the battery charger.



# Review

Initially I started with `DanctNIX` (ArchLinux), mostly because I like ArchLinux but also because encrypted system partition with `dm-crypt` is a must-have. Unfortunately there are many problems, mostly caused by broken Pine64 community.

I also started with `SXMO`, but soon had to switch to `Phosh`. `SXMO` is nice and super-cool, but unfinished. For example you can't send / press numerical keys during call. This is important when calling companies to navigate though answering machine.

Right now, I am using Manjaro with Phosh, just because it seems to be the least broken option.

Overall, this phone has a lot of pros and cons.

Pros:
- You can purchase spare parts
- One of few Open-Source friendly phones on market that did not die few months after launch

Cons:
- GSM module is fucked, often goes offline during sleep without warning
- Hight power-consumption and low battery capacity
- Available Operating Systems other than Manjaro have poor support from Pine64 (manufacturer)
	- TLDR:
		- Pine64 likes Manjaro and nobody else
		- Manjaro is a bunch of noobs
		- Nobody ever upstreams their changes so each distro must re-invent the wheel to get it working
	- Long version (drama alert):
		- [Martijn Braam's blog post about leaving Pine64](https://blog.brixit.nl/why-i-left-pine64/)
		- [Pine64 response to Martijn Braam's blog post](https://www.pine64.org/2022/08/18/a-response-to-martijns-blog/)
		- [Drew DeVault's blog post about Pine64 community](https://drewdevault.com/2022/08/18/PINE64-let-us-down.html)

Verdict (after almost 2 years of daily use):
- Would I recommend this phone to "normal" people as daily driver?
	- **No way**
- Would I recommend this phone to Linux geeks as daily driver?
	- **No**
- Would I recommend this phone to Linux geeks as a toy?
	- **Maybe**
- I am happy with my purchase of this phone?
	- **Not really, I am disappointed with the software and Pine64 community**
- Would I buy this phone again?
	- **No**

Just to say, my requirements for phone are functional and reliable GSM, running Linux, WiFi. That's it, my demands are very low.

Since this phone is such a disaster, I am actually considering to resurrect my old project to build [RaspberryPi-phone](https://git.sr.ht/~atomicfs/handheld-pitop) based on RaspberryPi-Zero.


## Few interesting sources

- [hackaday.com / Open Firmware For PinePhone LTE Modem – What’s Up With That?](https://hackaday.com/2022/07/12/open-firmware-for-pinephone-lte-modem-whats-up-with-that/)

