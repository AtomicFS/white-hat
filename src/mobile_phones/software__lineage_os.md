# LineageOS

![](../img/construction.gif)


```admonish info
[LineageOS](https://www.lineageos.org/) is a free and open-source operating system for various devices, based on the Android mobile platform.
```

In essence, replacement for [Android](software__android.md).

- [List of compatible devices](https://wiki.lineageos.org/devices/)

Some phones are better than others. If you are considering to purchase a phone witch is supported by LineageOS, definitelly check out the [community section](https://www.lineageos.org/community/), where people often write about their experiences.

For example here is a [reddit post about Redmi 5 Plus](https://www.reddit.com/r/LineageOS/comments/8afknq/comment/dwypol6/?utm_source=share&utm_medium=web2x&context=3) from 2018:

> I own a `Redmi 5 Plus`. With the new generation (5) you have to wait 360 hours to unlock your bootloader. Previously it was 72 hours, but now it's an actual half month.
>
> Also consider that there are tons of steps to perform before that - create a Mi Account, apply for having your Mi account set up as a developer account, add your devices to your Mi developer account, ensuring your ROM can even unlock the bootloader (there's a switch in MIUI, and my ROM couldn't, I had to flash the Global Dev ROM to do it, which you can do without unlocking the bootloader).
>
> All of this creates a hurdled process just to be able to flash a ROM that isn't made by Xiaomi, so I wouldn't recommend Xiaomi for LineageOS.


