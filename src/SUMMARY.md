# Summary

[White-Hat-Hacker](README.md)

---

# Generic notes

- [Constants](constants.md)
- [Abbreviations](abbreviations.md)

---

# Operating Systems

- [Operating Systems](operating_systems/operating_systems.md)
	- [Unix, Linux and computer revolution](operating_systems/operating_systems__unix-like.md)
	- [Lightweight](operating_systems/operating_systems__lightweight.md)
	- [Windows](operating_systems/operating_systems__windows.md)
	- [Windows-like](operating_systems/operating_systems__windows-like.md)
- [Linux](linux/linux.md)
	- [btrfs](linux/btrfs.md)
	- [dm-crypt](linux/dm-crypt.md)
		- [Remote unlock](linux/dm-crypt__remote_unlock.md)
			- [GRUB config](linux/dm-crypt__remote_unlock__grub.md)
			- [REFInd config](linux/dm-crypt__remote_unlock__refind.md)
			- [SSH key generation](linux/dm-crypt__remote_unlock__ssh-keygen.md)
	- [ssh](linux/ssh.md)
	- [dotfiles](linux/dotfiles.md)
	- [Archive encryption](linux/archive_encryption.md)
	- [Identify file type](linux/identify_file_type.md)
- [Linux Distributions](linux_distributions/linux_distributions.md)
- [ArchLinux](archlinux/archlinux.md)
	- [Installation](archlinux/installation.md)
		- [Manual](archlinux/installation_manual.md)
		- [Automatic](archlinux/installation_automatic.md)
	- [pacman](archlinux/pacman.md)
		- [pacman: cache](archlinux/pacman-cache.md)
		- [pacman: custom packages](archlinux/pacman-custom_packages.md)
		- [pacman: custom repository](archlinux/pacman-custom_repository.md)
	- [Meta package](archlinux/meta_package.md)

---

# Software

- [Licenses](licenses/licenses.md)
	- [Microsoft Windows EULA](licenses/microsoft_windows_eula.md)
- [Software](empty.md)
	- [Programming languages](programming_languages/programming_languages.md)
		- [C](programming_languages/c.md)
		- [JavaScript](programming_languages/javascript.md)

---

# Hardware

- [BIOS and UEFI](bios_and_uefi/bios_and_uefi.md)
	- [BIOS](bios_and_uefi/bios.md)
	- [UEFI](bios_and_uefi/uefi.md)
	- [coreboot](bios_and_uefi/coreboot.md)
		- [General notes](bios_and_uefi/coreboot__general.md)
		- [Raspberry Pi flashing](bios_and_uefi/coreboot__raspberrypi_flashing.md)
		- [Intel D510MO](bios_and_uefi/coreboot__intel_d510mo.md)
		- [Lenovo Thinkpad x230](bios_and_uefi/coreboot__thinkpad_x230.md)
		- [Gigabyte GA-G41M-ES2L](bios_and_uefi/coreboot__gigabyte_ga-g41m-es2l.md)
		- [ASUS F2A85-M2](bios_and_uefi/coreboot__asus_f2a85-m2.md)
	- [libreboot](bios_and_uefi/libreboot.md)
	- [Resetting BIOS / UEFI passwords](bios_and_uefi/resetting_bios_passwords.md)
- [Hardware](empty.md)
	- [Intel ME and AMD PSP](hardware/intel-me_and_amd-psp.md)
- [Mobile phones](mobile_phones/mobile_phones.md)
	- [Harware](mobile_phones/hardware.md)
		- [PinePhone](mobile_phones/hardware__pine-phone.md)
	- [Software](mobile_phones/software.md)
		- [Android](mobile_phones/software__android.md)
		- [LineageOS](mobile_phones/software__lineage_os.md)
- [Networking](empty.md)
	- [Naming conventions and ideas](networking/naming_conventions.md)
- [Cybersecurity](cybersecurity/cybersecurity.md)
	- [DNS sinkhole](cybersecurity/dns_sinkhole.md)
	- [VPN](cybersecurity/vpn.md)
	- [Malware](cybersecurity/malware.md)
		- [WannaCry](cybersecurity/malware__wannacry.md)
		- [Stuxnet](cybersecurity/malware__stuxnet.md)
	- [Sex toys](cybersecurity/sex_toys.md)
	- [Archive encryption](cybersecurity/archive_encryption.md)
	- [Famous hackers](cybersecurity/famous_hackers.md)
		- [Adrian Lamo](cybersecurity/famous_hackers__adrian_lamo.md)
		- [Kevin Mitnick](cybersecurity/famous_hackers__kevin_mitnick.md)
	- [Public-key cryptography](cybersecurity/public-key_cryptography.md)

---

# Everything sucks

- [Work from office sucks](everything_sucks/work_from_office.md)
- [Web sucks](everything_sucks/web_sucks.md)
- [Gaming sucks](everything_sucks/gaming_sucks.md)
```
	- [Speedrunning](gaming/speedrunning.md)
```
- [Music sucks](everything_sucks/music_sucks.md)
```
- [Photography](everything_sucks/photography.md)
- [Science scandals](everything_sucks/science_scandals.md)
```

---

# Daily Driving Linux Setup

- [sway](daily_driver_linux_setup/sway.md)
- [aerc](daily_driver_linux_setup/aerc.md)
	- [The manual setup](daily_driver_linux_setup/aerc_manual_setup.md)
	- [The automatic setup](daily_driver_linux_setup/aerc_automatic_setup.md)
- [tmate](daily_driver_linux_setup/tmate.md)
- [Multiboot USB](daily_driver_linux_setup/multiboot_usb.md)
- [GRUB ISO](daily_driver_linux_setup/grub_iso.md)

---

# Self hosting

- [Self hosting](self_hosting/self_hosting.md)
	- [MariaDB](self_hosting/mariadb.md)
	- [Paperless](self_hosting/paperless.md)
	- [Pi-hole](self_hosting/pi-hole.md)
	- [Tiny TIny RSS](self_hosting/tt-rss.md)
	- [nginx](self_hosting/nginx.md)
	- [phpMyAdmin](self_hosting/phpmyadmin.md)
	- [tmate](self_hosting/tmate.md)

---

# Miscellaneous

- [Baud rate](miscellaneous/baud_rate.md)
- [Markdown](miscellaneous/markdown.md)
	- [syntax highlight](miscellaneous/markdown__syntax_highlight.md)
- [mdbook](miscellaneous/mdbook.md)
- [Mailto links](miscellaneous/mailto_links.md)
- [Audio / Noise levels](miscellaneous/noise_levels.md)
- [My favorite quotes](miscellaneous/my_favorite_quotes.md)
- [My favorite memes](miscellaneous/my_favorite_memes.md)

---

- [template](_notes_/template.md)

