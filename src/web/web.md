# Web

![](../img/construction.gif)

When accessing any website on the internet, your broswer basically sends a request and server responds by sending bunch of files with at least one HTML file.

- HTML
- CSS
- JS

HTML can load JS by `script tag`, which can be provided by the server on anywhere else on the internet.

```admonish example title="index.html"
~~~html
<html>
<body>
<script src="/index.js" />
</body>
</html>
~~~
```

JS is client-side executed.
	- change page
	- receive data
	- send data


Client-side frameworks / Front end frameworks that build on top of JS to make writing client-side code simpler:
- React
- Angular
- Vue
- Svelte

JS is interpreted language

Browsers have `JavaScript engine`

C-like syntax (`if` statements, loops and so on)

Browsers often have JIT (Just in Time) compilers to speed up

NodeJS is server-side JS, used to write API, access databases and comunicate with other servers. NodeJS is based on V8 engine (just like Chrome) and allows JS to run outside of browser.
Firefox: SpiderMonkey
Chrome: V8

ECMASAcript is specification, JavaScript is language conforming to the specification. From time to time they release new specification (for example ES2015/ES6)


## Variables

Primitives
- int, bool, string

Compound
- array
	- ordered cluster of data

```js
const arr = [1,2,3]
```

```
console.log(arr[0])
1
```

- object

```js
const obj = {
	msg: "hello world",
	number: 1
	}
```

```
console.log(obj.msg);
"hello world"
```

Variables are declared with `let` or `const`.

```js
let num = 1
num = 2
const msg = "hello world"
```

# The DOM

DOM is the most important object in JS, it is structured JS representation of the HTML and is present in variable `document`.

The `document` object contains many `element` objects.

```js
document.querySelector()
element.addEventListener()
element.style.color
```



`for in` loop to iterate over object
```js
const obj = {
	msg: "hello world",
	number: 1
}
for (let key in obj) {
	console.log(key, obj[key]);
}
```

`for of` loop to iterate over array
```js
const arr = [1,2,3]
for (let num of arr) {
	console.log(num);
}
```


# Function
```js
function add(fist, second){
	return first+second;
}
```

# Method

Like function, but attached to data-type
```js
"hi".toUpperCase()
['c','b','a'].sort()
[1,2,3].indexOf(1)
```
```
"HI"
['a','b','c']
0
```

# Arrow function

```js
(a,b) => a+b
```

Quite often used in array methods
```js
[1,2].map(n=>n+1)
```

```
[2,3]
```


# Async programming

We do not know when finished nor if successful.

```js
fetch(http://localhost:3000/moreData)
```

Current standard is `async/await`

```js
async function getAsync(URL) {
	const response = await fetch(URL)
}
```

this will pause the program and wait until response it received or the request fails.


# Typescript

typescritp "compiles" into JS

JS has dynamic types
```js
const msg = "hello world"
```

TS has statis types
```ts
const msg string = "hello world"
```


# npm

node package manager



- Excellent video that explains the very basic principles: [Learn JAVASCRIPT in just 5 MINUTES (2020)](https://www.youtube.com/watch?v=c-I5S_zTwAc)


