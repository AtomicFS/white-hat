# dotfiles

![](../img/construction.gif)

**TODO:**
- [Dotfiles](https://wiki.archlinux.org/title/Dotfiles)
- [How I Over-Engineered My Dotfiles](https://bananamafia.dev/post/dotfiles/)


```admonish info
User-specific application configuration is traditionally stored in so called dotfiles (files whose filename starts with a dot). It is common practice to track dotfiles with a version control system such as Git to keep track of changes and synchronize dotfiles across various hosts.
```

There is a [article on archlinux wiki](https://wiki.archlinux.org/title/Dotfiles) about doftiles.

I am fan of [yadm](https://yadm.io/), which is what I am using and what I will focus on here.

`yadm` is based on git (basically just a git wrapper), with all of git's features with addition of alternative files, templates, bootstrapping script and hooks. [Documentation for yadm](https://yadm.io/docs/overview).

I am tracking two separate set of dotfiles - [user dotfiles](https://git.sr.ht/~atomicfs/dotfiles/) and [system dotfiles](https://git.sr.ht/~atomicfs/dotfiles-system) - therefore I have two repositories. Both these repositories are still work in progress, but I guess that is always the case with dotfiles xD.


## system dotfiles

### Starting from scratch

Here I took inspiration from [yadm FAQ](https://yadm.io/docs/faq#unconventional-cases).

The plan is to manage system dotfiles only unser root user, therefore I create a alias for root ([`/root/.bashrc`](https://git.sr.ht/~atomicfs/dotfiles-system/tree/master/item/root/.bashrc)):
```
alias yadm='sudo yadm --yadm-dir /etc/yadm'
```

```admonish warning
There is `sudo` used in the alias because of problems with environment variables. Something I should fix at some point.
```

Initialize the yadm repo using the worktree of `/`
```
# yadm init -w /
```

You should see something like:
```
Initialized empty shared Git repository in /root/.local/share/yadm/repo.git/
```

You can add remote:
```
yadm remote add origin git@git.sr.ht:~atomicfs/dotfiles-system
```

The you can start using it as git repository:
```
# yadm add /root/.bashrc
# yadm commit -m "added .bashrc file"
# yadm push --set-upstream origin master
```

