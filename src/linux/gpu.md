# Multiple AMD GPU's

![](../img/construction.gif)


```
sudo lshw -C display
```

```
  *-display                 
       description: VGA compatible controller
       product: Tonga XT / Amethyst XT [Radeon R9 380X / R9 M295X]
       vendor: Advanced Micro Devices, Inc. [AMD/ATI]
       physical id: 0
       bus info: pci@0000:09:00.0
       version: f1
       width: 64 bits
       clock: 33MHz
       capabilities: pm pciexpress msi vga_controller bus_master cap_list rom
       configuration: driver=amdgpu latency=0
       resources: irq:109 memory:c0000000-cfffffff memory:d0000000-d01fffff ioport:1000(size=256) memory:d9e00000-d9e3ffff memory:d9e40000-d9e5ffff
  *-display
       description: VGA compatible controller
       product: Tonga XT / Amethyst XT [Radeon R9 380X / R9 M295X]
       vendor: Advanced Micro Devices, Inc. [AMD/ATI]
       physical id: 0
       bus info: pci@0000:41:00.0
       version: f1
       width: 64 bits
       clock: 33MHz
       capabilities: pm pciexpress msi vga_controller bus_master cap_list rom
       configuration: driver=amdgpu latency=0
       resources: irq:111 memory:80000000-8fffffff memory:90000000-901fffff ioport:3000(size=256) memory:9ff00000-9ff3ffff memory:c0000-dffff
```

```
radeontop -b 9
radeontop -b 41
```

