# REFInd

Configuration for [REFInd](https://wiki.archlinux.org/index.php/REFInd), located at `ESP/refind.conf` (`ESP` is [EFI system partition](https://wiki.archlinux.org/index.php/EFI_system_partition)).

```admonish warning
I am no longer using REFInd on any of my devices, I prefer to use [GRUB](dm-crypt__remote_unlock__grub.md). Therefore this page is no longer maintained.
```

Most of the things are identical to [GRUB](dm-crypt__remote_unlock__grub.md) configuration.


```admonish example
~~~
"Boot with standard options"  "cryptdevice=/dev/nvme0n1p2:system_luks root=/dev/system_vg/root ip=dhcp rw add_efi_memmap initrd=initramfs-%v.img"
~~~
```

