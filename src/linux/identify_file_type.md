# Identify file type

![](../img/construction.gif)


```admonish info
In Linux you can use `file` command (package also called `file`) find the file type based on the actual file, indepentently on filename and extension.
```

For example when run on 7z file:
```
\$ file unknown.123
unknown.123: 7-zip archive data, version 0.4
```

Or when run on ordinary text file:
```
\$ file unknown.abc
unknown.abc: ASCII text, with very long lines (1042)
```

```admonish warning
One of the tools that forensic analytics use is filter that detects discrepancies between file extension and file type. **Such files are flagged as suspicious and only draw more attention.**

Let's that that user renames a `dick_pic.png` file to `dick_pic.zip`. On windows the file will appear corrupted since the extension is the primary source of file type information. However on Linux this is not always the case. Not to mention that you can easily with single quick command find the truth.

Surprisingly many people think that this is legitimate way of hiding the content of the file from prying eyes.
```

