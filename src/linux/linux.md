# Linux {{footnote: [https://ascii.co.uk/art/tux](https://ascii.co.uk/art/tux)}}

```
         _nnnn_        
        dGGGGMMb       
       @p~qp~~qMb      
       M|@||@) M|      
       @,----.JM|      
      JS^\__/  qKL     
     dZP        qKRb   
    dZP          qKKb  
   fZP            SMMb 
   HZM            MMMM 
   FqM            MMMM 
 __| ".        |\dS"qML
 |    `.       | `' \Zq
_)      \.___.,|     .'
\____   )MMMMMP|   .'  
     `-'       `--' hjm
```

```admonish info
[Linux](https://en.wikipedia.org/wiki/Linux) is an open-source Unix-like operating system based on the Linux kernel, an operating system kernel first released on September 17, 1991, by Linus Torvalds. Linux is typically packaged as a Linux distribution.

[Source](https://en.wikipedia.org/wiki/Linux)
```

