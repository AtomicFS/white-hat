# Archive encryption

![](../img/construction.gif)


It is easy to protect archive with a password, however common process will not encrypt the header (which contains the tree structure with file names).

```admonish warning
You can list contents of the archive without password.
```

```
\$ 7z l test.zip
```
```
7-Zip [64] 17.04 : Copyright (c) 1999-2021 Igor Pavlov : 2017-08-28
p7zip Version 17.04 (locale=en_CS.UTF-8,Utf16=on,HugeFiles=on,64 bits,24 CPUs x64)

Scanning the drive for archives:
1 file, 3366 bytes (4 KiB)

Listing archive: test.zip

--
Path = test.zip
Type = zip
Physical Size = 3366

   Date      Time    Attr         Size   Compressed  Name
------------------- ----- ------------ ------------  ------------------------
2022-05-11 22:12:29 .....         9399         3150  test.txt
------------------- ----- ------------ ------------  ------------------------
2022-05-11 22:12:29               9399         3150  1 files
```

~~~admonish info
You can use `p7zip` program to encrypt the header information:
```
\$ 7za -p'password' -mhe=on a archive_name.7z folder_name
\$ 7za -p -mhe=on a archive_name.7z folder_name
```
~~~

Where `-p'password'` sets the password and `-mhe=on` will enable encrypting the header. If there is no string with password, user will be prompted to enter it - this way it will not be logged in terminal history.

With header encrypted, you cannot access the information without password.
```
\$ 7z l test.7z
```

```
7-Zip [64] 17.04 : Copyright (c) 1999-2021 Igor Pavlov : 2017-08-28
p7zip Version 17.04 (locale=en_CS.UTF-8,Utf16=on,HugeFiles=on,64 bits,24 CPUs x64)

Scanning the drive for archives:
1 file, 3215 bytes (4 KiB)

Listing archive: test.7z


Enter password (will not be echoed):

ERROR: test.7z : Can not open encrypted archive. Wrong password?

ERRORS:
Headers Error


Errors: 1
```

