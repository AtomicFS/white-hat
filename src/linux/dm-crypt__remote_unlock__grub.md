# GRUB config

Configuration for [GRUB](https://wiki.archlinux.org/title/GRUB), located at `/etc/default/grub`.

Edit `/etc/default/grub` and add network configuration into `GRUB_CMDLINE_LINUX_DEFAULT` attribute.

Most of the information is from [ArchLinux wiki](https://wiki.archlinux.org/title/Dm-crypt/Specialties#Remote_unlocking_(hooks:_netconf,_dropbear,_tinyssh,_ppp)).

There are multiple ways to configure this, however my favorite is to use IP address assigned by DHCP server, essentially:
```
ip=dhcp
```

I also like to add `netconf_timeout`, especially on laptop, otherwise the device will hand and indefinitely wait for IP address from DHCP.
```
netconf_timeout=10
```

```admonish example
Example with UUID
~~~ini
{{#include ../..//submodules/dotfiles-system/etc/yadm/alt/etc/default/grub##hostname.ark:6}}
~~~
```

It is also possible to use multiple configurations. You can add a specific network configuration into GRUB menu as additional entry. That way, you can pick on boot whether you wish to use DHCP or hard-coded IP address.

