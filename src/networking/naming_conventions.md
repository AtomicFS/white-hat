# Naming conventions and ideas

![](../img/construction.gif)


[Naming Schemes](https://namingschemes.com/)

> A good naming scheme is scalable, unique, and easy to remember. The purpose of these naming schemes is to name networked servers, wireless access points or client computers, but it can also be used to name projects, products, variables, streets, pets, kids, or any other project where unique names and rememberable names are required.

With multiple devices on network, it is a good idea to establish a naming scheme.

Example of one scheme (source no longer available):
> My wifi network is "Periodic" and that leads itself to all sorts of sub-naming schemes:
> - Linux machines are noble gasses (Xenon, Argon, Neon, etc.)
> - Desktops and laptops are transition metals (Iridium, Titanium, Zirconium, etc.)
> - Phones are non-metals (Carbon, Oxygen, Nitrogen, etc.)
> - Virtual machines are radioactive (those machines have a short half-life; Plutonium, Uranium, etc.)
> - My tethering network is Actinoids
> - My wife's tethering network is Lanthanoids

Or medieval scheme from [linustechtips.com](https://linustechtips.com/main/topic/549876-what-are-your-computers-names/?page=5&tab=comments#comment-7297934):

>Currently everything I have is medieval ages inspired. The network is "The Kingdom" and the devices on it are as follows: King (My main rig) Queen (My wifes) Prince and Princess (You can guess) Scribe (printer) MiniMessenger (My phone) Jester (TV PC) and so on and so forth for other computers that could fit into a castle somehow.


---
## List of ideas by origin

### Firefly
- Ares
- Hera
- Miranda
- Osiris
- Serenity
- Whitefall

### Elements
- Argon
- Boron
- Chrom
- Cobalt
- Krypton
- Mercury
- Neon
- Radon
- Selen
- Thor
- Titan
- Tungsten
- Uran
- Xenon

### Stars, planets, moons, probes
- Charon
- Dragonfly
- Europa
- Horizon
- Iocaste
- Pandora
- Prometheus
- Saturn
- Thebe
- Venus

### EVE online ships
- Ark
- Avatar
- Blackbird
- Caracal
- Cerberus
- Charon
- Chimera
- Claw
- Corax
- Falcon
- Golem
- Guardian
- Heron
- Hyperion
- Jaguar
- Legion
- Leviathan
- Merlin
- Nomad
- Oracle
- Paladin
- Panther
- Phoenix
- Providence
- Raptor
- Raven
- Reaper
- Rook
- Scorpion
- Sentinel
- Thorax
- Tristan
- Typhoon
- Zealot

### Human names
- Adam
- Cassandra
- Falcon (Robert Falcon Scott)
- Jack
- Norman
- Bismarck
- Gaspar/Caspar, Melchior, and Balthasar/Balthazar (Bible / Evangelion)

### Top500 supercomputers
- Excalibur
- Pangea
- Thunder
- Topaz
- Trinity
- Vulcan

### Movies and series
- Akira
- Andrew
- Cairon
- Defiant
- Flynn
- Guardian
- Leviathan
- Sam
- Sentinel
- Sullivan
- Weebo
- Calculon (Futurama)
- Tinny Tim (Futurama / Markiplier)
- Tyderium (StarWars)
- Tiberius (That's the T in James T Kirk)

### Games: Crysis, DeusEx, TimeShift
- Alcatraz
- Prophet
- Sarif
- SSAM

### Random
- Hydra
- Phantom
- Turing
- Orca
- Infinity
- Orion
- Pegasus
- Cosmos
- Dwarf
- Neutron
- Tau
- Neuron
- Buttons
- Prism
- Hemlock
- BigBrother


---
## Pre-sorted by application

### Desktop
- Phoenix
- Norman
- Hyperion
- Krypton
- Radon
- Raptor

### Laptop
- Falcon
- Nomad

### Server-like
- Leviathan
- Whitefall
- Legion
- Hyperion
- Horizon
- Cassandra

### NAS
- Ark
- Charon
- Providence

### Router
- Sullivan
- Paladin
- Blackbird
- Alcatraz

### Switch
- Corax
- Chimera

### Firewall
- Alcatraz
- Defiant
- Gatekeeper
- Guardian
- Heimdall
- Rook

### Intrusion detection system (IDS)
- Zealot
- Sentinel
- Scorpion

### Phone
- Dragonfly
- Merlin
- Sam
- Topaz

### Email server
- Hermes
- Pigeon

