# Photography

![](../img/construction.gif)

Canon 550D, 600D

- [Feature comparison matrix](https://builds.magiclantern.fm/features.html)
- [Random guy on the internet camera recommendations](https://magiclanternshooter.com/recommendedgear/cameras/)
- [Open Source cameras](https://apertus.org/)
