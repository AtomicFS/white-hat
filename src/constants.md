# Constants

| Quantity           | Symbol    | Value             | Units   |
|:-------------------|:----------|------------------:|--------:|
| Speed of Light     | $c_0$     | 299792458         | $m/s$   |
| Speed of Sound     |           | 340.29            | $m/s$   |
| Pi                 | $\pi$     | 3.141592653589793 |         |
| Euler's number     | e         | 2.718281828459045 |         |
| Gravity of Earth   | g         | 9.780327          | $m/s^2$ |

