# Intel ME and AMD PSP

![](../img/construction.gif)


[SOURCE](https://libreboot.org/faq.html\#intelme}{libreboot.org/faq)

Relatively recent discoveries about hidden "features" in Intel and AMD processors such as Intel ME (Management Engine) and AMD PSP (Platform Security Processor) has risen many serious concerns.

It was later followed by discovery of fatal flaws in the architecture - most famous is SPECTER and MELTDOWN which are possible thanks to the speculative computing. These flaws in architecture in addition to hidden operating system inside every CPU has lead to significant traction and serious security and privacy concerns (2017 and especially in 2018).

```admonish info
Intel ME is an independent co-processor, running custom operating system (based on MINIX 3), integrated inside CPU and it is the base hardware for many features like Intel AMT (Active Management Technology - for remote management), Intel Boot Guard, Intel PAVP (Platform Embedded Security Technology Revealed) and many others. To provide such features, it requires full access to the system, including memory (through DMA) and network access.

Intel ME is in every processor since 2006 and AMD PSP since 2013.
```

There is overwhelming number concerns about these systems as they present an almost undetectable back-door into any and every computer. After the reveal of Intel ME, many vulnerabilities have been discovered and attempts to remove, disable or replace Intel ME have been made. In essence the Intel ME and most likely AMD PSP are extremely flawed and vulnerable and present great risk to security and privacy (more info at [wikipedia.org/wiki/Intel_Management_Engine](https://en.wikipedia.org/wiki/Intel_Management_Engine)).

```admonish warning
Intel Management Engine is a severe threat to privacy and security, not to mention freedom, since it is a remote backdoor that provides remote access to a computer where it is present (publicly admitted by Intel).
```


## Option 1 - run old hardware

It is possible to completely avoid the Intel ME and AMD PSP by simply running on old hardware from era preceding them. This solution isn't too bad, assuming use of AMD components - where one can go as recent as 2013.

To improve the security on such device (and sometimes even performance), it is recommended to flash the motherboard with alternative firmware such as [coreboot](../bios_and_uefi/coreboot.md).


## Option 2 - reduce effects

Another option is to modify the already present firmware, which in case of Intel ME is possible with use of [ME Cleaner](https://github.com/corna/me_cleaner). ME Cleaner is a Python script able to modify an Intel ME image with the final purpose of reducing its ability to interact with the system.

According to the [GitHub documentation](https://github.com/corna/me_cleaner/wiki/How-to-apply-me_cleaner), it takes the firmware image, then it is modified and flashed back. External flashing is recommended.

Since multitude of devices are also supported by [coreboot](../bios_and_uefi/coreboot.md), coreboot in combination with Me Cleaner will significantly reduce the effects. For example Thinkpad x230 has Intel ME in it, but it can be reduced from 5.2 MB to mere 98.3 kB - reduction to less than 2% of original size.


[Quote from ME Cleaner's README](https://github.com/corna/me_cleaner/blob/master/README.md#what-can-be-done):
> Before Nehalem (ME version 6, 2008/2009) the ME firmware could be removed completely from the flash chip by setting a couple of bits inside the flash descriptor, effectively disabling it.
>
> Starting from Nehalem the Intel ME firmware can't be removed anymore: without a valid firmware the PC shuts off forcefully after 30 minutes, probably as an attempt to enforce the Intel Anti-Theft policies.
>
> However, while Intel ME can't be turned off completely, it is still possible to modify its firmware up to a point where Intel ME is active only during the boot process, effectively disabling it during the normal operation, which is what ME Cleaner tries to accomplish.

