# Music sucks

![](../img/construction.gif)

## Copyright industry is ruining the music and musicians

- [Music Collection Org: Revenues are booming ... and that's proof why we need even more draconian copyright laws](https://www.techdirt.com/2019/11/18/music-collection-org-revenues-are-booming-thats-proof-why-we-need-even-more-draconian-copyright-laws/)
	- TLDR: The annual revenue reports show record hight profits. But it is not enough, they want more!
- [Music Piracy Continues To Drop Dramatically, But The Industry Hates To Admit That Because It Ruins The Narrative](https://www.techdirt.com/2019/10/07/music-piracy-continues-to-drop-dramatically-industry-hates-to-admit-that-because-it-ruins-narrative/)
	- TLDR: The piracy is dropping, but industry refuses to acknowledge it.
- [Major Labels Claim Copyright Over Public Domain Songs; YouTube Punishes Musician](https://www.techdirt.com/2012/08/28/major-labels-claim-copyright-over-public-domain-songs-youtube-punishes-musician/)
	- TLDR: Musician Dave Colvin makes music, but gets slammed with copyright claims.


- [RIAA Launches Brand New Front Group Pretending To Represent Independent Artists](https://www.techdirt.com/2021/02/01/riaa-launches-brand-new-front-group-pretending-to-represent-independent-artists/)


## Fight against copyright

- [Attempt To Put Every Musical Melody Into The Public Domain Demonstrates Craziness Of Modern Copyright](https://www.techdirt.com/2020/02/21/attempt-to-put-every-musical-melody-into-public-domain-demonstrates-craziness-modern-copyright/)
	- TLDR: There is a limited number of possible melodies to be made, and to copyright-claim them is stupid (common practice nowadays). So Damien Riehl and Noah Rubin create a program to generate all possible melodies and put them into public domain to shield musicians from copyright claims.
- [Tom Lehrer, Still Awesome, Releases Lyrics Into The Public Domain](https://www.techdirt.com/2020/10/23/tom-lehrer-still-awesome-releases-lyrics-into-public-domain/)


## Questionable

- [Another Acquisition: Epic May Be Moving Beyond Just Gaming By Acquiring Bandcamp](https://www.techdirt.com/2022/03/10/another-acquisition-epic-may-be-moving-beyond-just-gaming-by-acquiring-bandcamp/)
	- [Why I love Bandcamp: Waived-fee Fridays, solid app, no DRM, more](https://arstechnica.com/gaming/2021/08/today-is-bandcamp-friday-put-100-of-your-music-purchases-in-bands-hands/)

