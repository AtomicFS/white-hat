# Work from office sucks

```admonish warning
{{#include ../_notes_/everything_sucks_warning.md}}
```

With [COVID19](https://en.wikipedia.org/wiki/Coronavirus_disease_2019), there was a massive wave of work from home. And recently there has been a push from employers and managers to get back into office. There is no denial, a lot of people like working from home and it is not going away.

```admonish info
[87% of those currently working from home said they would like to continue to do so](https://www.forbes.com/sites/davidrvetter/2020/06/16/how-working-from-home-could-save-11-billion-road-miles-cut-emissions/)
```

So I thought I would try to create a brief yet comprehensive overview of benefits and drawbacks, along with arguments from both sides.

There is a lot to cover, so I tried to separate it into sections by individual topics. Each section has also a very brief [TLDR](https://www.urbandictionary.com/define.php?term=TLDR) subsection with summary.


---
# Ecological effects

Over the years we have seen a lot of coverage, and a lot of contradicting arguments. Just search [work from home environmental impact](https://duckduckgo.com/?t=ffab&q=work+from+home+environmental+impact&ia=web) and you will be overwhelmed with articles of various opinions and qualities.

Few examples:
- [6 Surprising Environmental Impacts of Remotely Working from Home](https://earth.org/environmental-impacts-of-remotely-working-from-home/)
- [Telecommuting Could Save U.S. Over $700 Billion a Year and Much More](https://globalworkplaceanalytics.com/cut-oil)
- [5 Ways Companies Can Embrace Sustainability in a Remote Work Setting And How It Will Impact Their Future Growth](https://www.globalization-partners.com/blog/5-ways-companies-can-embrace-sustainability-in-a-remote-work-setting-and-how-it-will-impact-their-future-growth-2/)
- [How Eco-Friendly Is Remote Working?](https://www.forbes.com/sites/adigaskell/2021/10/21/how-eco-friendly-is-remote-working/)

So, let's break it down.


## Less commuting = less emissions?

```admonish info
There is no doubt that less commuting to work means less emissions - smaller \\(CO_2\\) footprint, cleaner air in cities, less traffic.
```

According to [Globalization Partners](https://www.globalization-partners.com/blog/5-ways-companies-can-embrace-sustainability-in-a-remote-work-setting-and-how-it-will-impact-their-future-growth-2/), emissions were reduced by 25% to 34% in London due to [COVID19](https://en.wikipedia.org/wiki/Coronavirus_disease_2019) confinement measures. Forbes released an article: [How Working From Home Could Save 11 Billion Road Miles, Cut Emissions](https://www.forbes.com/sites/davidrvetter/2020/06/16/how-working-from-home-could-save-11-billion-road-miles-cut-emissions/).

On top of that the [average American spends 52 minutes commuting](https://www.census.gov/library/visualizations/interactive/travel-time.html) to work.

Employees often with the new found flexibility will travel more to enjoy themselves, partially canceling out the initial savings in commuting. I have not found data to show how much more travel that is, but even if it cancels out completely, isn't it better to spend the \\(CO_2\\) footprint on enjoyable and relaxing trips rather then simply commute to work?

In world where it is either one or the other, I would argue that it is far better investment to work from home and travel for relaxation.


## Waste of furniture and office equipment

Another argument against working from home is that reducing office space increases furniture waste.

This is nothing else than [sunk cost fallacy](https://en.wikipedia.org/wiki/Sunk_cost) and mentality "I bought it, so you will use it!". If you do not want to throw it away, sell it. If you can't sell it, then donate it. I am certain that a lot of people will welcome free office equipment for their new home office. Or give it to the next office renter.

The other side of the argument is the waste on employees side. They have purchased furniture and equipment to create a comfortable home office, which would go wasted if they have to get back to office. 

In my personal opinion, this argument is invalid and frankly sounds hypocritical to me.


## Higher energy consumption per household

Well, the only difference is where the electricity is consumed - office or home. Solution is simple - subsidize the bill.


## TLDR
```admonish tldr
There is no argument here, work from home is better for the environment.
```


---
# Financial effects and Real estate

```admonish warning title="Disclaimer"
To demonstrate the financial effects, I will focus on New York City, since I am a following it's situation for some time.
```

The property in New York was always pricey thanks to high demand, which has given birth to Billionaires' Row - in essence a safety deposit (safe investment) for the rich.

```admonish info
There is a well made documentary made by [B1M: Why New York's Billionaires' Row Is Half Empty](https://www.youtube.com/watch?v=Wehsz38P74g).
```

However many of the properties are vacant for years, some even from time before [COVID19](https://en.wikipedia.org/wiki/Coronavirus_disease_2019) (see YouTube playlist of [walkthroughs thought dead city neighborhood](https://www.youtube.com/playlist?list=PLkVbIsAWN2lt_n88l7WC2u22TRHKmw8ya)). Then of course the pandemic has amplified the problem, thanks to lock downs and work from home. Since companies do not need the office space, the value is slowly going down as demonstrated in [supply and demand](https://en.wikipedia.org/wiki/Supply_and_demand) economic model.

![](../img/everything_sucks/supply-demand-equilibrium.png)

This is influencing also the government since according to [New York City budget](https://comptroller.nyc.gov/reports/comments-on-new-york-citys-fiscal-year-2023-adopted-budget/) around 31% of the income is from property tax, which is determined from value of the property. New York will have to adjust to [$9 billion dollar plunge in NYC commercial real estate](https://nypost.com/2023/01/21/9b-plunge-in-nyc-real-estate-sets-up-brutal-political-fight/).

[After Pandemic, Shrinking Need for Office Space Could Crush Landlords](https://www.nytimes.com/2021/04/08/business/economy/office-buildings-remote-work.html) ... this resembles China's problem with real estate.


## China real estate bubble

For some context, majority of real-estate in China is sold as investment.

```admonish info
In China, general population has very limited options to invest. [Deutsche Welle News: Homebuyers pay price for China's property meltdown](https://www.youtube.com/watch?v=t9gnrhEP0tQ) provides a general overview.
```

This lead to entire cities being vacant, owned as investment. China's government then introduced a [three red lines policy in 2020](https://en.wikipedia.org/wiki/2020%E2%80%932022_Chinese_property_sector_crisis#The_%22three_red_lines%22_and_other_Chinese_regulations) which triggered a domino effect. Developers could not lend more money and could not finish already started projects. People did not want to pay mortgages for unfinished houses, especially since the construction has halted with no hope of resuming. Developers had to undercut the market to sell any real estate to raise capital, which in turn caused drop of real estate prices first time in 20 years.

```admonish info
[29% of China's GDP was real estate in 2021](https://www.theguardian.com/world/2021/oct/15/chinas-booming-real-estate-market-could-spell-trouble-for-the-economy), which is huge!

For comparison, in [USA real estate accounted for 17% in 2022](https://www.nar.realtor/blogs/economists-outlook/how-do-home-sales-affect-the-economy-and-the-job-market-in-your-state).
```

This bubble is ready to burst for some time, and the pop will likely be worse than the [2008 financial crisis](https://en.wikipedia.org/wiki/2007%E2%80%932008_financial_crisis) simply due to the shear size.


## 12 billion lost, or gained?

Well, it very much depends on the perspective, look at the phrasing of this article: [Remote Work Costing Manhattan More Than $12 Billion a Year, Report Says](https://www.nbcnewyork.com/news/local/__trashed-25/4101088/). In essence, the article is about people spending less money near their offices. But look at the wording - it is phrased that city and business are loosing money. Why?

One could also argue that it is actually 12 billion dollars saved by the employees, or simply spend elsewhere - in their local communities and small local business. So let me rephrase it:

```admonish success
Remote Work Saves Workers More Than $12 Billion a Year, Report Says.
```


## Desperate attempts to fix the New York's problems

[New York has passes new legislation to tax remote workers](https://www.bloomberg.com/opinion/articles/2023-01-20/us-remote-work-tax-rules-are-a-confusing-mess), resulting in double taxation if the employee does not live in New York but the employer resides there. In essence, employee has to pay tax in state of residence as well as tax in New York.

Regarding the real estate, property owners refuse to lower the prices, leading to low occupancy. Low occupancy then leads to lower traffic in the area which decreases profitability of said property. This in turn further lowers the attractiveness for potential renters or buyers, killing the occupancy numbers. Welcome infinite loop called real estate bubble.

There were some attempts to convert commercial real estate into residential, but that is not always possible. Consider your own requirements for apartment (even cheap one) - you will likely expect as bare minimum running water, toilet, shower and window. Now imagine typical office building ... how on earth you will slice it into apartments? The associated costs with the conversion would be astronomical.

![](../img/everything_sucks/office-building-layout.jpg)

Not to mention the necessary legal challenge of changing the property from commercial into residential.


## TLDR
```admonish tldr
Overpriced real estate property is common problem and now those who are hurting the most (financially) are government and property owners.

They force you to go back to office to stop hemorrhaging money they feel entitled to.
```


---
# Work efficiency

Now a bit from my personal perspective. When the mandatory work from home hit me, it was hard - mostly because the company I worked for was not ready to support work from home. But eventually it improved and I started to like it.

I have saved a lot of money thanks to home cooking and had healthier foods. I could better arrange my day and work to my needs, it gave me great flexibility and I felt much less stressed.


## Why programmers wear headphones and listen to music all the time?

While I was in the office, I was more-less forced to wear headphones and listen to music all the time. My office was not too big (thankfully not the open space monstrosity), but it had a few people in it.

And as programmer, my work requires concentration and creativity - basically I needed to get into the zone to be really productive. And this does take time, 30 minutes if I am lucky, hour or two when unlucky. And any interruption means I have to start all over - be it email notification, somebody asking me what is 1+1 or just slurping coffee too loud - it does not matter.

This is seriously frustrating and exhausting.

![](../img/everything_sucks/focus.png)

So one line of defense was to wear headphones to filter out the distracting elements and colleagues. Even though I hated to listen to music while working and it hindered my concentration, it was still preferable.

Important to say, I have nothing against them, it is just that they are not compatible with the zone.

And I am not the only one, there are other people having the same exact problem. Just a sample:
- [How do you get into the zone? How long does it take? What steps do you take before?](https://softwareengineering.stackexchange.com/questions/20542/how-do-you-get-into-the-zone-how-long-does-it-take-what-steps-do-you-take-befo)
- [Software Engineers: How often do you get in the "zone" or "flow" while at work?](https://www.reddit.com/r/cscareerquestions/comments/335jcb/software_engineers_how_often_do_you_get_in_the/)


## Are other people also more efficient from home?

According to article published by [Tech.co: Working From Home Is More Efficient Than Ever](https://tech.co/news/report-working-from-home-efficient).

```admonish quote
In a [January report](https://wfhresearch.com/wp-content/uploads/2022/01/Barrero-AEA-NABE-Jan22.pdf) based off this survey:
- 22.9% of workers said working remotely was **hugely better** than they'd expected
- 22% said it was **substantially better**
- 26.1% found it to be **about the same**
- 5.7% said it was **worse**
```

```admonish quote
Last month, for instance, [we covered a study](https://tech.co/news/remote-workers-optimistic-in-office-employees) out from the ADP Research Institute, which found that remote employees are slightly more optimistic than non-remote workers — 90% of remote workers report job satisfaction, while 82% of commuters do the same.

Plus, a full 64% of employees say they'd consider starting up a job search if their current boss asked them to return to the office full time.
```


## What about socializing? Face-to-face feels better!

Well ... no. I myself am introvert and socializing drains me. Not to mention the [in the zone problems](#why-programmers-wear-headphones-and-listen-to-music-all-the-time).

Of course everything depends on the nature of the work. Trying to solve some technical problem with colleagues is better in the office, but listening to corporate lorem-ipsum power-point presentation is better spent with wireless headphones in the kitchen cooking lunch.


## TLDR

```admonish tldr
Has my efficiency increased? Yes, definitely. But not only that, my happiness increased too.
```


---
# Managers: The ultimate conspiracy

So after all of the arguments above, your manager still demands everyone to come back to the office and refuses to listen to voice of reason? As usual there is plethora of possible explanations, ranging from classic nonsense, all the way to large scale conspiracies. The [financial aspect](#financial-effects-and-real-estate) falls into the large-scale conspiracy. So to balance is out, here are some smaller-scale examples with anecdotal evidence.

![](../img/everything_sucks/cubicle.jpg)


## Paid for results, not time

When the employee is in the office, it is very simple - the time starts with arrival and ends with departure. However with work from home, the employer looses this means of control and suddenly must trust the employee to put in the hours. Suddenly the only tangible evidence is the output (results). Interesting article about this from [Forbes: The Real Reasons Why Companies Don't Want You To Work Remotely - Results Not Hours](https://www.forbes.com/sites/jackkelly/2021/08/17/the-real-reasons-why-companies-dont-want-you-to-work-remotely/).

This is of course unacceptable to greedy overlords, because the employee could be done with work in half the time and slack of for the remainder, and yet be paid in full.

But why is this wrong? Shouldn't this be a reward to the employee for doing things more efficiently? For optimizing their work workflow or simply getting better at what they do? It is a common practice in classic on-premises work that efficient and fast employees are rewarded with more work. Which results in dissatisfaction and obvious countermeasures - "look busy" or "pretend you are working". There is simply no incentive for the employees to do better.

And work from home places the employee out of manager's supervision, so they can't so easily assign more work to them when they look "not busy".

Funnily enough, the sudden freedom from hawkish supervision opens up new possibilities, such as [over-employment](https://www.bbc.com/worklife/article/20210927-the-overemployed-workers-juggling-remote-jobs) (there is a interesting subreddit [r/overemployed](https://www.reddit.com/r/overemployed/)). Maybe people practising over-employment do not reccomend it though. It is demanding and not sustainable in long run.

```admonish info
Over-employment is in essence joggling multiple employments at the same time. For example a experienced and good senior programmer will get employed twice for junior position. Junior positions because expectations are lower and deadlines are easy to meet. The idea being that monthly income from two or more junior positions is greater than from one senior position.

As with everything, over-employment has pros and cons.

Pros:
- Higher pay
- Lower fear of loosing job

Cons:
- Constant switching between tasks
- Stress from colliding meetings
- Increasing demands often result in burnout
- Might be illegal in certain locations
```

But maybe this whole approach should change as a study from [Stanford professor](https://www.cnbc.com/2019/03/20/stanford-study-longer-hours-doesnt-make-you-more-productive-heres-how-to-get-more-done-by-doing-less.html) shows:

```admonish quote
In his research, economics professor John Pencavel found that **productivity per hour decline sharply when a person works more than 50 hours a week**. After 55 hours, productivity drops so much that **putting in any more hours would be pointless**. And, those who work up to 70 hours a week are only getting the same amount of work done as those who put in the 55 hours.
```

With this being said, employers should heavily invest their time into figuring out how to incentives working faster and more efficiently, instead of punishment. In a way, work from home seems like a good balance since the employee can "go home early" with no negative impact.

Then there is also the trade-off of productive and unproductive hours - when I found myself unproductive, rather than pushing though my limits, I would take time off (play a video-game for example) and return to work later. Basically exchanging useless time I would otherwise spend staring at the problem with no results, for a time of peak efficiency in late afternoon or maybe even weekend. However this requires mutual trust between employers and employees, which can be a problem.


## Micromanagement and trust issues

This one is more on the bad manager side. Managers with need for micromanagement or trust issues will of course despise work from home. Suddenly they can't check if you work or slack off, they can't harass you as much as they would like, and so on.

[Forbes: The Real Reasons Why Companies Don't Want You To Work Remotely - Middle Management Is Running Scared](https://www.forbes.com/sites/jackkelly/2021/08/17/the-real-reasons-why-companies-dont-want-you-to-work-remotely/)

This is not a problem of work from home, but problem of toxic management. And should be addressed as such.

![](../img/everything_sucks/image.png)


## Incompetent and manipulative management

Another example of bad and toxic management. Incompetent managers hate work from home, because suddenly their incompetence is visible and gaslights is harder. Let me elaborate.

```admonish info
[Gaslighting](https://en.wikipedia.org/wiki/Gaslighting) is basically conscious intent to brainwash. It is a form of manipulation and psychological control, often associated with abusive relationships.

[What is gaslighting with examples](https://www.medicalnewstoday.com/articles/gaslighting#gaslighting-examples).
```

When working from home, it is much harder to keep the level of plausible deniability. When talking to someone in person, there is not record of what was said (under normal circumstances), and therefore it is possible to gaslight the employee.

However with work from home, it is not that easy to do when everything is written down in email. Not to mention that phone or video call conversations can be easily recorded.


---
# Conclusion

If your employees like to work from home, let them. It is as simple as that. Listen to them and threat them as one of the most, if not the most, valuable asset.

Sticking to work from office just to keep price of real estate high and wallets of select few full is unsustainable and selfish. Sticking to work from office just to keep and eye on employees with hawkish supervision and distrust is avoiding the root cause of the problem - "employee = lazy thief" mindset.

In the end, it boils down to single question:

```admonish question
Is is morally acceptable to exploit majority of population for profit of few individuals?
```

I would like to answer this question with a quote from my favorite TV show Star Trek:

```admonish quote
The needs of the many outweigh the needs of the few.
```


## Is this whole argument communist?

Maybe ... maybe ... but considering that automation is lately replacing more jobs than are added yearly, sooner or later a fundamental shift will be inevitable.

Nice made video from [Kurzgesagt: The Rise of the Machines – Why Automation is Different this Time](https://www.youtube.com/watch?v=WSKi8HfcxEk).

```admonish tip
The society in Star Trek is technically socialist utopia, however it is necessary thanks to clean energy and [replicators](https://en.wikipedia.org/wiki/Replicator_(Star_Trek)), which in essence rendered majority of jobs unnecessary. [Captain Picard talks about economics of the future](https://www.youtube.com/watch?v=PV4Oze9JEU0&t=119s).
```

If we want to improve peoples lives, and truly take better care of the environment, we have to change.


## Do I hate managers?

Depends, I hate people like this:
- [CEO Celebrates Worker Who Sold Family Dog After He Demanded They Return to Office](https://www.vice.com/en/article/wxj574/ceo-celebrates-worker-who-sold-family-dog-after-he-demanded-they-return-to-office)
- [The ‘Pity City’ CEO Is Sorry Now](https://www.vice.com/en/article/ak3gy8/the-pity-city-ceo-is-sorry-now)

