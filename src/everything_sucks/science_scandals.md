# Science scandals

![](../img/construction.gif)

- [The man who almost faked his way to a Nobel Prize](https://www.youtube.com/watch?v=nfDoml-Db64)
- [The man who tried to fake an element](https://www.youtube.com/watch?v=Qe5WT22-AO8), the race for the periodic table.


