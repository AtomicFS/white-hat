# Speedrunning

![](../img/construction.gif)

I am not a speedrunner, not am I involved in the speedrunning community. But as I consider myself a casual gamer, I am aware of speedrunning and some things from this community do interest me.


## The Boba skip
Wholesome story about accidental discovery which has shaken the Metal Gear Solid 1 speedrunning.

[TLDR](https://www.urbandictionary.com/define.php?term=TLDR): A gaming streamer finds a glitch in Metal Gear Solid, which allows player to skip lenghty section of the game (allows to skip at least 2 minutes! In speedrunning, every second counts). And the whole speedrunning community goes bananas.
- [Casual Streamer Accidentally Finds HUGE Skip That Speedrunners Missed](https://www.youtube.com/watch?v=mv0Hwb0Se4E)


## Fascinating tools used for speedrunning
It is quite interesting to see how speedrunnes create and use tools to help them speedrun. And this speedrun looks like magic.
- [zylenox: Minecraft Speedrun World Record (2023-02-08)](https://www.youtube.com/watch?v=eFbXCm4VRyg)
	- [Karl Jobst: The Greatest Speedrun In Minecraft History Just Happened](https://www.youtube.com/watch?v=9hgNfdJiOjQ)


## The Dream scandal
This is a story about rise and fall of popular gamer, speedrunner, youtuber and streamer called [Dream](https://www.youtube.com/@dream).

[TLDR](https://www.urbandictionary.com/define.php?term=TLDR): Known speedrunner cheats and get's caught. Dream firstly denies all the accusations but later admits his game was modified giving him unfair advantage. Also proceeds to argue that he did not know. Overall, it is just weird mess full of chaos and misinformation.

- [Karl Jobst: The Biggest Cheating Scandal In Speedrunning History](https://www.youtube.com/watch?v=f8TlTaTHgzo), really good video with explanation of Minecraft, game mechanics and analysis of speedrun by player Dream. Also covers a bit problems with randomness and why would speedrunners cheat.
	- Quite lenghty video (1h 16m) with a lot of drama: [Karl Jobst: Dream's Cheating Confession: Uncovering The Truth](https://www.youtube.com/watch?v=G3Yzk-3SZfs) (watch only if really interested this specific scandal, otherwise it is a waste of time)
	- TLDR: It is not a question whether or not Dream cheated (it was proven he cheated), but rather if it was intentional or not. It is possible that Dream has made a honest mistake and then terribly handled the following communication.

