# Gaming

## Steam
[Steam Flatpak installation](https://wiki.archlinux.org/title/Steam#Alternative_Flatpak_installation)
```
flatpak --user remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
flatpak --user install flathub com.valvesoftware.Steam
flatpak run com.valvesoftware.Steam
flatpak override --user com.valvesoftware.Steam --filesystem=/data/games/steam
```


## protontricks
[protontricks](https://flathub.org/apps/details/com.github.Matoking.protontricks)
```
flatpak --user install com.github.Matoking.protontricks
flatpak override --user com.github.Matoking.protontricks --filesystem=/data/games/steam
```

```
flatpak run com.github.Matoking.protontricks
```

