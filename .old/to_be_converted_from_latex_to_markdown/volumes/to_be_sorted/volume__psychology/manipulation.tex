\section{Manipulation}
\label{ch:psychology_manipulation}
\source{https://en.wikipedia.org/wiki/Psychological_manipulation}{wikipedia.org/wiki/Psychological\_manipulation}\\\\
Psychological manipulation is a type of social influence that aims to change the behavior or perception of others through abusive, deceptive, or underhanded tactics. By advancing the interests of the manipulator, often at another's expense, such methods could be considered exploitative, abusive, devious, and deceptive.\\\\
According to psychology author George K. Simon, successful psychological manipulation primarily involves the manipulator:

\begin{itemize}
\item Concealing aggressive intentions and behaviors and being affable.
\item Knowing the psychological vulnerabilities of the victim to determine which tactics are likely to be the most effective.
\item Having a sufficient level of ruthlessness to have no qualms about causing harm to the victim if necessary.
\end{itemize}

Consequently, the manipulation is likely to be accomplished through covert aggressive (relational aggressive or passive aggressive) means.\\\\
How manipulators control their victims - According to Harriet B. Braiker (2004), who identified the following ways that manipulators control their victims:

\begin{itemize}
\item Positive reinforcement: includes praise, superficial charm, superficial sympathy (crocodile tears), excessive apologizing, money, approval, gifts, attention, facial expressions such as a forced laugh or smile, and public recognition.
\item Negative reinforcement: involves removing one from a negative situation as a reward, e.g. "You won't have to do your homework if you allow me to do this to you."
\item Intermittent or partial reinforcement: Partial or intermittent negative reinforcement can create an effective climate of fear and doubt. Partial or intermittent positive reinforcement can encourage the victim to persist - for example in most forms of gambling, the gambler is likely to win now and again but still lose money overall.
\item Punishment: includes nagging, yelling, the silent treatment, intimidation, threats, swearing, emotional blackmail, the guilt trip, sulking, crying, and playing the victim.
\item Traumatic one-trial learning: using verbal abuse, explosive anger, or other intimidating behavior to establish dominance or superiority; even one incident of such behavior can condition or train victims to avoid upsetting, confronting or contradicting the manipulator.
\end{itemize}

%========================================================================================
%========================================================================================

\subsection{Manipulative techniques}
Simon identified the following manipulative techniques:
\begin{itemize}
\item \tbf{Lying (by commission):} It is hard to tell if somebody is lying at the time they do it, although often the truth may be apparent later when it is too late. One way to minimize the chances of being lied to is to understand that some personality types (particularly psychopaths) are experts at the art of lying and cheating, doing it frequently, and often in subtle ways.
\item \tbf{Lying by omission:} This is a very subtle form of lying by withholding a significant amount of the truth. This technique is also used in propaganda.
\item \tbf{Denial:} Manipulator refuses to admit that they have done something wrong.
\item \tbf{Rationalization:} An excuse made by the manipulator for inappropriate behavior. Rationalization is closely related to spin.
\item \tbf{Minimization:} This is a type of denial coupled with rationalization. The manipulator asserts that their behavior is not as harmful or irresponsible as someone else was suggesting, for example, saying that a taunt or insult was only a joke.
\item \tbf{Selective inattention or selective attention:} Manipulator refuses to pay attention to anything that may distract from their agenda, saying things like "I don't want to hear it".
\item \tbf{Diversion:} Manipulator not giving a straight answer to a straight question and instead being diversionary, steering the conversation onto another topic.
\item \tbf{Evasion:} Similar to diversion but giving irrelevant, rambling, vague responses, weasel words.
\item \tbf{Covert intimidation:} Manipulator throwing the victim onto the defensive by using veiled (subtle, indirect or implied) threats.
\item \tbf{Guilt trip:} A special kind of intimidation tactic. A manipulator suggests to the conscientious victim that they do not care enough, are too selfish or have it easy. This usually results in the victim feeling bad, keeping them in a self-doubting, anxious and submissive position.
\item \tbf{Shaming:} Manipulator uses sarcasm and put-downs to increase fear and self-doubt in the victim. Manipulators use this tactic to make others feel unworthy and therefore defer to them. Shaming tactics can be very subtle such as a fierce look or glance, unpleasant tone of voice, rhetorical comments, subtle sarcasm. Manipulators can make one feel ashamed for even daring to challenge them. It is an effective way to foster a sense of inadequacy in the victim.
\item \tbf{Vilifying the victim:} More than any other, this tactic is a powerful means of putting the victim on the defensive while simultaneously masking the aggressive intent of the manipulator, while the manipulator falsely accuses the victim as being an abuser in response when the victim stands up for or defends themselves or their position.
\item \tbf{Playing the victim role:} Manipulator portrays themself as a victim of circumstance or of someone else's behavior in order to gain pity, sympathy or evoke compassion and thereby get something from another. Caring and conscientious people cannot stand to see anyone suffering and the manipulator often finds it easy to play on sympathy to get cooperation.
\item \tbf{Playing the servant role:} Cloaking a self-serving agenda in guise of a service to a more noble cause, for example saying they are acting in a certain way to be "obedient" to or in "service" to an authority figure or "just doing their job".
\item \tbf{Seduction:} Manipulator uses charm, praise, flattery or overtly supporting others in order to get them to lower their defenses and give their trust and loyalty to the manipulator. They will also offer help with the intent to gain trust and access to an unsuspecting victim they have charmed.
\item \tbf{Projecting the blame (blaming others):} Manipulator scapegoats in often subtle, hard-to-detect ways. Often, the manipulator will project their own thinking onto the victim, making the victim look like they have done something wrong. Manipulators will also claim that the victim is the one who is at fault for believing lies that they were conned into believing, as if the victim forced the manipulator to be deceitful. All blame, except for the part that is used by the manipulator to accept false guilt, is done in order to make the victim feel guilty about making healthy choices, correct thinking and good behaviors. It is frequently used as a means of psychological and emotional manipulation and control. Manipulators lie about lying, only to re-manipulate the original, less believable story into a "more acceptable" truth that the victim will believe. Projecting lies as being the truth is another common method of control and manipulation. Manipulators love to falsely accuse the victim as "deserving to be treated that way." They often claim that the victim is crazy and/or abusive, especially when there is evidence against the manipulator. (See Feigning, below.)
\item \tbf{Feigning innocence:} Manipulator tries to suggest that any harm done was unintentional or that they did not do something that they were accused of. Manipulator may put on a look of surprise or indignation. This tactic makes the victim question their own judgment and possibly their own sanity.
\item \tbf{Feigning confusion:} Manipulator tries to play dumb by pretending they do not know what the victim is talking about or is confused about an important issue brought to their attention. The manipulator intentionally confuses the victim in order for the victim to doubt their own accuracy of perception, often pointing out key elements that the manipulator intentionally included in case there is room for doubt. Sometimes manipulators will have used cohorts in advance to help back up their story.
\item \tbf{Brandishing anger:} Manipulator uses anger to brandish sufficient emotional intensity and rage to shock the victim into submission. The manipulator is not actually angry, they just put on an act. They just want what they want and get "angry" when denied. Controlled anger is often used as a manipulation tactic to avoid confrontation, avoid telling the truth or to further hide intent. There are often threats used by the manipulator of going to police, or falsely reporting abuses that the manipulator intentionally contrived to scare or intimidate the victim into submission. Blackmail and other threats of exposure are other forms of controlled anger and manipulation, especially when the victim refuses initial requests or suggestions by the manipulator. Anger is also used as a defense so the manipulator can avoid telling truths at inconvenient times or circumstances. Anger is often used as a tool or defense to ward off inquiries or suspicion. The victim becomes more focused on the anger instead of the manipulation tactic.
\item \tbf{Bandwagon effect:} Manipulator comforts the victim into submission by claiming (whether true or false) that many people already have done something, and the victim should as well. These include phrases such as "Many people like you ..." or "Everyone does this anyways.". Such manipulation can be seen in peer pressure situations, often occurring in scenarios where the manipulator attempts to influence the victim into trying drugs or other substances.
\end{itemize}

%========================================================================================
%========================================================================================

\subsection{Vulnerabilities exploited by manipulators}
According to Braiker's self-help book, manipulators exploit the following vulnerabilities (buttons) that may exist in victims:
\begin{itemize}
\item the "disease to please"
\item addiction to earning the approval and acceptance of others
\item Emotophobia (fear of negative emotion; i.e. a fear of expressing anger, frustration or disapproval)
\item lack of assertiveness and ability to say no
\item blurry sense of identity (with soft personal boundaries)
\item low self-reliance
\item external locus of control
\end{itemize}

According to Simon, manipulators exploit the following vulnerabilities that may exist in victims:
\begin{itemize}
\item naïveté - victim finds it too hard to accept the idea that some people are cunning, devious and ruthless or is "in denial" if they are being victimized.
\item over-conscientiousness - victim is too willing to give manipulator the benefit of the doubt and see their side of things in which they blame the victim.
\item low self-confidence - victim is self-doubting, lacking in confidence and assertiveness, likely to go on the defensive too easily.
\item over-intellectualization - victim tries too hard to understand and believes the manipulator has some understandable reason to be hurtful.
\item emotional dependency - victim has a submissive or dependent personality. The more emotionally dependent the victim is, the more vulnerable they are to being exploited and manipulated.
\end{itemize}

%========================================================================================
%========================================================================================

\subsection{Motivations of manipulators}
Manipulators can have various possible motivations, including but not limited to:
\begin{itemize}
\item the need to advance their own purposes and personal gain at virtually any cost to others
\item a strong need to attain feelings of power and superiority in relationships with others
\item a want and need to feel in control
\item a desire to gain a feeling of power over others in order to raise their perception of self-esteem
\item boredom, or growing tired of their surroundings, seeing it as a game more than hurting others
\item covert agenda, criminal or otherwise, including financial manipulation (often seen when the elderly or unsuspecting, unprotected wealthy are intentionally targeted for the sole purpose of obtaining a victim's financial assets)
\end{itemize}

%========================================================================================
%========================================================================================

\subsection{Forms of psychological manipulation}
\textbf{Gaslighting} is a form of psychological manipulation that seeks to sow seeds of doubt in a targeted individual or in members of a targeted group, making them question their own memory, perception, and sanity. Using persistent denial, misdirection, contradiction, and lying, it attempts to destabilize the victim and delegitimize the victim's belief. \href{https://en.wikipedia.org/wiki/Gaslighting}{wikipedia.org/wiki/Gaslighting}\\

%========================================================================================
%========================================================================================

\subsection{Covert hypnosis / conversational hypnosis}
Hypnotic language can be used to push emotional buttons of given person to persuade them the manipulator wants. Some examples are:\\
\textbf{I wouldn't want to tell you to ... / I wouldn't tell you to ... .} - this builds on not giving instruction or suggestion, therefore it do not raise resistance against such thought.\\
\textbf{You might want to ... now.} - this is considered as priming, which increases the likelihood of performing given statement.\\
\textbf{You don't have to ... now.} - telling people that they don't have to do something, is exactly what they think and want to hear.\\
\textbf{What it is that helps you to know whether you should ... or ... ?} - giving a question instead of statement will pull the person from passive into active state - into thinking. Giving too many choices will cause people to retract back.\\
\textbf{Why is it that some people see it and most people don't?} - gives them credit that they are capable and makes them tell the manipulator precisely what they want.\\
\textbf{I don't know if ... is a good idea. / I don't know if ... is the best thing for sure. } - by waking from passive mode into active, but that takes a while to happen and in that brief window victims are likely to comply.\\\\
Hypnosis also uses a lot "double bind technique", which simply said is only a illusion of choice - person is given a choice but both of the options are identical. For example, manipulator wants person to do something, but the person would decline if given a YES/NO question, such as "Meet with me on Thursday" - instead they ask "Let's meet, what is better for you - Thursday or Friday?".


















%========================================================================================
%========================================================================================

\subsection{Persuasion techniques}

\subsubsection{Low-ball}
\source{https://en.wikipedia.org/wiki/Low-ball}{en.wikipedia.org/wiki/Low-ball}\\\\
Low-ball is a persuasion and selling technique where the buyer offers very low price with the intention to increase it, but with goal to get more profitable deal than was the initial asking price.\\\\
Example of successful application:
\begin{itemize}
\item Item is being listed for $100 USD$.
\item Manipulator offers only $40 USD$, which sparks discussion. Seller and manipulator are making offers, seller is lowering the price and manipulator is increasing the offer.
\item In the end, manipulator seals a deal with $70 USD$ offer, much lower than original asking price.
\end{itemize}

\begin{quoting}
Cialdini, Cacioppo, Bassett, and Miller (1978) demonstrated the technique of low-balling in a university setting. They asked an initial group of first-year psychology students to volunteer to be part of a study on cognition. The researchers were clear about the meeting time being 7 a.m. Only 24 per cent of the first-year college students were willing to sacrifice and wake up early to support research in psychology. In a second group condition, the subjects were asked the same favour, but this time they were not told a time. Of them, 56 per cent agreed to take part. After agreeing to help in the study, they were told that they would have to meet at 7 a.m. and that they could back out if they so wished. None backed out of their commitment.\\\\
On the day of the actual meeting, $95\%$ of the students who had promised to come showed up for their 7 a.m. appointment, which means that, in the end, $53.5\%$ of the subject pool agreed to the experiment. Hence, when people have already showed commitment it's least likely for them to back off as they have already made up their mind.
\end{quoting}


\subsubsection{Cornering}
It is a technique in which manipulator uses multiple e-mails and phone numbers to low-ball seller.\\\\
Example of successful application:
\begin{itemize}
\item Item is being listed for $100 USD$.
\item Manipulator make offers from multiple origins of $40 USD$, $50 USD$, $60 USD$.
\item Seller might try to negotiate but in the end picks the best offer.
\item Manipulator gets a deal with $60 USD$, much lower than original asking price.
\end{itemize}





