\subsection{Frequency analysis}
\label{sec:frequency_analysis}
\source{http://practicalcryptography.com/cryptanalysis/text-characterisation/}{practicalcryptography.com/cryptanalysis/text-characterisation}\\
\source{http://practicalcryptography.com/cryptanalysis/text-characterisation/monogram-bigram-and-trigram-frequency-counts/}{/practicalcryptography.com/cryptanalysis/text-characterisation/monogram-bigram-and-trigram-frequency-counts}\\
\source{http://practicalcryptography.com/cryptanalysis/text-characterisation/quadgrams/}{practicalcryptography.com/cryptanalysis/text-characterisation/quadgrams}
\source{https://en.wikipedia.org/wiki/Frequency_analysis}{wikipedia.org/wiki/Frequency\_analysis}

%========================================================================================
%========================================================================================

\subsubsection*{Overview}

Frequency analysis is the study of frequency of letters (or group of letters). Frequency analysis is based on the fact that, in any given stretch of written language, certain letters and combinations of letters occur with varying frequencies. Moreover, there is a characteristic distribution of letters that is roughly the same for almost all samples of that language.\\\\
Interestingly, machines such as Enigma were essentially immune to straightforward frequency analysis.

\begin{table}[H]
\centering
\begin{tabular}{ | l | p{7cm} | }
\hline
\tbf{First published}	& 9th century \\ \hline
\tbf{Authors}			& Al-Kindi \\ \hline
\tbf{Category}			& Text Characterisation \\ \hline
\tbf{Effective defense}	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Homophony (e.g. homophonic substitution in section \ref{sec:substitution_cipher})
	\end{itemize}
	\end{minipage}
	\\ \hline
\tbf{Theoretical defense}	& \\ \hline
\end{tabular}
\caption{Overview - Frequency analysis.}
\label{tab:frequency_analysis_overview}
\end{table}

It is also possible that the plaintext does not exhibit the expected distribution of letter frequencies. Shorter messages are likely to show more variation. It is also possible to construct artificially skewed texts. For example, entire novels have been written that omit the letter \tit{e} altogether - a form of literature known as a lipogram.

%========================================================================================
%========================================================================================

\subsubsection*{Description}

Data presented in this section originate from \href{http://practicalcryptography.com/cryptanalysis/letter-frequencies-various-languages/english-letter-frequencies/}{practicalcryptography.com} (which in turn originate from \href{http://wortschatz.uni-leipzig.de/en/download/}{wortschatz.uni-leipzig.de/en}). This data is generated from $4.5$ billion characters in English text.\\\\
Frequency analysis is not only for single characters (figure \ref{fig:frequency_analysis_monogram}), it is also possible to measure the frequency other occurences such as bigrams (pairs of characters; figure \ref{fig:frequency_analysis_bigram}), trigrams (occurrence of 3 characters; figure \ref{fig:frequency_analysis_trigram}), whole words and more.

\begin{figure}[H]
	\centering
	\fbox{
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/frequency_analysis_english_monogram.png}
	}
	\caption{Frequency analysis - English - monogram.}
	\label{fig:frequency_analysis_monogram}
\end{figure}

\begin{figure}[H]
	\centering
	\fbox{
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/frequency_analysis_english_bigram.png}
	}
	\caption{Frequency analysis - English - bigram.}
	\label{fig:frequency_analysis_bigram}
\end{figure}

\begin{figure}[H]
	\centering
	\fbox{
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/frequency_analysis_english_trigram.png}
	}
	\caption{Frequency analysis - English - trigram.}
	\label{fig:frequency_analysis_trigram}
\end{figure}

To measure the similarity of deciphered text to actual language (English in this case), one can use frequency analysis of \tit{quadgrams} - deciphered text should approximate typical English text in relative frequency of occurring \tit{quadgrams}. For more information see \href{http://practicalcryptography.com/cryptanalysis/text-characterisation/quadgrams/}{practicalcryptography.com}.\\\\
Letter frequency depends on used language - some languages are compared in figure \ref{fig:letter_frequencies_across_languages}.

\begin{figure}[H]
	\centering
	\fbox{
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/letter_frequencies_across_languages.png}
	}
	\caption{Letter frequencies across languages (\href{https://upload.wikimedia.org/wikipedia/en/timeline/d8b53fa1e67acc2deeea77c87a9402a7.png}{wikipedia.org}.}
	\label{fig:letter_frequencies_across_languages}
\end{figure}



%========================================================================================
%========================================================================================

\subsubsection*{Examples}

Following example is taken from \name{The Gold-Bug} by \name{Edgar Allan Poe} (\href{https://en.wikipedia.org/wiki/Frequency_analysis}{wikipedia.org}).\\\\
Capital letters represent encrypted text and lower case letters represent plain-text.

\begin{lstlisting}[style=file, caption=step 0]
Encrypted message:
LIVITCSWPIYVEWHEVSRIQMXLEYVEOIEWHRXEXIPFEMVEWHKVSTYLXZIXLIKIIXPIJVSZEYPERRGERIM
WQLMGLMXQERIWGPSRIHMXQEREKIETXMJTPRGEVEKEITREWHEXXLEXXMZITWAWSQWXSWEXTVEPMRXRSJ
GSTVRIEYVIEXCVMUIMWERGMIWXMJMGCSMWXSJOMIQXLIVIQIVIXQSVSTWHKPEGARCSXRWIEVSWIIBXV
IZMXFSJXLIKEGAEWHEPSWYSWIWIEVXLISXLIVXLIRGEPIRQIVIIBGIIHMWYPFLEVHEWHYPSRRFQMXLE
PPXLIECCIEVEWGISJKTVWMRLIHYSPHXLIQIMYLXSJXLIMWRIGXQEROIVFVIZEVAEKPIEWHXEAMWYEPP
XLMWYRMWXSGSWRMHIVEXMSWMGSTPHLEVHPFKPEZINTCMXIVJSVLMRSCMWMSWVIRCIGXMWYMX
\end{lstlisting}

\begin{enumerate}[start=1]
\item Frequency analysis of given text shows that \tit{I} is the most common character, \tit{XL} is the most common bigram and \tit{XLI} is the most common trigram. This strongly suggest that \tit{I}, \tit{XL} and \tit{XLI} are substitution for \tit{e}, \tit{th} and \tit{the} respectively. The second most common letter in message is \tit{E}, suggesting it is substituted \tit{a} (third most common letter in English - since \tit{t} is already taken).
	\begin{itemize}
	\setlength\itemsep{-0.5em}
	\item \tit{I} = \tit{e}
	\item \tit{XL} = \tit{th}
	\item \tit{XLI} = \tit{the}
	\item \tit{E} = \tit{a}
	\end{itemize}
\end{enumerate}

\begin{lstlisting}[style=file, caption=step 1]
heVeTCSWPeYVaWHaVSReQMthaYVaOeaWHRtatePFaMVaWHKVSTYhtZetheKeetPeJVSZaYPaRRGaReM
WQhMGhMtQaReWGPSReHMtQaRaKeaTtMJTPRGaVaKaeTRaWHatthattMZeTWAWSQWtSWatTVaPMRtRSJ
GSTVReaYVeatCVMUeMWaRGMeWtMJMGCSMWtSJOMeQtheVeQeVetQSVSTWHKPaGARCStRWeaVSWeeBtV
eZMtFSJtheKaGAaWHaPSWYSWeWeaVtheStheVtheRGaPeRQeVeeBGeeHMWYPFhaVHaWHYPSRRFQMtha
PPtheaCCeaVaWGeSJKTVWMRheHYSPHtheQeMYhtSJtheMWReGtQaROeVFVeZaVAaKPeaWHtaAMWYaPP
thMWYRMWtSGSWRMHeVatMSWMGSTPHhaVHPFKPaZeNTCMteVJSVhMRSCMWMSWVeRCeGtMWYMt
\end{lstlisting}

\begin{enumerate}[start=2]
\item Searching the test for patters might reveal some ideas for guesses, such as \tit{Rtate} is \tit{state}, \tit{atthattMZe} is \tit{atthattime} or \tit{heVe} is \tit{here}.
	\begin{itemize}
	\setlength\itemsep{-0.5em}
	\item \tit{R} = \tit{s}
	\item \tit{M} = \tit{i}
	\item \tit{Z} = \tit{m}
	\end{itemize}
\end{enumerate}

\begin{lstlisting}[style=file, caption=step 2]
hereTCSWPeYraWHarSseQithaYraOeaWHstatePFairaWHKrSTYhtmetheKeetPeJrSmaYPassGasei
WQhiGhitQaseWGPSseHitQasaKeaTtiJTPsGaraKaeTsaWHatthattimeTWAWSQWtSWatTraPistsSJ
GSTrseaYreatCriUeiWasGieWtiJiGCSiWtSJOieQthereQeretQSrSTWHKPaGAsCStsWearSWeeBtr
emitFSJtheKaGAaWHaPSWYSWeWeartheStherthesGaPesQereeBGeeHiWYPFharHaWHYPSssFQitha
PPtheaCCearaWGeSJKTrWisheHYSPHtheQeiYhtSJtheiWseGtQasOerFremarAaKPeaWHtaAiWYaPP
thiWYsiWtSGSWsiHeratiSWiGSTPHharHPFKPameNTCiterJSrhisSCiWiSWresCeGtiWYit
\end{lstlisting}

\begin{enumerate}[start=3]
\item Filling in additional letter combinations should be easier with every step. But it might happen that some of the assumptions is incorrect and it might require backtracking. Repeat the process until all letters are deciphered and text makes sense.
\end{enumerate}

\begin{lstlisting}[style=file, caption=step 3]
hereuponlegrandarosewithagraveandstatelyairandbroughtmethebeetlefromaglasscasei
nwhichitwasencloseditwasabeautifulscarabaeusandatthattimeunknowntonaturalistsof
courseagreatprizeinascientificpointofviewthereweretworoundblackspotsnearoneextr
emityofthebackandalongoneneartheotherthescaleswereexceedinglyhardandglossywitha
lltheappearanceofburnishedgoldtheweightoftheinsectwasveryremarkableandtakingall
thingsintoconsiderationicouldhardlyblamejupiterforhisopinionrespectingit
\end{lstlisting}

\begin{enumerate}[start=4]
\item Insert spaces and punctuation.
\end{enumerate}

\begin{lstlisting}[style=file, caption=step 4]
Message:
Hereupon Legrand arose, with a grave and stately air, and brought me the
beetle from a glass case in which it was enclosed. It was a beautiful
scarabaeus, and, at that time, unknown to naturalists-of course a great prize
in a scientific point of view. There were two round black spots near one
extremity of the back, and a long one near the other. The scales were
exceedingly hard and glossy, with all the appearance of burnished gold. The
weight of the insect was very remarkable, and, taking all things into
consideration, I could hardly blame Jupiter for his opinion respecting it.

Mixed alphabet: EKGHIJYLMNAPZWSCDVRXTOQBFU
\end{lstlisting}

%========================================================================================
%========================================================================================

\subsubsection*{Typical occurrences - English}

English language has some typical occurrences such as:
\begin{itemize}
\setlength\itemsep{-0.5em}
\item \tit{Q} and \tit{U} nearly always occur together.
\item The most common doubled letters: \tit{LL EE SS OO TT FF RR NN PP CC} (\href{https://en.wikipedia.org/wiki/Letter_frequency}{wikipedia.org})
\item US vs UK English
	\begin{itemize}
	\item US English uses more letter \tit{Z} than UK English.
	\item US examples: analyze, apologize, recognize
	\item UK examples: analyse, apologise, recognise
	\end{itemize}
\item Top 12 letters constitute $81\%$ of the total usage; top 8 letters constitute $65\%$ of the total usage.
\item The most common initial letters are \tit{S}, \tit{A} and \tit{C}.
\end{itemize}






