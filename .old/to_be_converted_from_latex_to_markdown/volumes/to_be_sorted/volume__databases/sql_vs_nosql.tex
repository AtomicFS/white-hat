\section{SQL vs NoSQL}
\label{sec:sql_vs_nosql}
\source{https://www.sitepoint.com/sql-vs-nosql-differences/}{sitepoint.com}, \source{https://www.youtube.com/watch?v=ZS_kXvOeQ5Y}{youtube.com}\\\\
First of all, \name{SQL} (also called relational database) and \name{NoSQL} are alternatives which work in very different ways and it incorrect to claim that one is better that the other. They are simply different and are suited for different applications. Very large projects like Facebook do use both \name{SQL} and \name{NoSQL}. Also, keep in mind that \name{NoSQL} is rather hyped nowadays (SQL is in practical use longer and has less bugs).

%========================================================================================
%========================================================================================

\subsection{SQL}
SQL is actually a language (\tit{Structured Query Language}), which is a language that essentially allows to write a database queries (but also creating, changing, deleting, etc.). SQL-type database stores data in related tables. For example, if you run an online book store, book information can be added to a table named book (see table \ref{tab:sql_example_table}).

\begin{table}[H]
\centering
\begin{tabular}{ | l | l | l | l | l | }
\hline
\tbf{id} 	& \tbf{title}				& \tbf{author}		& \tbf{format}		& \tbf{price}\\ \hline
1		& JavaScript: Novice to Ninja	& Darren Jones		& ebook			& 29.00 \\ \hline
2		& Jump Start Git			& Shaumik Daityari	& ebook			& 29.00 \\ \hline
\end{tabular}
\caption{SQL table example.}
\label{tab:sql_example_table}
\end{table}

Every row is a different book record. Content of the table is defined by fields (columns - in example table \ref{tab:sql_example_table} it would be id, title, author, format and price) and every entry is a record (row) which contains values for each filed. It is important to note that record can't have more or less fields than the table (but the value can be empty). In addition all of the values must be normalized).\\\\
For SQL database, it is common to contain multiple tables which are related to each other. In the example of online book store, one table would contain the books (table \ref{tab:sql_example_table}), other the customers (table \ref{tab:sql_example_table_customer}) and another the orders (table \ref{tab:sql_example_table_order}).

\begin{table}[H]
\centering
\begin{tabular}{ | l | l | l | }
\hline
\tbf{id} 	& \tbf{email}		& \tbf{name} \\ \hline
1		& max@example.com	& Max Test \\ \hline
2		& min@example.com	& Min Manuel \\ \hline
\end{tabular}
\caption{SQL table example.}
\label{tab:sql_example_table_customer}
\end{table}

\begin{table}[H]
\centering
\begin{tabular}{ | l | l | l | }
\hline
\tbf{id} 	& \tbf{customer id}	& \tbf{product id} \\ \hline
1		& 2				& 1 \\ \hline
2		& 1				& 1 \\ \hline
3		& 2				& 2 \\ \hline
\end{tabular}
\caption{SQL table example.}
\label{tab:sql_example_table_order}
\end{table}

In this particular example, one customer may want multiple products and every product may be wanted by multiple customers - this is so called "many to many" relation. All types of relations can be seen in figure \ref{fig:sql_table_relations_types}.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/sql_table_relations_types.png}
	\caption{SQL table relation types (\href{https://www.youtube.com/watch?v=ZS_kXvOeQ5Y}{source}).}
	\label{fig:sql_table_relations_types}
\end{figure}

The SQL is capable of querying these relations with JOIN commands so one can retrieve all the connected data in once result set.

%========================================================================================
%========================================================================================

\subsection{NoSQL}
NoSQL databases store data in JSON-like field-value pair documents (see listing \ref{lst:nosql_example_table}).

\begin{lstlisting}[style=file, caption=NoSQL document example, label={lst:nosql_example_table}]
{
	ISBN: 9780992461225,
	title: "JavaScript: Novice to Ninja",
	author: "Darren Jones",
	format: "ebook",
	price: 29.00
}
\end{lstlisting}

Such a database contains collections in which store documents (document is an entry). These documents do not have to follow the same schema (you can store any data you like in any document).\\\\
In addition, there are no relations between collections (it is possible to add manually with custom queries, but generally there are no relations). The main idea is that all of the relevant data is in one place - in example of the online book store, the orders would contain all of the products and user information in every single document. This is done so that query for order can visit only the orders collection (meaning that there are some duplicated data).\\\\
Advantage is simple queries and short reading time, since the record is simply pulled from single collection. Unfortunately, while writing or changing data, all of the instances must be updated as well, which has negative effect on the performance.

%========================================================================================
%========================================================================================

\subsection{Comparison}
\subsubsection{Data}
SQL tables create a strict data template, so it's difficult to make mistakes. NoSQL is more flexible and forgiving, but being able to store any data anywhere can lead to consistency issues.\\\\
In an SQL database, it's impossible to add data until you define tables and field types in what's referred to as a schema. The schema optionally contains other information, such as:
\begin{itemize}
\item Primary keys - unique identifiers such as the ISBN which apply to a single record
\item Indexes - commonly queried fields indexed to aid quick searching
\item Relationships - logical links between data fields
\item Functionality such as triggers and stored procedures
\end{itemize}

Your data schema must be designed and implemented before any business logic can be developed to manipulate data. It's possible to make updates later, but large changes can be complicated.\\\\
In a NoSQL database, data can be added anywhere, at any time. There's no need to specify a document design or even a collection up-front.


\subsubsection{Performance}
Perhaps the most controversial comparison, NoSQL is regularly quoted as being faster than SQL. This isn't surprising; NoSQL's simpler denormalized store allows you to retrieve all information about a specific item in a single request. There's no need for related JOINs or complex SQL queries.\\\\
That said, your project design and data requirements will have most impact. A well-designed SQL database will almost certainly perform better than a badly designed NoSQL equivalent and vice versa.


\subsubsection{Scaling}
As your data grows, you may find it necessary to distribute the load among multiple servers. This can be tricky for SQL-based systems. How do you allocate related data? Clustering is possibly the simplest option; multiple servers access the same central store — but even this has challenges.\\\\
NoSQL's simpler data models can make the process easier, and many have been built with scaling functionality from the start. That is a generalization, so seek expert advice if you encounter this situation.\\\\
But simply said, SQL database can be easily scaled vertically (more powerful \hw), but it is very difficult to scale vertically (adding more independent servers). NoSQL on the other hand can be scaled easily both horizontally and vertically.


\subsubsection{Summary}
SQL and NoSQL databases do the same thing in different ways. It's possible choose one option and switch to another later, but a little planning can save time and money.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/sql_vs_nosql_comparison.png}
	\caption{SQL vs NoSQL comparison (\href{https://www.youtube.com/watch?v=ZS_kXvOeQ5Y}{source}).}
	\label{fig:sql_vs_nosql_comparison}
\end{figure}

Projects where \name{SQL} is ideal:
\begin{itemize}
\item logical related discrete data requirements which can be identified up-front
\item data integrity is essential
\item standards-based proven technology with good developer experience and support
\end{itemize}

Projects where \name{NoSQL} is ideal:
\begin{itemize}
\item unrelated, indeterminate or evolving data requirements
\item simpler or looser project objectives, able to start coding immediately
\item speed and scalability is imperative
\end{itemize}

In the case of our book store, an SQL database appears the most practical option - especially when we introduce ecommerce facilities requiring robust transaction support.\\
A NoSQL database may be more suited to projects where the initial data requirements are difficult to ascertain. That said, don't mistake difficulty for laziness: neglecting to design a good data store at project commencement will lead to problems later.

\subsubsection{Recommendation}
\source{https://www.youtube.com/watch?v=7Pg5AMSInJ0}{youtube.com}\\\\
Generally, you can build any application with either of them, however it is highly recommended to use SQL database for following reasons:
\begin{itemize}
\item SQL is used in practice much longer.
\item SQL is more polished with less bugs.
\item SQL is standardized (implementations are practically interchangeable).
\item It is highly unlikely that the application will grow into such extent that weaknesses of SQL would cause necessity for NoSQL. And in those rare cases, simply migrate some of the data to NoSQL (use both SQL and NoSQL).
\end{itemize}

%========================================================================================
%========================================================================================





