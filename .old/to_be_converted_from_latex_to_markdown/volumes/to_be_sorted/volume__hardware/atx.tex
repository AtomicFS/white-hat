\section{ATX - Desktop computer form format}
\label{sec:atx}
\source{https://en.wikipedia.org/wiki/ATX}{wikipedia.org/wiki/ATX}\\
\source{http://www.buildcomputers.net/computer-case-sizes.html}{buildcomputers.net/computer-case-sizes}\\\\

\tbf{ATX} (Advanced Technology eXtended) is a specification (standard) for motherboard and power-supply developed by Intel in 1995. The specification defines physical dimensions, mounting points, I/O panel and power connectors.\\\\
\tbf{ATX} is the most common motherboard standard, making the board $12 x 9.6 in$ ($305 x 244 mm$).\\\\
In 2004, Intel announced the \tbf{BTX} (Balanced Technology eXtended) standard, intended as a replacement for \tbf{ATX}. One of the reasons for the failure of \tbf{BTX} to gain traction in was the rise of energy-efficient components (CPUs, chipsets and GPUs) which require less power and produce less waste heat, eliminating two of the primary intended benefits of BTX.

%========================================================================================
%========================================================================================

\subsection{Motherboards}
\begin{figure}[H]
	\centering
	\fbox{
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/VIA_Mini-ITX_Form_Factor_Comparison.jpg}
	}
	\caption{Motherboard form factors (\href{https://upload.wikimedia.org/wikipedia/commons/1/11/VIA_Mini-ITX_Form_Factor_Comparison.jpg}{wikimedia.org}).}
	\label{fig:motherboard_formfactors}
\end{figure}

\subsection{Cases}
They are not standardized, but following will apply to the majority of cases.
\begin{figure}[H]
	\centering
	\fbox{
	\includegraphics[width=0.8\textwidth,height=\textheight,keepaspectratio]{../Figures/xcomputer-case-sizes.jpg}
	}
	\caption{Desktop cases (\href{http://www.buildcomputers.net/images/xcomputer-case-sizes.jpg.pagespeed.ic.LglmnVI5Qh.jpg}{buildcomputers.net}).}
	\label{fig:desktop_cases}
\end{figure}

\begin{table}[H]
\centering
\begin{tabular}{l|c|c|c|c|}
\cline{2-5}
                                      & SFF      & Mini Tower & Mid Tower & Full Tower \\ \hline
\multicolumn{1}{|l|}{Motherboards}    & Mini-ITX & Mini-ITX   & Mini-ITX  & Mini-ITX   \\
\multicolumn{1}{|l|}{}                &          & MicroATX   & MicroATX  & MicroATX   \\
\multicolumn{1}{|l|}{}                &          &            & ATX       & ATX        \\
\multicolumn{1}{|l|}{}                &          &            &           & EATX       \\ \hline
\multicolumn{1}{|l|}{5.25" Drive Bay} & 1        & 1-2        & 2-5       & 3-6        \\ \hline
\multicolumn{1}{|l|}{3.5" Drive Bays} & 1-3      & 4-6        & 6-8       & 6-13       \\ \hline
\multicolumn{1}{|l|}{2.5" Drive Bays} & 0-4      & 0-4        & 0-10      & 0-11       \\ \hline
\multicolumn{1}{|l|}{Expansion Slots} & 2        & 4          & 7-8       & 7-10       \\ \hline
\multicolumn{1}{|l|}{Graphics Cards}  & 1        & 1-2        & 2-3       & 3-4        \\ \hline
\multicolumn{1}{|l|}{Case Fans}       & 1-3      & 2-4        & 3-9       & 5-10       \\ \hline
\end{tabular}
\caption{Overview of case properties}
\label{table:atx_cases}
\end{table}

5.25" Drive Bays - For optical drives (e.g. DVD and Blu-ray drives), multi card readers, fan controllers, cooling fans and even drawers. Most 5.25" bays are external (one side is exposed to the outside - so the optical drive can be accessed by user).\\\\
3.5" Drive Bays (Internal) - For desktop hard disk drives.\\\\
3.5" Drive Bays (External) - For multi card readers, USB ports hub and cooling fans.\\\\
2.5" Drive Bays - For solid state drives and laptop hard disk drives. Most 2.5" bays are internal. Some low end and older cases lack 2.5" bays, but you can always install SSDs into 3.5" bays using converter brackets.\\\\
Expansion Slots - For expansion cards such as graphics cards, sound cards, network cards (Wi-Fi, Ethernet, Bluetooth) and connector cards (USB, FireWire, eSATA). Mid to high end graphics cards often take up two slots.

%========================================================================================
%========================================================================================

\subsection{Power supplies}
ATX power supplies generally have the dimensions of $150 \times 86 \times 140 mm$.
\begin{figure}[H]
	\centering
	\fbox{
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/ATX_power_supply_connector.png}
	}
	\caption{24-pin ATX12V 2.x power supply connector (\href{https://en.wikipedia.org/wiki/ATX}{wikipedia.org}).}
	\label{fig:atx_power_connector}
\end{figure}

ATX power supply specification is shown in table \ref{table:atx_psu} (these specifications may vary depending on the standard revision). There is also specification for ripple in a 10 Hz–20 MHz bandwidth.
\begin{table}[H]
\centering
\begin{tabular}{|c|c|c|}
\hline
Supply (V)	& Tolerance				& Ripple, p. to p., max. (mV) \\ \hline
+5			& $\pm$5\% ~($\pm 0.25V$)	& 50						\\ \hline
-5			& $\pm$10\% ~($\pm 0.50V$)	& 50						\\ \hline
+12			& $\pm$5\% ~($\pm 0.60V$)	& 120					\\ \hline
-12			& $\pm$10\% ~($\pm 1.20V$)	& 120					\\ \hline
+3.3			& $\pm$5\% ~($\pm 0.165V$)	& 50						\\ \hline
+5 standby	& $\pm$5\% ~($\pm 0.25V$)	& 50						\\ \hline
\end{tabular}
\caption{Overview of case properties}
\label{table:atx_psu}
\end{table}

The 20–24-pin Molex \unsure{Is the connector really called like this?} Mini-Fit Jr. has a power rating of 600 volts, 8 amperes maximum per pin (while using 18 AWG wire).

%========================================================================================
%========================================================================================

\subsection{ATX PSU Revisions}

The following table shows the \tbf{ATX} revision, along with time of introduction and very brief description of content.

\begin{table}[H]
\centering
\begin{tabular}{ | l | p{9cm} | r | }
\hline
Revision					& Changes							& Date \\ \hline
Original					& 								& 1995 \\ \hline
ATX12V 1.0	& 
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item 12V rail - increase power
	\item Extra 4-pin mini fit JR, 12V for CPU
	\end{itemize}
	\end{minipage}
	& Feb 2000 \\ \hline
ATX12V 1.1	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Mainly minor changes
	\item 3.3V rail - slight power increase
	\end{itemize}
	\end{minipage}
	& Aug 2000 \\ \hline
ATX12V 1.2	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Only minor changes
	\item -5V rail no longer required
	\end{itemize}
	\end{minipage}
	& Jan 2002 \\ \hline
ATX12V 1.3	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item 12V rail - slight power increase
	\item Minimal required efficiency for light and normal load
	\item Defined acoustic levels
	\item Introduction of SATA (section \ref{sec:sata})
	\item -5V rail removed (but not prohibited)
	\end{itemize}
	\end{minipage}
	& Apr 2003 \\ \hline
ATX12V 2.0	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Most power is now provided on 12V rails (standard specifies two independent 12V rails)
	\item 3.3V and 5V rails - significantly reduced power
	\item Main ATX power connector extended to 24 pins
	\item The six-pin AUX connector from ATX12V 1.x removed (replaced by 24-pin main connector)
	\item Required SATA power connector
	\item Many other changes
	\end{itemize}
	\end{minipage}
	& Feb 2003 \\ \hline
ATX12V 2.01	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Minor changes
	\item -5V rail removed completely
	\end{itemize}
	\end{minipage}
	& Jun 2004 \\ \hline
ATX12V 2.1	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Power increased on all rails
	\item Efficiency requirements changed
	\end{itemize}
	\end{minipage}
	& Mar 2005 \\ \hline
ATX12V 2.2	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Some corrections
	\item High Current Series wire terminals for 24-pin main and 4-pin 12V power connectors
	\end{itemize}
	\end{minipage}
	& May 2005 \\ \hline
ATX12V 2.3	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Recommended efficiency increased to 80\%, required to 70\%
	\item 12V minimum load lowered
	\item Removed absolute over-current limit of 240VA per rail (12V lines can provide more than 20A)
	\end{itemize}
	\end{minipage}
	& Mar 2007 \\ \hline
ATX12V 2.31	&
	\begin{minipage}[t]{\linewidth}
	\begin{itemize}[nosep,leftmargin=*]
	\item Maximum allowed ripple of 400mV to the PWR\_ON and PWR\_OK signals
	\item DC power must hold for more than 1ms after the PWR\_OK signal drops
	\item Clarified country-specific input line harmonic content and electromagnetic compatibility requirements
	\end{itemize}
	\end{minipage}
	& Feb 2008 \\ \hline
ATX12V 2.4	&
	& Apr 2013 \\ \hline
\end{tabular}
\caption{ATX PSU revisions.}
\label{tab:atx_psu_revisions}
\end{table}

%========================================================================================
%========================================================================================

\subsection{80 Plus}
\source{https://en.wikipedia.org/wiki/80_Plus}{wikipedia.org/wiki/80\_Plus}\\\\
80 Plus (trademarked 80 PLUS) is a voluntary certification program intended to promote efficient energy use in computer power supply units (PSUs).
\begin{figure}[H]
	\centering
	\fbox{
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Figures/80plus.png}
	}
	\caption{Efficiency level certifications (\href{https://en.wikipedia.org/wiki/80_Plus}{wikipedia.org}).}
	\label{fig:80plus}
\end{figure}



