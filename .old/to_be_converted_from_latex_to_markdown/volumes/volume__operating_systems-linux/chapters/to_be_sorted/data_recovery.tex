\section{Data recovery - PhotoRec}
\label{sec:data_recovery_photorec}
\source{https://www.cgsecurity.org/wiki/PhotoRec}{cgsecurity.org/wiki/PhotoRec}\\\\
If data have been only deleted, this procedure will recover them. However, if the disk have experienced a failure, see section \ref{sec:hw_data_recovery}.\\\\
Universal \ossw{} tool for recovering data is \name{PhotoRec}. It ignores the file-system, scanning the entire space for familiar file types (it will still work with severely damaged file-system or reformatted medium).

\begin{quoting}
\name{PhotoRec} is a file recovery utility using data carving techniques. It searches for known file headers and because there is (usually) no data fragmentation, it can recover the whole file. It can also handle some cases of low data fragmentation.
\end{quoting}

It is a part of \name{TestDisk} - program for recovering partitions, fixing partition tables etc.\\\\
Even though \name{PhotoRec} uses read-only access to handle the device for safety, I recommend to make a binary image anyway for SD cards, flash discs and similar - not for safety but for higher speeds.\\\\
Supported file-systems are:
\begin{itemize}
\setlength\itemsep{-0.5em}
\item FAT
\item NTFS
\item exFAT
\item ext2 / ext3 / ext4
\item HFS+
\end{itemize}

Over 480 supported file formats (see appendix \ref{appendix:file_extensions} or \href{https://www.cgsecurity.org/wiki/File_Formats_Recovered_By_PhotoRec}{cgsecurity.org} website).\\\\
To recover more files, it is possible to add custom signatures. For more information see \href{https://www.cgsecurity.org/wiki/Add_your_own_extension_to_PhotoRec}{cgsecurity.org/wiki/Add\_your\_own\_extension\_to\_PhotoRec}.

%========================================================================================
%========================================================================================

\subsection{Installation}

Debian, Ubuntu or Linux Mint
\begin{lstlisting}[style=terminal]
# apt-get install testdisk
\end{lstlisting}

CentOS, RHEL or Fedora
\begin{lstlisting}[style=terminal]
# yum install testdisk
\end{lstlisting}

Fedora 22 or newer
\begin{lstlisting}[style=terminal]
# dnf install testdisk
\end{lstlisting}

Arch Linux
\begin{lstlisting}[style=terminal]
# pacman -S testdisk
\end{lstlisting}

Gentoo
\begin{lstlisting}[style=terminal]
emerge testdisk
\end{lstlisting}

%========================================================================================
%========================================================================================

\subsubsection{Usage}

Unmount the device or partition and run following command.
\begin{lstlisting}[style=terminal]
# photorec /dev/sdaX
\end{lstlisting}

For binary copy, use following command.
\begin{lstlisting}[style=terminal]
$ photorec ./partition_image.img
\end{lstlisting}

In \name{PhotoRec}, confirm the selected device as shown in figure \ref{fig:photorec_01} (shown figures are example with a binary copy of partition).

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_01.png}
	%}
	\caption{PhotoRec - Select media.}
	\label{fig:photorec_01}
\end{figure}

On the next screen (as shown in figure \ref{fig:photorec_02}), navigate in the bottom menu into \textit{Options} (figure \ref{fig:photorec_04}) and \textit{File Opt} (figure \ref{fig:photorec_04}).

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_02.png}
	%}
	\caption{PhotoRec - Select partition.}
	\label{fig:photorec_02}
\end{figure}

As shown in figure \ref{fig:photorec_03}, \tit{Options} menu allows to change some of the behavior (it is recommended to keep these options at default values).

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_03.png}
	%}
	\caption{PhotoRec - Options.}
	\label{fig:photorec_03}
\end{figure}

As shown in figure \ref{fig:photorec_04}, \tit{File Opt} menu allows to specify which files to restore.

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_04.png}
	%}
	\caption{PhotoRec - File Options.}
	\label{fig:photorec_04}
\end{figure}

Chose which file-system was used on the device or partition (see figure \ref{fig:photorec_05}).

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_05.png}
	%}
	\caption{PhotoRec - Select used file system.}
	\label{fig:photorec_05}
\end{figure}

Next, as shown in figure \ref{fig:photorec_06}, chose the space to analyze - search on whole partition, or search only the free space.

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_06.png}
	%}
	\caption{PhotoRec - Select space to analyze.}
	\label{fig:photorec_06}
\end{figure}

Now, figure \ref{fig:photorec_07}, chose the target location for recovered files.\\\\
\textcolor{red}{\tbf{WARNING:} Do not select the same device / partition! This would overwrite the existing data and prevent recovery.}

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_07.png}
	%}
	\caption{PhotoRec - Select destination directory.}
	\label{fig:photorec_07}
\end{figure}

Data recovery will begin (figure \ref{fig:photorec_08}).

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_08.png}
	%}
	\caption{PhotoRec - Recovery in progress.}
	\label{fig:photorec_08}
\end{figure}

When the recovery is done, screen similar to one in figure \ref{fig:photorec_09} should appear. Now it is safe to close the program, all files have been saved into selected destination.

\begin{figure}[H]
	\centering
	%\fbox{
	\includegraphics[width=1\textwidth,height=\textheight,keepaspectratio]{../Figures/photorec_09.png}
	%}
	\caption{PhotoRec - Recovery finished.}
	\label{fig:photorec_09}
\end{figure}










