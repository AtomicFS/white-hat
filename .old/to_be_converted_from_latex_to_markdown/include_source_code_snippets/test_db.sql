CREATE TABLE `students` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` varchar(255) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `test` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`score` tinyint(4) DEFAULT NULL,
	`test_date` date DEFAULT NULL,
	`id_student` INT(11),
	PRIMARY KEY (`id`)
);

ALTER TABLE `test` ADD CONSTRAINT `test_fk0` FOREIGN KEY (`id_student`) REFERENCES `students`(`id`);

