'May 2019
' Script to hide selected updates
' Original Mike.Moore Dec 17, 2012 on answers.microsoft (it hid everything)
' Link to script: http://www.msfn.org/board/topic/163162-hide-bing-desktop-and-other-windows-updates/

'== Check privileges
Wscript.Echo "Check privileges"

Set WshShell = WScript.CreateObject("WScript.Shell")
If WScript.Arguments.Length = 0 Then
	Set ObjShell = CreateObject("Shell.Application")
	ObjShell.ShellExecute "cscript.exe" _
	, """" & WScript.ScriptFullName & """ RunAsAdministrator", , "runas", 1
	WScript.Quit
End if

'== Start
Wscript.Echo "Start"
Wscript.Echo "Pass counter:"

Dim WSHShell, StartTime, ElapsedTime, strUpdateName, strAllHidden
Dim Checkagain 'Find more keep going otherwise Quit

Dim hideupdates(87) 'TO ADD 1 EDIT THE (3) AND ADD another hideupdates(#)

hideupdates(0) = "Bing Bar"   'Bing
hideupdates(1) = "Bing Desktop"   'Bing
hideupdates(2) = "KB2505438"   'Anti-piracy
hideupdates(3) = "KB2506928"   '
hideupdates(4) = "KB2545698"   'IE9
hideupdates(5) = "KB2574819"   'Telemetry
hideupdates(6) = "KB2592687"   '
hideupdates(7) = "KB2660075"   '
hideupdates(8) = "KB2673774"   'Bing
hideupdates(9) = "KB2709981"   '
hideupdates(10) = "KB2726535"   '
hideupdates(11) = "KB2798162"   '
hideupdates(12) = "KB2830477"   'Telemetry
hideupdates(13) = "KB2876229"   'Skype
hideupdates(14) = "KB2882822"   'Questionable
hideupdates(15) = "KB2902907"   'Telemetry
hideupdates(16) = "KB2919355"   '
hideupdates(17) = "KB2922324"   '
hideupdates(18) = "KB2923545"   'Questionable
hideupdates(19) = "KB2952664"   'Win10
hideupdates(20) = "KB2970228"   '
hideupdates(21) = "KB2976978"   'Win8
hideupdates(22) = "KB2976987"   '
hideupdates(23) = "KB2977759"   'Telemetry
hideupdates(24) = "KB2990214"   'Win10
hideupdates(25) = "KB2994023"   '
hideupdates(26) = "KB2999226"   'Questionable
hideupdates(27) = "KB3012973"   'Win10
hideupdates(28) = "KB3015249"   'Telemetry
hideupdates(29) = "KB3021917"   'Telemetry
hideupdates(30) = "KB3022345"   'Telemetry
hideupdates(31) = "KB3035583"   'Win10
hideupdates(32) = "KB3042058"   'Telemetry
hideupdates(33) = "KB3044374"   'Win10
hideupdates(34) = "KB3045999"   '
hideupdates(35) = "KB3046480"   'Telemetry
hideupdates(36) = "KB3050265"   'Win10
hideupdates(37) = "KB3050267"   'Win10
hideupdates(38) = "KB3064683"   'Win10
hideupdates(39) = "KB3065987"   'Win10
hideupdates(40) = "KB3065988"   'Win10
hideupdates(41) = "KB3068707"   'Telemetry
hideupdates(42) = "KB3068708"   'Telemetry
hideupdates(43) = "KB3072318"   'Win10
hideupdates(44) = "KB3072630"   '
hideupdates(45) = "KB3074677"   'Win10
hideupdates(46) = "KB3075249"   'Telemetry
hideupdates(47) = "KB3075851"   'Win10
hideupdates(48) = "KB3075853"   'Win10
hideupdates(49) = "KB3076895"   '
hideupdates(50) = "KB3080149"   'Telemetry
hideupdates(51) = "KB3081437"   'Win10
hideupdates(52) = "KB3081454"   'Win10
hideupdates(53) = "KB3081954"   'Telemetry
hideupdates(54) = "KB3083324"   'Win10
hideupdates(55) = "KB3083325"   'Win10
hideupdates(56) = "KB3083710"   'Telemetry
hideupdates(57) = "KB3083711"   'Telemetry
hideupdates(58) = "KB3086255"   'Questionable
hideupdates(59) = "KB3088195"   'Telemetry
hideupdates(60) = "KB3090045"   'Win10
hideupdates(61) = "KB3092627"   '
hideupdates(62) = "KB3093513"   '
hideupdates(63) = "KB3093983"   'Telemetry
hideupdates(64) = "KB3112336"   'Win10
hideupdates(65) = "KB3112343"   'Win10
hideupdates(66) = "KB3118401"   'Questionable
hideupdates(67) = "KB3123862"   'Win10
hideupdates(68) = "KB3133977"   '
hideupdates(69) = "KB3135445"   'Win10
hideupdates(70) = "KB3135449"   'Win10
hideupdates(71) = "KB3138612"   'Questionable
hideupdates(72) = "KB3138615"   'IE10
hideupdates(73) = "KB3139929"   'IE 11
hideupdates(74) = "KB3146449"   'Win10
hideupdates(75) = "KB3148198"   'IE11
hideupdates(76) = "KB3150513"   'Win10
hideupdates(77) = "KB3161608"   'Win10
hideupdates(78) = "KB3161647"   'Win10
hideupdates(79) = "KB3163589"   'Win10
hideupdates(80) = "KB3170735"   'Win10
hideupdates(81) = "KB3173040"   'Win10
hideupdates(82) = "KB3184143"   'Win10
hideupdates(83) = "KB3192403"   'Telemetry
hideupdates(84) = "KB3192404"   'Telemetry
hideupdates(85) = "KB971033"   'Anti-piracy
hideupdates(86) = "Language Pack"   'Annoying
hideupdates(87) = "Silverlight"   'Annoying

Set WSHShell = CreateObject("WScript.Shell")

StartTime = Timer 'Start the Timer

Set updateSession = CreateObject("Microsoft.Update.Session")
updateSession.ClientApplicationID = "MSDN Sample Script"
Set updateSearcher = updateSession.CreateUpdateSearcher()
Set searchResult = updateSearcher.Search("IsInstalled=0 and Type='Software' and IsHidden=0")

Checkagain = "True"

For K = 0 To 10 'Bing Desktop has 4, Silverlight has 5
If Checkagain = "True" Then
	Wscript.Echo K
	Checkagain = "False"
	CheckUpdates
	ParseUpdates
End if
Next

Wscript.Echo "Script done"

ElapsedTime = Timer - StartTime
strTitle = "Hidden Windows updates."
strText = strAllHidden
strText = strText & vbCrLf & ""
strText = strText & vbCrLf & "Total Time " & ElapsedTime
intType = vbOkOnly

'Silent just comment these 2 lines with a ' and it will run and quit
Set objWshShell = WScript.CreateObject("WScript.Shell")
intResult = objWshShell.Popup(strText, ,strTitle, intType)

'Open Windows Update after remove the comment '
'WshShell.Run "%windir%\system32\control.exe /name Microsoft.WindowsUpdate"

Set objWshShell = nothing
Set WSHShell = Nothing
WScript.Quit


Function ParseUpdates 'cycle through updates
For I = 0 To searchResult.Updates.Count-1
Set update = searchResult.Updates.Item(I)
strUpdateName = update.Title
'WScript.Echo I + 1 & "> " & update.Title
For j = 0 To UBound(hideupdates)
if instr(1, strUpdateName, hideupdates(j), vbTextCompare) = 0 then
Else
strAllHidden = strAllHidden _
& vbcrlf & update.Title
update.IsHidden = True'
Checkagain = "True"
end if
Next
Next
End Function

Function CheckUpdates 'check for new updates cause Bing Desktop has 3
Set updateSession = CreateObject("Microsoft.Update.Session")
updateSession.ClientApplicationID = "MSDN Sample Script"
Set updateSearcher = updateSession.CreateUpdateSearcher()
Set searchResult = _
updateSearcher.Search("IsInstalled=0 and Type='Software' and IsHidden=0")
End Function
