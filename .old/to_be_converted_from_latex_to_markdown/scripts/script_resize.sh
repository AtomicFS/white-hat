#!/bin/bash

declare -a StringArray=( "png" "jpg" "JPG" )

WORKING_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SOURCE="$WORKING_DIR/Figures_source"
DESTINATION="$WORKING_DIR/Figures"
export WORKING_DIR
export SOURCE
export DESTINATION

imagemagick_func () {
	BASENAME=$(basename "$1")
	#convert "$SOURCE/$BASENAME" -resize 800x600\> "$DESTINATION/$BASENAME"	# 4:3
	convert "$SOURCE/$BASENAME" -resize 1024x768\> "$DESTINATION/$BASENAME"	# 4:3
	#convert "$SOURCE/$BASENAME" -resize 1280x960\> "$DESTINATION/$BASENAME"		# 4:3
	#convert "$SOURCE/$BASENAME" -resize 1400x1050\> "$DESTINATION/$BASENAME"		# 4:3
	#convert "$SOURCE/$BASENAME" -resize 1600x1200\> "$DESTINATION/$BASENAME"		# 4:3
}
export -f imagemagick_func

for val in ${StringArray[@]}; do
	find . -type f -name "*.$val" -exec bash -c 'imagemagick_func {}' \;
done

