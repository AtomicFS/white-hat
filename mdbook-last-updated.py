#!/usr/bin/python

import json
import sys
import re
import subprocess
import os


def get_git_date(path) -> str:
    real_path = os.path.join('src', path)
    if not os.path.isfile(real_path):
        return ''

    git_proc = subprocess.Popen(
        ['git', 'log', '-1', '--pretty=Last modified: %cs', real_path],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = git_proc.communicate()
    return str(stdout.decode(errors='ignore').strip())


def insert_modify_date(chapter):
    old_content = chapter['Chapter']['content']
    date = get_git_date(chapter['Chapter']['source_path'])
    chapter['Chapter']['content'] = f"*{date}*\n\n{old_content}"


def process_chapter(chapter):
    if 'source_path' in chapter['Chapter'] and chapter['Chapter']['source_path']:
        insert_modify_date(chapter)
    if 'sub_items' in chapter['Chapter']:
        for item in chapter['Chapter']['sub_items']:
            process_chapter(item)


if __name__ == '__main__':
    if len(sys.argv) > 1:  # we check if we received any argument
        if sys.argv[1] == "supports":
            # then we are good to return an exit status code of 0, since the other argument will just be the renderer's name
            sys.exit(0)

    # load both the context and the book representations from stdin
    context, book = json.load(sys.stdin)
    # and now, we can just modify the content

    # For movies and shows:
    with open('/tmp/mdbook-stuff.txt', 'w') as debug_file:
        # debug_file.write(json.dumps(book, indent=2))
        for section in book['sections']:
            # If current section is section with movies or shows
            if 'Chapter' in section:
                process_chapter(chapter=section)

    # we are done with the book's modification, we can just print it to stdout
    print(json.dumps(book))
